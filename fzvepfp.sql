--------------------------------------------------------
--  File created - Monday-August-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View FZVEPFP
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FISHRS"."FZVEPFP" ("PIDM", "PART_ORDER", "FOAPAL_PART", "FOAPAL_VALUE") AS 
  select
       pidm, part_order, foapal_part,
       v.gorsdav_value.accessvarchar2() foapal_value
  from gorsdav v,
      (select pidm, tabl, foapal_part, part_order
         from(select ftvsdat_sdat_code_opt_1 foapal_part,
                     ftvsdat_code_level part_order
                from ftvsdat
               where ftvsdat_sdat_code_entity = 'FZKEPRC'
                 and ftvsdat_sdat_code_attr = 'CODE-FORMAT-PART'
                 and ftvsdat_status_ind = 'A'
                 and ftvsdat_eff_date <= sysdate
                 and nvl(ftvsdat_term_date, sysdate+1) > sysdate
                 and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate),
             (select distinct gorsdav_table_name tabl, gorsdav_pk_parenttab pidm
                from gorsdav
               where gorsdav_table_name = 'SPRIDEN'
                 and gorsdav_attr_name in('EP_DEFCOAS','EP_DEFFUND','EP_DEFORGN',
                             'EP_DEFACCT','EP_DEFPROG','EP_DEFACTV','EP_DEFLOCN')))
 where pidm = gorsdav_pk_parenttab(+) and tabl = gorsdav_table_name(+)
   and 'EP_DEF'||foapal_part = gorsdav_attr_name(+);
