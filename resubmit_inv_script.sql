/* Run as part of first close */
declare
resubmit_req clob;
l_rsp clob;
cursor inv_req_list is
select distinct fzrcxml_eprc_id
  from fzrcxml
 where fzrcxml_doc_status = 'E'
   and fzrcxml_doc_type = 'INVOICE'
   and fzrcxml_error_msg like '%Purchase Order date cannot be greater than invoice transaction date.';
   
begin

  for i in inv_req_list loop
  
    select fzrcxml_doc
       into resubmit_req
       from fzrcxml
       where fzrcxml_eprc_id = i.fzrcxml_eprc_id
         and fzrcxml_doc_status = 'E';
  
  /* This will resubmit the request.  
  The new inv trans date will be the system date and the new payment due will be the greater 
  between the trans date and the invoice date + numbers of days due
  */
  
    l_rsp := fzkeprc.f_request(resubmit_req,'EPUNIMARKET');
    
    update fzrcxml
      set fzrcxml_doc_status = 'X'
      where fzrcxml_doc_status = 'E'
      and fzrcxml_eprc_id  = i.fzrcxml_eprc_id;
    
  end loop;
  
 exception 
   when others then dbms_output.put_line(sqlerrm);
 end;
 