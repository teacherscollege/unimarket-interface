CREATE OR REPLACE PACKAGE        "FZKEPRC_EX" IS
/*******************************************************************************

   NAME:       FZKEPRC_EX

   PURPOSE:    This package is Unimarket expenses upload.

               Procedure p_ExpenseValidate gets
                           completes and validates FZRGURA table rows
               Procedure p_InvoiceExpenses creates invoice expenses.

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  05/14/2013  John Stark    Initial.
   
 

*******************************************************************************/

-- InvoiceDetailRequest members ------------------------------------------------
g_timestamp   VARCHAR2(14);
g_pmt_due_date DATE;
cursor g_fzrgura_c (p_bank_code VARCHAR2) 
IS
   SELECT f.*, f.ROWID   
     FROM FISHRS.FZRGURA F
   WHERE f.FZRGURA_LOAD_IND  = 'V'
     AND f.fzrgura_bank_code = NVL(p_bank_code,f.fzrgura_bank_code)
    order by FZRGURA_DOC_CODE, FZRGURA_TRAN_NUMBER;
    
TYPE g_fzrgura_tabtype IS TABLE OF g_fzrgura_c%ROWTYPE
                      INDEX BY BINARY_INTEGER; 
                      
procedure  p_ExpenseValidate(p_trans_date in DATE, p_msg out varchar2);
procedure p_InvoiceExpenses (p_bank_code varchar2, p_status out varchar2,  p_count out NUMBER);
---=============================================================================
END FZKEPRC_EX;
/


CREATE OR REPLACE PACKAGE BODY        FZKEPRC_EX IS
/*******************************************************************************

   NAME:       FZKEPRC_EX

   PURPOSE:    This package is the Unimarket expenses upload.

               Procedure p_ExpenseValidate gets
                           completes and validates FZRGURA table rows
               Procedure p_InvoiceExpenses creates invoice expenses.
   
   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20130816    J Stark       To check for duplicate expenses for the same 
                             individual, create a week ending 'W/O ' value 
                             in the invoice code field (fabinvh_code) and 
                             check for an existing invoice for this vendor
                             with the same week ending value. 
   20131004    J Stark       Set the Direct Deposit Override box in FAIINVE 
                             "unchecked" (set to 'N') AKA 
                             the FABINVH_ACH_OVERRIDE_IND AKA
                             to allow check print.
                             SET fabinvh_vend_inv_code to W/O MM/DD/YYYY
   20131031    J Stark	     update the code for Unimarket E-Expenses to 
                             pickup the oldest date submitted and use 
                             date to create the vendor invoice code 
                             that beginning with W/O.
                             Increment all supplemental expenses submitted
                             for the same week:
                                 1st Exp W/O XX/XX/XX
                                 2nd Exp W/O XX/XX/XXS
                                 3rd Exp W/O XX/XX/XXS2
                                 4th Exp W/O XX/XX/XXS3                              
         
*******************************************************************************/

function get_pidm_from_uni(p_uni VARCHAR2) RETURN NUMBER
IS 
   v_pidm  spriden.spriden_pidm%type := NULL;
BEGIN
  SELECT spriden_pidm
    INTO v_pidm
    FROM saturn.spriden
   WHERE spriden_ntyp_code = 'UNI'
     AND lower(spriden_id) = p_uni;
   RETURN v_pidm;
exception
   WHEN OTHERS THEN
       RETURN NULL;
end;

function get_banner_from_uni(p_uni VARCHAR2) RETURN VARCHAR2
IS 
   v_banner_id VARCHAR2(30) := NULL;
BEGIN
  SELECT gobeacc_username
    INTO v_banner_id
    FROM saturn.spriden, 
         general.gobeacc
   WHERE spriden_ntyp_code = 'UNI'
     AND lower(spriden_id) = p_uni
     AND spriden_pidm = gobeacc_pidm; 
   RETURN v_banner_id;
exception
   WHEN OTHERS THEN
       RETURN NULL;
end;

function get_bank_code(p_pidm NUMBER) RETURN VARCHAR2
IS 
   v_bank_code VARCHAR2(2) := '14';
BEGIN

SELECT DISTINCT '16'
  INTO v_bank_code
  FROM GXRDIRD
 WHERE GXRDIRD_PIDM = p_pidm
   AND GXRDIRD_STATUS = 'A'
   AND GXRDIRD_PERCENT = '100'
   AND GXRDIRD_AP_IND = 'A';
RETURN v_bank_code;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN '14';
  WHEN OTHERS THEN 
    RETURN '14';
END;
-- TC MOD 20131031 START ------------------------------------------------------
function get_wo_code(p_fzrgura_doc_code VARCHAR2) RETURN VARCHAR2
IS 
   v_wo_code VARCHAR2(14) := NULL;
   v_min_activity_date DATE := NULL;
BEGIN
   SELECT MIN(FZRGURA_ACTIVITY_DATE)
     INTO v_min_activity_date
     FROM FZRGURA
    WHERE FZRGURA_DOC_CODE = p_fzrgura_doc_code;
   v_wo_code := 'W/O '|| TO_CHAR(NEXT_DAY(v_min_activity_date - 7, 'SUN'),'MM/DD/YY');
   RETURN v_wo_code;
EXCEPTION
  WHEN OTHERS THEN 
    RETURN NULL;
END;

function check_dup_invoiced_expense(p_wo_code VARCHAR2, p_vend_pidm NUMBER)
   RETURN varchar2 -- invoice document code
IS 
   v_invoice_code fabinvh.fabinvh_code%type := NULL;
   v_work_wo_code fzrgura.fzrgura_wo_code%type := NULL;
   v_wo_code_index number(2,0) := NULL;
BEGIN
   SELECT MAX(FABINVH_VEND_INV_CODE) 
     INTO v_work_wo_code
     FROM FABINVH
    WHERE FABINVH_VEND_PIDM = p_vend_pidm
      AND FABINVH_VEND_INV_CODE LIKE p_wo_code ||'%';
-- NO DUPLICATE INVOICE USE BASE WO CODE
   IF v_work_wo_code IS NULL
   THEN 
      RETURN  p_wo_code;
   END IF;
-- FIRST DUPLICATE, APPEND 'S'
   IF v_work_wo_code = p_wo_code
   THEN 
      RETURN p_wo_code || 'S';
-- SECOND DUPLICATE, INCREMENT TO 'S2'      
   ELSIF v_work_wo_code = p_wo_code||'S'
   THEN 
      RETURN p_wo_code || 'S 2';
-- MORE THAN 2 DUPLICATES - INCREMENT 'SX' IF POSSIBLE        
   ELSE
     v_wo_code_index := TO_NUMBER(SUBSTR(v_work_wo_code,14)) + 1;
     RETURN  SUBSTR(v_work_wo_code,1,13) || LPAD(TRIM(TO_CHAR(v_wo_code_index,'99')),2);
   END IF;
   RETURN v_work_wo_code ||'S';
EXCEPTION
WHEN NO_DATA_FOUND THEN
   RETURN NULL;
WHEN OTHERS THEN
-- CANNOT INCREMENT APPEND 'S'  
   IF LENGTH(v_work_wo_code) < 15
   THEN
      RETURN v_work_wo_code ||'S';
   ELSE
     RETURN SUBSTR(v_work_wo_code,1,14)|| CHR(ascii(SUBSTR(v_work_wo_code,15,1))+1);
   END IF;
END;

-- TC MOD 20131031 END   ------------------------------------------------------  
function  check_for_invoice (p_invoice_code VARCHAR2)
          RETURN date
IS
v_existing_invoice_date DATE := NULL;
BEGIN
SELECT DISTINCT fabinvh_invoice_date
  INTO v_existing_invoice_date
  FROM fabinvh
 WHERE fabinvh_code = p_invoice_code;
 RETURN v_existing_invoice_date;
EXCEPTION 
WHEN NO_DATA_FOUND THEN 
   RETURN NULL;
WHEN OTHERS THEN
   RETURN NULL;    
END;
-- TC MOD 20130816 START ------------------------------------------------------       
function check_for_invoiced_expense(p_wo_date VARCHAR2, p_vend_pidm NUMBER)
   RETURN varchar2 -- invoice document code
IS 
   v_invoice_code fabinvh.fabinvh_code%type := NULL;
BEGIN
   SELECT MAX(fabinvh_code)
     INTO v_invoice_code
     FROM fabinvh
    WHERE fabinvh_vend_pidm = p_vend_pidm
      AND fabinvh_vend_inv_code = p_wo_date;
    RETURN v_invoice_code;
EXCEPTION
WHEN NO_DATA_FOUND THEN
   RETURN NULL;
WHEN OTHERS THEN
   RETURN NULL;  
END;

function check_for_duplicate_expense(p_wo_date VARCHAR2, 
                                     p_employee_uni VARCHAR2,
                                     p_doc_code VARCHAR2
                                     ) 
  RETURN varchar2 -- expense document code
IS
  v_doc_code fzrgura.fzrgura_doc_code%type := NULL;
BEGIN
   SELECT MAX(fzrgura_doc_code)
     INTO v_doc_code
     FROM fzrgura
    WHERE FZRGURA_EMPLOYEE_UNI =  p_employee_uni
      AND p_wo_date = 'W/O '|| TO_CHAR(NEXT_DAY(fzrgura_activity_date - 7, 'SUN'),'MM/DD/YY')
      AND FZRGURA_DOC_CODE  != p_doc_code;  
    RETURN v_doc_code;
EXCEPTION
WHEN NO_DATA_FOUND THEN
   RETURN NULL;
WHEN OTHERS THEN
   RETURN NULL;
END;
-- TC MOD 20130816 END   ------------------------------------------------------
procedure get_vendor_info (p_pidm         IN NUMBER,
                           p_last_name    OUT VARCHAR2,
                           p_street_line1 OUT VARCHAR2,
                           p_city         OUT VARCHAR2,
                           p_state        OUT VARCHAR2,
                           p_zip          OUT VARCHAR2,
                           p_atype_code   OUT VARCHAR2,
                           p_atype_seqno  OUT VARCHAR2,
                           p_msg          IN OUT VARCHAR2) is
cursor vendor_cursor (vendor_pidm number) IS                            
   SELECT spriden_last_name,
          spraddr_street_line1,
          spraddr_city,
          spraddr_stat_code,
          spraddr_zip,
          spraddr_atyp_code,
          spraddr_seqno        
   FROM 
       saturn.spriden,
       saturn.spraddr
   WHERE spriden_pidm = vendor_pidm
     AND spriden_change_ind IS NULL
     AND spraddr_pidm = spriden_pidm
     AND SPRADDR_ATYP_CODE IN ('W2','MA')
     AND ((spraddr_from_date IS NULL AND spraddr_to_date IS NULL)
     OR (spraddr_to_date IS NOT NULL 
         AND spraddr_from_date IS NOT NULL
         AND TRUNC(SYSDATE) BETWEEN spraddr_from_date AND spraddr_to_date)
     OR (spraddr_from_date IS NULL 
         AND spraddr_to_date IS NOT NULL
         AND spraddr_to_date >= TRUNC(SYSDATE))
     OR (spraddr_from_date IS NOT NULL
         AND spraddr_to_date IS NULL
         AND spraddr_from_date <= TRUNC(SYSDATE))
         )
   ORDER BY SPRADDR_ATYP_CODE DESC; 
   vendor_info vendor_cursor%ROWTYPE;
   
begin
    open vendor_cursor(p_pidm);
    fetch vendor_cursor into vendor_info;
    IF vendor_cursor%NOTFOUND THEN
      p_msg := 'Vendor address not found. ';  
    ELSE
      p_msg := NULL;
      p_last_name    := vendor_info.spriden_last_name;   
      p_street_line1 := vendor_info.spraddr_street_line1;
      p_city         := vendor_info.spraddr_city;        
      p_state        := vendor_info.spraddr_stat_code;   
      p_zip          := vendor_info.spraddr_zip;         
      p_atype_code   := vendor_info.spraddr_atyp_code;    
      p_atype_seqno  := vendor_info.spraddr_seqno;        
    END IF;
    close vendor_cursor;
    RETURN;
EXCEPTION when others then
   p_msg := SUBSTR('SQL ERROR'||SQLERRM,1,100);
   RETURN;
end;  
procedure get_foapal      (p_acci_code     IN VARCHAR2,
                           p_fund_code    OUT VARCHAR2,
                           p_orgn_code    OUT VARCHAR2,
                           p_prog_code    OUT VARCHAR2,
                           p_msg          OUT VARCHAR2 ) is
cursor index_cursor (acci_code VARCHAR2) IS                            
   SELECT ftvacci_fund_code,
          ftvacci_orgn_code,
          ftvacci_prog_code,
          ftvacci_nchg_date    
   FROM 
       fimsmgr.ftvacci
   WHERE FTVACCI_COAS_CODE = '1'
     AND FTVACCI_ACCI_CODE = P_ACCI_CODE
     AND FTVACCI_NCHG_DATE > TRUNC(SYSDATE)
     AND FTVACCI_STATUS_IND = 'A'
   ORDER BY FTVACCI_NCHG_DATE ASC; 
   fopal_info index_cursor%ROWTYPE;
begin
    open index_cursor(p_acci_code);
    fetch index_cursor into fopal_info;
    IF index_cursor%NOTFOUND THEN
      p_msg := 'FOPAL not found for index. ';  
    ELSE
      p_msg := NULL;
      p_fund_code := fopal_info.ftvacci_fund_code;
      p_orgn_code := fopal_info.ftvacci_orgn_code;
      p_prog_code := fopal_info.ftvacci_prog_code;
    END IF;
    close index_cursor;
    RETURN;
EXCEPTION when others then
   p_msg := SUBSTR('SQL ERROR'||SQLERRM,1,100);
   RETURN;
end;                     
--------------------------------------------------------------------------------
procedure p_ExpenseValidate(p_trans_date in DATE, p_msg out varchar2) is
  v_invoice_date DATE := NULL;
  v_msg fzrgura.fzrgura_error_msg%TYPE := NULL;
  r_msg fzrgura.fzrgura_error_msg%TYPE := NULL;   
  v_error_count NUMBER(7) := 0;
  v_timestamp   VARCHAR2(14) := TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS');   --format - YYYYMMDDHH24MISS
cursor fzrgura_c 
IS
    SELECT fishrs.fzrgura.* 
     FROM 
   FISHRS.FZRGURA
   WHERE FZRGURA_LOAD_IND IS NULL 
      OR  FZRGURA_LOAD_IND NOT IN ('V', 'I')
    FOR UPDATE;
   fzrgura_rec fzrgura_c%ROWTYPE;
   fzrgura_v   fzrgura_c%ROWTYPE;
   v_doc_code fzrgura.fzrgura_doc_code%type := NULL;
   v_invoice_code fabinvh.fabinvh_code%type := NULL;
-- TC MOD 20130816 START ------------------------------------------------------       
   v_wo_code VARCHAR2(14) := NULL;
-- TC MOD 20130816 END   ------------------------------------------------------       
begin
   for fzrgura_rec IN fzrgura_c LOOP
       fzrgura_v := fzrgura_rec;
       v_msg := NULL;
       IF fzrgura_v.fzrgura_system_time_stamp IS NULL
       THEN
          fzrgura_v.fzrgura_system_time_stamp :=  v_timestamp;
       END IF;
       fzrgura_v.fzrgura_pidm :=  get_pidm_from_uni(fzrgura_rec.fzrgura_employee_uni);
       IF fzrgura_v.fzrgura_pidm IS NULL
       THEN
          v_msg := v_msg || 'Buyer User Name - UNI not found. ';
       ELSE
/*
          fzrgura_v.fzrgura_id   :=  get_banner_from_uni(fzrgura_rec.fzrgura_employee_uni);
          IF fzrgura_v.fzrgura_id IS NULL
          THEN 
              v_msg := v_msg || 'BANNER ID not found. ';
          END IF;
*/         
          fzrgura_v.fzrgura_bank_code := get_bank_code(fzrgura_v.fzrgura_pidm);
          IF fzrgura_v.FZRGURA_BANK_CODE IS NULL
              THEN
                v_msg := v_msg || 'Bank code error. ';
          END IF;
          r_msg := NULL;
          get_vendor_info (fzrgura_v.fzrgura_pidm,
                           fzrgura_v.fzrgura_last_name,      
                           fzrgura_v.fzrgura_street_line1,   
                           fzrgura_v.fzrgura_city,           
                           fzrgura_v.fzrgura_stat_code,      
                           fzrgura_v.fzrgura_zip,            
                           fzrgura_v.fzrgura_atyp_code,      
                           fzrgura_v.fzrgura_atyp_seqno,           
                           r_msg );
          IF r_msg IS NOT NULL 
          THEN
             v_msg := v_msg || r_msg;
          END IF;
          IF fzrgura_v.fzrgura_activity_date IS NULL
          THEN
            v_msg := v_msg || ' Date of expense is missing.';
          END IF;
-- TC MOD 20130816 START ------------------------------------------------------
-- TC MOD 20131031 START ------------------------------------------------------
--          v_wo_code := get_wo_code(fzrgura_v.fzrgura_activity_date);
-- GET THE BASE W/O CODE
          v_wo_code := get_wo_code(fzrgura_v.fzrgura_doc_code);
/*         
          v_doc_code := check_for_duplicate_expense(v_wo_code,
                                     fzrgura_v.fzrgura_employee_uni,
                                     fzrgura_v.fzrgura_doc_code 
                                     );
          IF v_doc_code IS NOT NULL 
          THEN
            v_msg := v_msg || ' Expense document with the same W/O date - '||v_doc_code||' '||v_wo_code||'.';                                     
          END IF;
-- TC MOD 20130816 END   ------------------------------------------------------
*/
-- TC MOD 20131031 END   ------------------------------------------------------        
       END IF;
       v_invoice_date := check_for_invoice (fzrgura_v.fzrgura_doc_code);
       IF v_invoice_date IS NOT NULL
       THEN
          v_msg := v_msg || 'Existing Invoice '
                         ||fzrgura_v.fzrgura_doc_code 
                         ||' found. Invoice date: '
                         ||TO_CHAR(v_invoice_date,'MM/DD/YYYY')
                         ||'. ';
       END IF;
       r_msg := NULL;
       get_foapal( fzrgura_v.fzrgura_acci_code,
                   fzrgura_v.fzrgura_fund_code,
                   fzrgura_v.fzrgura_orgn_code,
                   fzrgura_v.fzrgura_prog_code,
                   r_msg);
       IF r_msg IS NOT NULL 
       THEN
          v_msg := v_msg || r_msg;
       END IF;
-- TC MOD 20131031 START ------------------------------------------------------              
/*
-- TC MOD 20130816 START ------------------------------------------------------              
       v_invoice_code := check_for_invoiced_expense(v_wo_code,
                                                    fzrgura_v.fzrgura_pidm
                                                    );
       IF v_invoice_code IS NOT NULL 
       THEN
            v_msg := v_msg || ' Existing expense invoice with same W/O date - '||v_invoice_code||'.';                                     
       END IF;
-- TC MOD 20130816 END   ------------------------------------------------------                                               
*/
-- TC MOD 20131031 END   ------------------------------------------------------                                               
       IF v_msg IS NOT NULL
       THEN 
           UPDATE FZRGURA
             SET FZRGURA_LOAD_IND  = 'E',
                 FZRGURA_ERROR_MSG = v_msg
           WHERE CURRENT OF fzrgura_c;
       ELSE
       	   fzrgura_v.fzrgura_wo_code     := v_wo_code;
       	   fzrgura_v.fzrgura_trans_date  := p_trans_date;       	   
           fzrgura_v.fzrgura_change_date := SYSDATE;
           fzrgura_v.fzrgura_load_ind    := 'V';
           fzrgura_v.fzrgura_error_msg   := NULL;
           UPDATE FZRGURA  
             SET ROW = fzrgura_v 
           WHERE CURRENT OF fzrgura_c;
       END IF;
        
   END LOOP;  
   COMMIT;
   SELECT count(*)
     INTO v_error_count
     FROM FZRGURA
    WHERE FZRGURA_LOAD_IND = 'E';
   IF v_error_count > 0
   THEN
      p_msg := 'Validation failed. Error count: '||TRIM(TO_CHAR(v_error_count,'999,999'));
      UPDATE FZRGURA                                         
         SET fzrgura_load_ind = 'E'                          
      WHERE fzrgura_load_ind = 'V'                           
        AND fzrgura_doc_code IN (SELECT fzrgura_doc_code     
                                 FROM FZRGURA                   
                                 WHERE fzrgura_load_ind = 'E'); 
      COMMIT;
   END IF;
   
exception when others then
   ROLLBACK;                  
   tcapp.util_msg.logentry(SQLERRM);   
end;
--------------------------------------------------------------------------------
procedure p_write_error_message(p_rowid IN VARCHAR2, p_doc_code IN VARCHAR2, p_msg IN OUT VARCHAR2)
IS
      PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    UPDATE FZRGURA F 
       SET F.FZRGURA_LOAD_IND = 'E', 
           F.FZRGURA_ERROR_MSG = SUBSTR(p_msg,1,200)
     WHERE ROWID = p_rowid;
    UPDATE FZRGURA F 
       SET F.FZRGURA_LOAD_IND = 'E'
     WHERE F.ROWID != p_rowid AND F.FZRGURA_DOC_CODE = p_doc_code;      
    COMMIT;
EXCEPTION
   WHEN OTHERS 
   THEN 
       p_msg := 'p_write_error_message: '||SQLERRM;
       RETURN;
END;
--------------------------------------------------------------------------------
PROCEDURE p_create_expense_invoice (p_fzrgura_tab IN g_fzrgura_tabtype, p_msg out VARCHAR2, p_warning_msg out VARCHAR2)
  IS
    v_item_count NUMBER(4,0) := 0;
    v_fzrgura_tab g_fzrgura_tabtype;
    v_fzrgura g_fzrgura_c%ROWTYPE;
    v_msg VARCHAR2(200) := NULL;
    v_current_rowid rowid;
    v_invoice_rowid rowid;
    v_seq_num farinva.farinva_seq_num%TYPE;
    v_comm_desc VARCHAR2(50);
---
    lv_ach_override_ind VARCHAR2(1);
    fabinvh_rec        fb_invoice_header.invoice_header_rec; 
    tbbdetc_row        tbbdetc%ROWTYPE;
    lv_rowid           VARCHAR2(100);
-- declare variables to store values to be inserted into farinva
  lv_rucl_code       farinva.farinva_rucl_code%TYPE;    
-- declare local variables to store error messages
  lv_error_msg           gb_common_strings.err_type;
  lv_warning_msg         gb_common_strings.err_type;
  lv_success_msg         gb_common_strings.err_type;
-- TC MOD 20130816 START ------------------------------------------------------       
-- check for duplicate expense based on W/O date
   v_invoice_code fabinvh.fabinvh_code%type := NULL;
   v_wo_code fzrgura.fzrgura_wo_code%type := NULL;      
-- TC MOD 20130816 END   ------------------------------------------------------          
-- Get the ach indicator from tbbdetc
   CURSOR tbbdetc_ach_c(detail_code tbbdetc.tbbdetc_detail_code%TYPE) IS
   SELECT * FROM tbbdetc
   WHERE tbbdetc_detail_code = detail_code
   AND tbbdetc_dird_ind = 'Y';  
   CURSOR fzrgura_acctg_c (p_doc_code fzrgura.fzrgura_doc_code%TYPE) IS
      SELECT fzrgura_coas_code,
             fzrgura_acci_code,
             fzrgura_fund_code,
             fzrgura_orgn_code,
             fzrgura_acct_code,
             fzrgura_prog_code,
             fzrgura_actv_code,
             fzrgura_locn_code,             
             SUM(fzrgura_trans_amt) AS fzrgura_trans_amt        
        FROM fzrgura
       WHERE fzrgura_doc_code = p_doc_code
       GROUP BY
             fzrgura_coas_code,
             fzrgura_acci_code,
             fzrgura_fund_code,
             fzrgura_orgn_code,
             fzrgura_acct_code,
             fzrgura_prog_code,
             fzrgura_actv_code,
             fzrgura_locn_code
       ORDER BY 
             fzrgura_coas_code,
             fzrgura_acci_code,
             fzrgura_fund_code,
             fzrgura_orgn_code,
             fzrgura_acct_code,
             fzrgura_prog_code,
             fzrgura_actv_code,
             fzrgura_locn_code;    
BEGIN 
-- Get the tax processing indicator from fobsysc
-- note: fobsysc_row.fobsysc_tax_processing_ind = 'N' (NO TAXES, NO TAX GROUP)
--       BANK CODE is set 
/*
--  Set the contexts for the 3 invoice APIs and complete process
   gb_common.p_set_context('FB_INVOICE_HEADER','BATCH_TRAN','REFUND','N');
   gb_common.p_set_context('FB_INVOICE_ITEM',  'BATCH_TRAN','TRUE','N');
   gb_common.p_set_context('FB_INVOICE_ACCTG', 'BATCH_TRAN','TRUE','N');
   gb_common.p_set_context('FP_INVOICE.P_COMPLETE', 'BATCH_TRAN_POST','TRUE','N');    
*/
   v_current_rowid := p_fzrgura_tab(1).rowid;

-- check for duplicate invoice
   BEGIN
       fabinvh_rec.r_code :=  fb_invoice_header.f_gen_validate_invoice_code(p_code => p_fzrgura_tab(1).fzrgura_doc_code,
                                                                            p_invoice_prefix_type => 'S');
   EXCEPTION
      WHEN OTHERS THEN
           v_msg := SUBSTR('DUPLICATE INVOICE: '||SQLERRM,1,180);
   END;    
   -- DUPLICATE INVOICE NUMBER
   IF v_msg IS NOT NULL
   THEN 
      p_msg := v_msg;
      RETURN;
   END IF;
-- TC MOD 20131031 START ------------------------------------------------------                 
/*
-- TC MOD 20130816 START ------------------------------------------------------          
   -- DUPLICATE W/O Expense
   v_invoice_code := check_for_invoiced_expense(p_fzrgura_tab(1).wo_date,
                                                p_fzrgura_tab(1).fzrgura_pidm
                                               );
   IF v_invoice_code IS NOT NULL 
   THEN
      p_msg := 'Existing expense invoice with the same W/O date - '||v_invoice_code||'.'; 
      RETURN;                                       
   END IF;      
-- TC MOD 20130816 END   ------------------------------------------------------          
*/
   IF p_fzrgura_tab(1).fzrgura_wo_code IS NULL  
   THEN
      p_msg := 'Missing W/O date for expense.'; 
      RETURN;          
   END IF;
   v_wo_code := check_dup_invoiced_expense(p_fzrgura_tab(1).fzrgura_wo_code, p_fzrgura_tab(1).fzrgura_pidm);
-- TC MOD 20131031 END   ------------------------------------------------------          
   -- Set the direct deposit indicator for this pidm    
   tbbdetc_row := NULL;
   IF fb_common.f_get_vendor_ap_ach_status(p_vend_pidm    => p_fzrgura_tab(1).fzrgura_pidm,
                                           p_atyp_code    => p_fzrgura_tab(1).fzrgura_atyp_code,
                                           p_atyp_seq_num => p_fzrgura_tab(1).fzrgura_atyp_seqno
                                           )
                                      IN ('A','P') 
   THEN
      OPEN tbbdetc_ach_c(p_fzrgura_tab(1).fzrgura_detail_code);
      FETCH tbbdetc_ach_c INTO tbbdetc_row;
      IF tbbdetc_ach_c%NOTFOUND THEN
         lv_ach_override_ind := 'Y';
      ELSE
         lv_ach_override_ind := 'N';
      end IF;
      CLOSE tbbdetc_ach_c;
    
   ELSE
      lv_ach_override_ind := 'N';
   END IF;
-- IF TRANS DATE IS NULL SET TO THE CURRENT DATE
   IF p_fzrgura_tab(1).fzrgura_trans_date IS NOT NULL
   THEN
      fabinvh_rec.r_trans_date:= p_fzrgura_tab(1).fzrgura_trans_date; 
    ELSE
       fabinvh_rec.r_trans_date:= TRUNC(SYSDATE);
    END IF;   
/*       
-- Get the rule class to be used  (may put this in the global area or default to "INNI")  
   lv_rucl_code := foksels.f_get_ftvsdat_data(
                           f_entity     => 'FARINVA',
                           f_attr       => 'RUCL_CODE',
                           f_code_opt_1 => 'INNI',
                           f_eff_date   =>fabinvh_rec.r_trans_date);    
*/   
-- Create the header of the invoice using the header API
      BEGIN
         fb_invoice_header.p_create(
               p_code                =>  fabinvh_rec.r_code,
--               p_code                =>  p_fzrgura_tab(1).fzrgura_doc_code,
               p_pohd_code           =>  NULL,
               p_vend_pidm           =>  p_fzrgura_tab(1).fzrgura_pidm,
               p_user_id             =>  p_fzrgura_tab(1).fzrgura_user_id,
              -- p_vend_inv_code       =>  NULL, 
-- TC MOD 20131004 START ------------------------------------------------------                                        
--               p_vend_inv_code       =>  'EXPENSE',
               p_vend_inv_code       =>  v_wo_code,            
-- TC MOD 20131004 END   ------------------------------------------------------               
               p_invoice_date        =>  fabinvh_rec.r_trans_date,
               p_pmt_due_date        =>  g_pmt_due_date,
               p_trans_date          =>  fabinvh_rec.r_trans_date,
               p_cr_memo_ind         =>  'N',               
               p_hold_ind            =>  'N',
               p_grouping_ind        =>  'M',
               p_bank_code           =>  p_fzrgura_tab(1).fzrgura_bank_code,
               p_ruiv_ind            =>  'N',
               p_edit_defer_ind      =>  'N',
               p_tgrp_code           =>  NULL,
               p_curr_code           =>  NULL, -- only base currency allowed.
               p_disc_code           =>  NULL, -- no discount
               p_atyp_code           =>  p_fzrgura_tab(1).fzrgura_atyp_code,  
               p_atyp_seq_num        =>  p_fzrgura_tab(1).fzrgura_atyp_seqno, 
               p_atyp_code_vend      =>  p_fzrgura_tab(1).fzrgura_atyp_code,
               p_atyp_seq_num_vend   =>  p_fzrgura_tab(1).fzrgura_atyp_seqno,
               p_single_acctg_ind    =>  'Y',  --  'Y' IS DOCUMENT LEVEL ACCOUNTING , 'N' IS COMMODITY LEVEL ACCOUNTING
               p_one_time_vend_name  =>  NULL,
               p_one_time_vend_addr1 =>  NULL,
               p_one_time_vend_addr2 =>  NULL,
               p_one_time_vend_addr3 =>  NULL,
               p_one_time_vend_city  =>  NULL,
               p_one_time_vend_state =>  NULL,
               p_one_time_vend_zip   =>  NULL,
               p_one_time_vend_natn  =>  NULL,
               p_ruiv_installment_ind=>  'N',
               p_multiple_inv_ind    =>  'N',
-- TC MOD 20131004 START ------------------------------------------------------               
--               p_ach_override_ind    =>  lv_ach_override_ind,
               p_ach_override_ind    =>  'N',
-- TC MOD 20131004 END    ------------------------------------------------------                              
               p_origin_code         =>  'EEXPENSE',          
               p_data_origin         =>  'Banner',
               p_rowid_out           =>  fabinvh_rec.r_internal_record_id
         );

-- Query the newly created header for later use
         fabinvh_rec := fb_invoice_header.f_query_one_rec(p_rowid => fabinvh_rec.r_internal_record_id);

      EXCEPTION
         WHEN OTHERS THEN
              v_msg := SUBSTR('INVOICE HEADER ERROR: '||SQLERRM,1,200);
      END;    
    -- ERROR AT THIS POINT IS A DB ERROR
    IF v_msg IS NOT NULL
    THEN 
       p_msg := v_msg;
       RETURN;
    END IF;
--  LOOP THROUGH EXPENSE LINES TABLE    
    FOR ix IN 1..p_fzrgura_tab.COUNT 
    LOOP
       v_item_count := ix;
       -- create invoice items
       v_comm_desc := substrb(nvl(p_fzrgura_tab(ix).fzrgura_detail_desc,p_fzrgura_tab(ix).fzrgura_desc),1,50);
       IF p_fzrgura_tab(ix).fzrgura_supplier_type IS NOT NULL
       THEN
          IF v_comm_desc IS NOT NULL
          THEN
             v_comm_desc :=  SUBSTRB(v_comm_desc || ' - ' || p_fzrgura_tab(ix).fzrgura_supplier_type,1,50); 
          ELSE
             v_comm_desc := SUBSTRB(p_fzrgura_tab(ix).fzrgura_supplier_type,1,50); 
          END IF;
       END IF;
       BEGIN
            fb_invoice_item.p_create(
                     p_invh_code         =>  fabinvh_rec.r_code,
                     p_item              => v_item_count,
                     p_user_id           => fabinvh_rec.r_user_id,
                     p_comm_desc         => v_comm_desc,
                     p_appr_qty          => 1,
                     p_appr_unit_price   => p_fzrgura_tab(ix).fzrgura_trans_amt,
                     p_hold_ind          => 'N',
                     p_data_origin       => 'EEXPENSE',
                     p_rowid_out         => lv_rowid
                 );
      
        EXCEPTION
            WHEN OTHERS THEN
                v_msg := SUBSTR('INVOICE DETAIL ERROR: '||SQLERRM,1,200);  
        END;
    END LOOP; 
    -- ERROR AT THIS POINT IS A DB ERROR
    IF v_msg IS NOT NULL
    THEN 
       p_msg := v_msg;
       RETURN;
    END IF;   
    -- LOOP THROUGH ACCOUNTING SUMMARY BUILD INVOICE ACCOUNTING LINES              
    -- create invoice accounting
    v_seq_num := 0;
    FOR fzrgura_acctg_r IN fzrgura_acctg_c(fabinvh_rec.r_code)
       LOOP
          v_seq_num := v_seq_num + 1;
          BEGIN      
             fb_invoice_acctg.p_create(
                       p_invh_code         => fabinvh_rec.r_code,
                       p_item              => 0,
                       p_seq_num           => v_seq_num,                          
                       p_user_id           => fabinvh_rec.r_user_id,
--                       p_rucl_code         => lv_rucl_code,                        
                       p_rucl_code         => 'INNI',                        
                       p_coas_code         => fzrgura_acctg_r.fzrgura_coas_code,
                       p_acci_code         => fzrgura_acctg_r.fzrgura_acci_code,
                       p_fund_code         => fzrgura_acctg_r.fzrgura_fund_code,
                       p_orgn_code         => fzrgura_acctg_r.fzrgura_orgn_code,
                       p_acct_code         => fzrgura_acctg_r.fzrgura_acct_code,
                       p_prog_code         => fzrgura_acctg_r.fzrgura_prog_code,
                       p_actv_code         => fzrgura_acctg_r.fzrgura_actv_code,
                       p_locn_code         => fzrgura_acctg_r.fzrgura_locn_code,
                       p_bank_code         => p_fzrgura_tab(1).fzrgura_bank_code,
                       p_appr_amt          => fzrgura_acctg_r.fzrgura_trans_amt,
                       p_data_origin       => 'eexpense',
                       p_rowid_out         => lv_rowid
                   );
          EXCEPTION
              WHEN OTHERS THEN
                  v_msg := SUBSTR('INVOICE ACCT ERROR: '||SQLERRM,1,200);  
          END;    
          IF v_msg IS NOT NULL
             THEN 
                EXIT;
          END IF;
    END LOOP;
    -- ERROR AT THIS POINT IS A DB ERROR
    IF v_msg IS NOT NULL
    THEN 
       p_msg := v_msg;
       RETURN;
    END IF;            
    -- complete invoice
    BEGIN
        fp_invoice.p_complete(
             p_code            => fabinvh_rec.r_code,
             p_user_id         => fabinvh_rec.r_user_id,
             p_completion_ind  => 'Y',
             p_success_msg_out => lv_success_msg,
             p_error_msg_out   => lv_error_msg,
             p_warn_msg_out    => lv_warning_msg);
    EXCEPTION
       WHEN OTHERS THEN
          v_msg := SUBSTR('INVOICE COMPLETE ERROR: '||SQLERRM,1,200);  
    END;          
       -- check for more errors
       -- set invoice records to 'I'
    
    -- ERROR AT THIS POINT IS AN INVOICE ERROR 
    -- NOTE: UPDATE WARNING MESSAGES....  
    v_msg := SUBSTR(lv_error_msg,1,200);
    IF lv_error_msg IS NOT NULL
    THEN
       p_write_error_message(p_fzrgura_tab(1).rowid, p_fzrgura_tab(1).FZRGURA_DOC_CODE, v_msg); 
       p_msg := v_msg;
       RETURN;
    END IF; 
--  NO ERRORS FLAG AS INVOICED 
    p_warning_msg := SUBSTR(lv_warning_msg,1,200);   
    UPDATE FZRGURA F 
       SET F.FZRGURA_LOAD_IND = 'I',
           F.FZRGURA_ERROR_MSG = SUBSTR(lv_warning_msg,1,200)
     WHERE F.FZRGURA_DOC_CODE = p_fzrgura_tab(1).FZRGURA_DOC_CODE;   
EXCEPTION 
  WHEN OTHERS THEN
     p_msg := SUBSTR('DB ERROR: '||SQLERRM,1,200); 
     tcapp.util_msg.logentry('INVOICE '|| p_fzrgura_tab(1).FZRGURA_DOC_CODE ||'DB ERROR: '||SQLERRM); 
     RETURN;    
END;
--------------------------------------------------------------------------------
procedure p_InvoiceExpenses (p_bank_code in VARCHAR2, p_status out VARCHAR2,  p_count out NUMBER) is
  v_count NUMBER(7) := 0; 
  v_error_msg VARCHAR2(200) := NULL;
  v_warning_msg VARCHAR2(200) := NULL;
  v_error_count NUMBER(5) := 0;
  v_warning_count NUMBER(5) := 0;
  v_doc_code FZRGURA.FZRGURA_DOC_CODE%TYPE := NULL;
  ix BINARY_INTEGER := 0;
  v_fzrgura_tab g_fzrgura_tabtype;
  v_fzrgura_empty_tab g_fzrgura_tabtype;
  c_fzrgura g_fzrgura_c%ROWTYPE;
begin
--  Set the contexts for the 3 invoice APIs and complete process
   gb_common.p_set_context('FB_INVOICE_HEADER','BATCH_TRAN','REFUND','N');
   gb_common.p_set_context('FB_INVOICE_ITEM',  'BATCH_TRAN','TRUE','N');
   gb_common.p_set_context('FB_INVOICE_ACCTG', 'BATCH_TRAN','TRUE','N');
   gb_common.p_set_context('FP_INVOICE.P_COMPLETE', 'BATCH_TRAN_POST','TRUE','N');
   v_fzrgura_tab := v_fzrgura_empty_tab;
-- Set payment due date to the next Wednesday, if today is Wedneday, 
-- Then the following Wednesday.
   g_pmt_due_date := NEXT_DAY(TRUNC(SYSDATE), 'WED');
   OPEN g_fzrgura_c(p_bank_code);
   FETCH g_fzrgura_c INTO c_fzrgura;  
   ix:= 0;
   v_fzrgura_tab := v_fzrgura_empty_tab;
   v_doc_code := c_fzrgura.fzrgura_doc_code;
   LOOP
     IF g_fzrgura_c%NOTFOUND
     THEN
       IF ix!= 0 
       THEN
           p_create_expense_invoice(v_fzrgura_tab, v_error_msg, v_warning_msg);
           v_count := v_count + 1;
           IF v_error_msg IS NOT NULL 
           THEN
             v_error_count := v_error_count + 1;
             v_error_msg := NULL;
           ELSIF v_warning_msg IS NOT NULL
           THEN
             v_warning_count := v_warning_count + 1;
             v_warning_msg := NULL;              
           END IF;
       END IF; 
       EXIT; 
     END IF;
--   At each break, build invoice.     
     IF v_doc_code != c_fzrgura.fzrgura_doc_code
     THEN 
         p_create_expense_invoice(v_fzrgura_tab, v_error_msg, v_warning_msg);
         v_count := v_count + 1;
         IF v_error_msg IS NOT NULL 
         THEN
            v_error_count := v_error_count + 1;
            v_error_msg := NULL;
         ELSIF v_warning_msg IS NOT NULL
         THEN
            v_warning_count := v_warning_count + 1;
            v_warning_msg := NULL;             
         END IF;
         ix:= 0;
         v_fzrgura_tab := v_fzrgura_empty_tab;
         v_doc_code := c_fzrgura.fzrgura_doc_code;
     END IF;
     ix:= ix + 1;
     v_fzrgura_tab(ix) := c_fzrgura;    
     FETCH g_fzrgura_c INTO c_fzrgura;    
   END LOOP;
   IF v_error_count != 0
   THEN
      gb_common.p_rollback;      
      p_count := 0;
      p_status := 'Error: Expenses not invoiced.';    
   ELSIF v_warning_count != 0
   THEN     
      gb_common.p_commit;
      p_count := v_count;
      p_status := 'Success: Expenses invoiced. '||TRIM(TO_CHAR(v_warning_count,'99,990'))||' Warnings Issued.';
   ELSE 
      gb_common.p_commit;
      p_count := v_count;
      p_status := 'Success: Expenses invoiced. No Warnings.'; 
   END IF;
--  Reset the contexts for the invoice APIs and complete process
   gb_common.p_set_context('FB_INVOICE_HEADER','BATCH_TRAN','');
   gb_common.p_set_context('FB_INVOICE_ITEM',  'BATCH_TRAN','');
   gb_common.p_set_context('FB_INVOICE_ACCTG', 'BATCH_TRAN','');
   gb_common.p_set_context('FP_INVOICE.P_COMPLETE', 'BATCH_TRAN_POST','');
   RETURN; 
EXCEPTION 
  WHEN OTHERS THEN
   ROLLBACK;
   p_count := 0;
   p_status := 'ERROR - '||SQLERRM;
   tcapp.util_msg.logentry(p_status);
END;


END FZKEPRC_EX;
/
