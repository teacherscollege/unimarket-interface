CREATE OR REPLACE PACKAGE        "FZKEPRC_UD" IS
/*******************************************************************************

   NAME:       FZKEPRC_UD

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_USER_DETAIL_REQUEST gets
                               <user-detail-request> 
                         and generates
                               <user-detail-response>

   NOTES:      Procedure P_USER_DETAIL_REQUEST called from FZKEPRC

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/20/2011  S.Vorotnikov  Initial (moved from FZKEPRC).

*******************************************************************************/

procedure p_user_detail_request(p_rsp in out CLOB, p_msg in out varchar2);

---=============================================================================
END FZKEPRC_UD;
/


CREATE OR REPLACE PACKAGE BODY        FZKEPRC_UD IS
/*******************************************************************************

   NAME:       FZKEPRC_UD

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_USER_DETAIL_REQUEST gets
                               <user-detail-request>
                         and generates
                               <user-detail-response>

   NOTES:      Procedure P_USER_DETAIL_REQUEST called from FZKEPRC

   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20121212    J Stark       Added added bansecr qualification to select
   20130325    J Stark       Modified: do not send default FOAPAL. 
                             Changed to receive and process UNI (which is  
                             the Unimarket ID). Added generic 
                             p_generate_default_response for non-Banner 
                             users.  It will give them BROWSER and 
                             EXPENSES roles.
   20130613    J Stark       Fix for user detail request SQL error:
                             ORA-01422: exact fetch returns more than 
                             requested number of rows.  There are multiple 
                             goremal_email_addresses that have the "preferred"
                             indicator set.  Created a function,
                             tc_get_email_address(), so that only one
                             email address will be returned, added it to
                             select in procedures p_generate_default_response 
                             and p_generate_response.
   20131025    J Stark       Remove from p_generic_default_response the 
                             default BROWSER role.
   20131209    J Stark       Modify p_generic_default_response to give the
                             'UM_RFX_COLLABORATION_CREATE' role this role
                             is very limited.                             
   20140124    J Stark       Set email address as uni + @tc.columbia.edu 
   20140506    J Stark       Send email if this is a new user. Get Org code
                             for new user. 
   20140509    J Stark       Give new users BROWSER and EXPENSE roles.  
   20140627    J Stark	     Give new users BROWSER role only.                                           
   20140716    J Stark       ADDED GLOBAL g_umkt_sysid (PROD/DEMO) 
                             FOR EMAILS
   20140907    J Stark	     Give new users CREATE BLANKET ORDER in addition to 
                             BROWSER role.                                

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/20/2011  S.Vorotnikov  Initial (moved from FZKEPRC).

   2.0  10/13/2011  S.Vorotnikov  Get value for organisation-code
   2.1  01/09/2012  S.Vorotnikov  Get value for format-ref attribute
                                   of the xml element account-code
   2.2  02/14/2012  S.Vorotnikov  Changed to where pidm = to_char(s.pidm)
   2.3  03/22/2012  S.Vorotnikov  Main select statement changed:
                                    removed: FTVSHIP
                                      added: FOBPROF_REQUESTER_ORGN_CODE IS NOT NULL
*******************************************************************************/
--
-- Request XML elements
   tenant_ref_domain   varchar2(100);
   tenant_ref_identity varchar2(100);
--
-- PL*SQL tables for user default FOAPAL
   type   PART_type   is table of varchar2(100) index by pls_integer;
   type   FOAPAL_type is table of varchar2(100) index by varchar2(100);
   PART   PART_type;
   FOAPAL FOAPAL_type;
--
   l_format_code ftvsdat.ftvsdat_sdat_code_opt_1%type;
   l_nsp varchar2(200) := fzkeprc.g_namespace;
   l_rsp clob := '<?xml version="1.0" encoding="UTF-8"?>';
--
   l_err varchar2(4000);
   l_xml_out xmltype;
   l_num pls_integer;
-- TC MOD 20140506 START ------------------------------------------------------
-- org code
   l_org_code ftvorgn.ftvorgn_orgn_code%TYPE;
-- TC MOD 20140506 END   ------------------------------------------------------   
--------------------------------------------------------------------------------
procedure validate_FOAPAL_part(p_part varchar2) is
   l_tmp varchar2(1000);
begin
   if p_part = 'COAS' then
      ffkfoap.p_validate_COAS(sysdate,FOAPAL(p_part),l_tmp);
   else execute immediate
     'begin '||
        'ffkfoap.p_validate_'||p_part||'(:dt,:coas,:val,:err);'||
     'end;'
      using sysdate, FOAPAL('COAS'), FOAPAL(p_part), in out l_tmp;
   end if;
   if l_tmp is not null then
      l_err := l_err||l_tmp||chr(10);
   end if;
end;
--------------------------------------------------------------------------------
function tc_uni_to_banner(p_uni VARCHAR2) RETURN VARCHAR2
IS 
   v_banner_id VARCHAR2(30) := NULL;
BEGIN
  SELECT gobeacc_username
    INTO v_banner_id
    FROM spriden, gobeacc
   WHERE spriden_ntyp_code = 'UNI'
     AND lower(spriden_id) = p_uni
     AND spriden_pidm = gobeacc_pidm; 
   RETURN v_banner_id;
exception
   WHEN OTHERS THEN
       RETURN NULL;
end;
function tc_get_user_name(p_uni VARCHAR2) RETURN VARCHAR2
IS
	v_user_name VARCHAR2(60) := NULL;
BEGIN
  SELECT spriden_first_name ||' '|| spriden_last_name ||' ('|| p_uni||')'
    INTO v_user_name
    FROM spriden
   WHERE spriden_ntyp_code = 'UNI'
     AND lower(spriden_id) = p_uni; 
   RETURN v_user_name;
EXCEPTION
   WHEN OTHERS THEN
       RETURN NULL;   	
END;
-- TC MOD 20140506 START ------------------------------------------------------
-- GET TIME SHEET ORG CODE
function p_get_user_org_ts(p_userid VARCHAR2) 
   return VARCHAR2
IS
   v_org VARCHAR2(6 BYTE) := NULL;
BEGIN
   SELECT nbrjobs_orgn_code_ts 
   INTO  v_org
   FROM
      (SELECT nbrjobs_orgn_code_ts, nbrjobs_effective_date
         FROM nbrjobs,
              spriden,
              nbrbjob
        WHERE spriden_id = UPPER(p_userid)
          AND spriden_ntyp_code = 'UNI'       
          AND spriden_pidm      = nbrjobs_pidm 
          AND nbrjobs_pidm      = spriden_pidm
          AND nbrjobs_status    = 'A'
          AND nbrjobs_suff      = '00'
          AND nbrjobs_ecls_code IS NOT NULL
          AND nbrjobs_effective_date <= TRUNC(SYSDATE)
          AND nbrjobs_pidm      = nbrbjob_pidm
          AND nbrjobs_suff      = nbrbjob_suff
          AND nbrjobs_posn      = nbrbjob_posn
          AND nbrbjob_contract_type = 'P'          
        ORDER BY nbrjobs_effective_date DESC
      ) A
   WHERE ROWNUM = 1;   
   RETURN v_org;
EXCEPTION 
 WHEN NO_DATA_FOUND THEN
      RETURN NULL;  
 WHEN OTHERS THEN
      RETURN NULL;  
END; 
--------------------------------------------------------------------------------
-- GET HOME ORG CODE
FUNCTION p_get_user_org_home(p_userid VARCHAR2) 
   RETURN   VARCHAR2
IS
	     v_org VARCHAR2(6 BYTE) := NULL;
BEGIN	     
   SELECT pebempl_orgn_code_home
     INTO v_org
     FROM pebempl,
          spriden
    WHERE spriden_id = UPPER(p_userid)
      AND spriden_ntyp_code = 'UNI'       
      AND spriden_pidm      = pebempl_pidm;
   RETURN v_org;
EXCEPTION 
 WHEN NO_DATA_FOUND THEN
      RETURN NULL;  
 WHEN OTHERS THEN
      RETURN NULL;  
END;  
--------------------------------------------------------------------------------
-- EMAIL PURCHASING IF THIS IS A NEW USER
procedure p_check_new_user (p_userid     varchar2,
                            p_orgn       varchar2 default null 
                            ) is
   PRAGMA AUTONOMOUS_TRANSACTION;
   l_from varchar2(100);
   l_subj varchar2(500);
   l_to varchar2(32000);
   l_cc varchar2(32000);
   v_msg varchar2(2000);
   crlf VARCHAR2( 2 ):= CHR( 13 ) || CHR( 10 );
   v_mail_successful VARCHAR2(300) := 'T' ; 
   v_mail_errmsg     VARCHAR2(300) := 'F' ;     
   v_new_user        VARCHAR2(9) := NULL;
   v_user_last_name  VARCHAR2(50);
   v_user_first_name VARCHAR2(50);
   v_orgn_title      VARCHAR2(50) := NULL;

begin
-- 
   BEGIN
     SELECT spriden_last_name, 
            spriden_first_name,
            fzbuser_user_id 
       INTO v_user_last_name,
            v_user_first_name,
            v_new_user
       FROM fzbuser,
            spriden
      WHERE spriden_id = upper(p_userid)
        and spriden_ntyp_code = 'UNI'
        and lower(spriden_id) = fzbuser_user_id(+);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
           v_new_user := NULL;
   END;
   IF v_new_user IS NOT NULL
   THEN 
      RETURN;
   END IF;
 
   l_subj := 'Unimarket: New User - '
             ||v_user_last_name
             ||', '
             ||v_user_first_name;
   v_msg := 'First time log in on '|| to_char(sysdate,'MM/DD/YYYY')||' for'||crlf
            || 'Name: '||v_user_first_name ||' '||v_user_last_name || crlf
            || 'User ID: '|| p_userid || crlf
            || 'Email: '||p_userid|| '@tc.columbia.edu'|| crlf;
   IF p_orgn IS NOT NULL
   THEN
      BEGIN
          SELECT ' - ' || ftvorgn_title 
            INTO v_orgn_title
            FROM ftvorgn
           WHERE ftvorgn_orgn_code =  p_orgn
            AND ftvorgn_coas_code = '1'
            AND ftvorgn_nchg_date > TRUNC(SYSDATE);
      EXCEPTION
           WHEN OTHERS THEN
                v_orgn_title := NULL;                      
      END;     
      v_msg := v_msg || 'Home Org Code: ' ||p_orgn || v_orgn_title || crlf;   
   END IF;               
   v_msg := v_msg || crlf || 'Welcome, '||v_user_first_name || ' ' || v_user_last_name ||', to Unimarket!';                  
--
   for e in(select ftvsdat_title email, ftvsdat_code_level code_level  
              from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and upper(ftvsdat_sdat_code_attr) =  'UM NEWUSER EMAIL'
               and upper(ftvsdat_sdat_code_opt_1) = 'FROM'
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level
            ) 
      loop
         l_from := e.email;
         for t in(select ftvsdat_title email 
                    from ftvsdat
                   where ftvsdat_sdat_code_entity = 'FZKEPRC'
                     and upper(ftvsdat_sdat_code_attr) =  'UM NEWUSER EMAIL'
                     and upper(ftvsdat_sdat_code_opt_1) = 'TO'
                     and ftvsdat_status_ind = 'A'
                     and ftvsdat_eff_date <= sysdate
                     and nvl(ftvsdat_term_date, sysdate+1) > sysdate
                     and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
                     and ftvsdat_code_level = e.code_level
                 )
            loop
          --      tcapp.util_msg.LogEntry('New User Send Mail To: '||t.email);
                if l_from is not null and t.email is not null then            
                   sokemal.p_sendemail (email_addr      => t.email,                   
                                        email_to_name   => NULL,
                                        email_from_addr => l_from,
                                        email_from_name => 'Unimarket '||fzkeprc.g_umkt_sysid||' - NO REPY',
                                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                                        email_subject   => l_subj,
                                        email_message   => v_msg,
                                        email_success   => v_mail_successful,
                                        email_reply     => v_mail_errmsg
                                       ); 
                else
 --                  dbms_output.put_line('ERROR in p_send_error_email: Can not send email!!!'); 
                   tcapp.util_msg.LogEntry('ERROR in p_send_new_user_email: Can not send email!!!');                                                                      
                end if;
             end loop;   
   end loop; -- end email loop
   
   INSERT INTO FZBUSER (fzbuser_activity_date, fzbuser_user_id)
   VALUES (sysdate, p_userid);
   COMMIT;
--
exception when others then
   dbms_output.put_line('ERROR in p_send_new_user_email: '||SQLERRM);
end;
-- TC MOD 20140506 END   ------------------------------------------------------  
--------------------------------------------------------------------------------
function p_invalid_user_response return clob is
   l_rsp clob;
begin
-- Generate invalid user response
   select l_rsp||
          xmlelement("user-detail-response",
             xmlattributes(fzkeprc.g_xmlns as "xmlns"),
             xmlelement("tenant-ref",
                xmlattributes(tenant_ref_domain   as "domain",
                              tenant_ref_identity as "identity")),
             xmlelement("user-detail",
                xmlelement("unimarket-user", fzkeprc.g_user),
                xmlelement("first-name", 'unknown'),
                xmlelement("last-name",  'unknown'),
                xmlelement("enabled", 'false'))).getclobval()
   into l_rsp from dual;
   return l_rsp;
end p_invalid_user_response;
--------------------------------------------------------------------------------
procedure p_generate_response is
begin
   select xmlelement("user-detail-response",
             xmlattributes(fzkeprc.g_xmlns as "xmlns"),
             xmlelement("tenant-ref",
                xmlattributes(tenant_ref_domain   as "domain",
                              tenant_ref_identity as "identity")),
--<<         user-detail
             xmlelement("user-detail",
                xmlelement("unimarket-user", fzkeprc.g_user),
                xmlelement("first-name",     fname),
                xmlelement("last-name",      lname),
--<<            address
                decode(email||phone, null, null,
                xmlelement("address",
                   decode(email, null, null,
                   xmlelement("email", email)),
                   decode(phone,null,null,
                   xmlelement("phone", phone)))),
-->>            enabled
                xmlelement("enabled", 'true'),
--<<            roles
                xmlelement("roles",
                  (select xmlagg(xmlelement("role-ref",
                             xmlattributes(ftvsdat_title as "code")))
                     from bansecr.gurucls, ftvsdat                        -- 20121212 added bansecr.
                    where gurucls_userid = upper(fzkeprc.g_user)
                      and ftvsdat_sdat_code_attr = gurucls_class_code
                      and ftvsdat_sdat_code_entity = 'FZKEPRC'
                      and ftvsdat_sdat_code_opt_1 = 'UMROLES'
                      and ftvsdat_status_ind = 'A'
                      and ftvsdat_eff_date <= sysdate
                      and nvl(ftvsdat_term_date, sysdate+1) > sysdate
                      and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate)),
--<<            organisation-units
                xmlelement("organisation-units",
                   xmlelement("organisation-unit-ref",
                      xmlattributes(fzkeprc.g_eprf_rec.fzreprf_instu_ref as "organisation-code",
                                                                    orgn as "code"))),
--<<            groups
                xmlelement("groups",
                  (select xmlagg(xmlelement("group-ref",
                                    xmlattributes(v.gorsdav_value.accessVarchar2() as "code")))
                     from gorsdav v
                    where gorsdav_table_name = 'SPRIDEN'
                      and gorsdav_attr_name  = 'EP_BUYER_GROUP'
-- TC MOD 20130325 START ------------------------------------------------------
                      and gorsdav_pk_parenttab = to_char(pidm)))
/*                      and gorsdav_pk_parenttab = to_char(pidm))),
--<<            account-codes
                xmlelement("account-codes",
                   xmlelement("account-code",
                      xmlattributes('true' as "default",
                             l_format_code as "format-ref"),
                     (select xmlagg(xmlelement("part", foapal_value)) x
                        from(select pidm, part_order, foapal_value
                               from fzvepfp order by part_order)
                        where pidm = to_char(s.pidm))))
*/                        
                        ))
-- TC MOD 20130325 END   ------------------------------------------------------                        
        into l_xml_out
-- TC MOD 20130613 START ------------------------------------------------------
        from(select spriden_pidm       pidm,
                    spriden_last_name  lname,
                    spriden_first_name fname,
                    trim(fobprof_requestor_phone_area||' '||
                         fobprof_requestor_phone_number) phone,
-- TC MOD 20140124 START ------------------------------------------------------                         
--                    tc_get_email_address(spriden_pidm) email,
                    fzkeprc.g_uni||'@tc.columbia.edu' email,
-- TC MOD 20140124 END   ------------------------------------------------------                                      
                    fobprof_requester_orgn_code  orgn
               from gobeacc, 
                    fobprof, 
                    spriden
              where gobeacc_username = upper(fzkeprc.g_user)
                and gobeacc_username = fobprof_user_id
                and spriden_pidm     = gobeacc_pidm
                and spriden_change_ind is null
                and fobprof_requester_orgn_code is not null
                ) s;
/*
        from(select spriden_pidm       pidm,
                    spriden_last_name  lname,
                    spriden_first_name fname,
                    trim(fobprof_requestor_phone_area||' '||
                         fobprof_requestor_phone_number) phone,
                    goremal_email_address email,
                    fobprof_requester_orgn_code  orgn
               from gobeacc, fobprof, spriden, general.goremal
              where gobeacc_username = upper(fzkeprc.g_user)
                and gobeacc_username = fobprof_user_id
                and spriden_pidm     = gobeacc_pidm
                and spriden_change_ind is null
                and fobprof_requester_orgn_code is not null
                and spriden_pidm = goremal_pidm(+)
                and goremal_status_ind    = 'A'
                and goremal_preferred_ind = 'Y'
                ) s;                
*/                
-- TC MOD 20130613 END   ------------------------------------------------------                
-->>>
end;
-- TC MOD 20130325 START ------------------------------------------------------
procedure p_generate_default_response is
begin
      select xmlelement("user-detail-response",
             xmlattributes(fzkeprc.g_xmlns as "xmlns"),
             xmlelement("tenant-ref",
                xmlattributes(tenant_ref_domain   as "domain",
                              tenant_ref_identity as "identity")),
--<<         user-detail
             xmlelement("user-detail",
                xmlelement("unimarket-user", fzkeprc.g_uni),
                xmlelement("external-user",fzkeprc.g_user),
                xmlelement("first-name",     fname),
                xmlelement("last-name",      lname),
--<<            address
                decode(email||phone, null, null,
                xmlelement("address",
                   decode(email, null, null,
                   xmlelement("email", email)),
                   decode(phone,null,null,
                   xmlelement("phone", phone)))),
-->>            enabled
                xmlelement("enabled", 'true'),
--<<            roles
                xmlelement("roles",
                  (select xmlagg(xmlelement("role-ref",
                          xmlattributes(ftvsdat_title as "code")))
                     from ftvsdat  
                     
-- TC MOD 20140907 START ------------------------------------------------------                                           
                      where ftvsdat_sdat_code_attr IN ('UM_BROWSER','UM_BLANKET_ORDER_CREATE')
-- TC MOD 20140907 END   ------------------------------------------------------                                           
-- TC MOD 20140627 START ------------------------------------------------------                                           
--                      where ftvsdat_sdat_code_attr IN ('UM_BROWSER')
-- TC MOD 20140509 START ------------------------------------------------------                                           
--                    where ftvsdat_sdat_code_attr IN ('UM_EXPENSES')
-- TC MOD 20140509 END --   where ftvsdat_sdat_code_attr IN ('UM_RFX_COLLABORATION_CREATE')
-- TC MOD 20140627 END   ------------------------------------------------------
-- TC MOD 20131209 END   ------------------------------------------------------                                                               
-- TC MOD 20131025 END   ------------------------------------------------------
                      and ftvsdat_sdat_code_entity = 'FZKEPRC'
                      and ftvsdat_sdat_code_opt_1 = 'UMROLES'
                      and ftvsdat_status_ind = 'A'
                      and ftvsdat_eff_date <= sysdate
                      and nvl(ftvsdat_term_date, sysdate+1) > sysdate
                      and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate)),
--<<            organisation-units
                xmlelement("organisation-units",
                   xmlelement("organisation-unit-ref",
                      xmlattributes(fzkeprc.g_eprf_rec.fzreprf_instu_ref as "organisation-code",
                                                                    orgn as "code"))),
--<<            groups set default to GNRL
                xmlelement("groups",
                  (select xmlagg(xmlelement("group-ref",
                                    xmlattributes('GNRL' as "code")))
                     from dual v ))
--<<            account-codes NADA
                ))
        into l_xml_out                
-- TC MOD 20130613 START ------------------------------------------------------
        from(select spriden_pidm       pidm,
                    spriden_last_name  lname,
                    spriden_first_name fname,
                    NULL phone,
-- TC MOD 20140124 START ------------------------------------------------------                         
--                    tc_get_email_address(spriden_pidm) email,
                    fzkeprc.g_uni||'@tc.columbia.edu' email,
-- TC MOD 20140124 END   ------------------------------------------------------                                 
                    'Purchasing'  orgn
               from spriden
              where lower(spriden_id)  = fzkeprc.g_uni
                and spriden_ntyp_code = 'UNI' 
                )s; 
/*
        from(select spriden_pidm       pidm,
                    spriden_last_name  lname,
                    spriden_first_name fname,
                    NULL phone,
                    goremal_email_address email,
                    'Purchasing'  orgn
               from gobeacc, 
                    spriden,
                    general.goremal
              where lower(spriden_id)  = fzkeprc.g_uni
                and spriden_pidm       = gobeacc_pidm
                and spriden_ntyp_code = 'UNI' 
                and spriden_pidm = goremal_pidm(+)
                and goremal_status_ind    = 'A'
                and goremal_preferred_ind = 'Y'
                )s; 
*/                               
-- TC MOD 20130613 END   ------------------------------------------------------                
end;
-- TC MOD 20130325 END   ------------------------------------------------------
--------------------------------------------------------------------------------
procedure p_user_detail_request(p_rsp in out CLOB, p_msg in out varchar2) is
begin
-- Get Request Data
-- TC MOD 20130325 START ------------------------------------------------------
--   fzkeprc.g_user      := fzkeprc.f_get_val('user_ref_identity', true); 
   fzkeprc.g_uni      := fzkeprc.f_get_val('user_ref_identity', true);   
   fzkeprc.g_user     := tc_uni_to_banner(fzkeprc.g_uni);  
-- TC MOD 20130325 END   ------------------------------------------------------
-- TC MOD 20140506 START ------------------------------------------------------
   l_org_code         :=  p_get_user_org_ts(fzkeprc.g_uni);
-- TC MOD 20140506 END   ------------------------------------------------------
   tenant_ref_domain   := fzkeprc.f_get_val('tenant_ref_domain');
   tenant_ref_identity := fzkeprc.f_get_val('tenant_ref_identity');
--
-- Get value for format-ref attribute of the xml element account-code
-- TC MOD 20130325 START ------------------------------------------------------                        
/*   select ftvsdat_sdat_code_opt_1
     into l_format_code
     from ftvsdat
    where ftvsdat_sdat_code_entity = 'FZKEPRC'
      and ftvsdat_sdat_code_attr   = 'CODE-FORMAT'
      and ftvsdat_status_ind       = 'A'
      and ftvsdat_eff_date <= sysdate
      and nvl(ftvsdat_term_date, sysdate+1) > sysdate
      and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
*/      
-- TC MOD 20130325 END   ------------------------------------------------------                        
--
-- Generate response
-- TC MOD 20130325 START ------------------------------------------------------
--      p_generate_response;
   IF fzkeprc.g_user IS NOT NULL 
   THEN
--      p_generate_response;
      p_generate_default_response;
   ELSE
      p_generate_default_response;
   END IF; 
-- TC MOD 20130325 END   --------------------------------------------------------
-- Delete empty groups
   select existsNode(l_xml_out,'//groups/group-ref',l_nsp) into l_num
     from dual;
   if l_num = 0 then
      select deletexml(l_xml_out,'//groups',l_nsp) into l_xml_out from dual;
   end if;
--
-- Check if user has everything set up properly
   select existsNode(l_xml_out,'//roles/role-ref',    l_nsp)+
          existsNode(l_xml_out,'//account-code/part', l_nsp) into l_num
     from dual;
-- TC MOD 20130325 START ------------------------------------------------------                        
--   if l_num < 2 then
   if l_num < 1 then                           
--      l_err := 'User '||fzkeprc.g_user||' does not have role or default FOAPAL.';
      l_err := 'User '|| tc_get_user_name(fzkeprc.g_uni) ||' does not have a role.';
-- TC MOD 20130325 END   ------------------------------------------------------        
      fzkeprc.p_send_error_email(l_err);
      p_rsp := p_invalid_user_response;
      return;                                                       -- return!!!
   end if;

--
--------------------------------------------------------------------------------
-- Validate user default FOAPAL
--
-- Get user default FOAPAL values from account-code/part xml elements and
-- using FOAPAL parts defined in FTVSDAT (FZKEPRC/CODE-FORMAT-PART)
-- TC MOD 20130325 START ------------------------------------------------------
/*  
   for r in(select ftvsdat_sdat_code_opt_1 part,
                   ftvsdat_code_level ordr,
                   ExtractValue(l_xml_out,'//account-code/part['||
                                          ftvsdat_code_level||']', l_nsp) val
              from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and ftvsdat_sdat_code_attr   = 'CODE-FORMAT-PART'
               and ftvsdat_status_ind       = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level)
   loop FOAPAL(r.part):=r.val; PART(r.ordr):=r.part; end loop;
--
-- Validate all the user default FOAPAL parts
   for t in PART.first .. PART.last loop validate_FOAPAL_part(PART(t)); end loop;
--
-- Check if there is any FOAPAL error
   if l_err is not null then
      l_err := 'User '||fzkeprc.g_user||' has invalid default FOAPAL '||chr(10)||l_err;
      fzkeprc.p_send_error_email(l_err);
      p_rsp := p_invalid_user_response;
      return;                                                       -- Return!!!
   end if;
*/   
-- TC MOD 20130325 END   ------------------------------------------------------     
--------------------------------------------------------------------------------
-- Everything is OK
   p_rsp := l_rsp||l_xml_out.getclobval();
-- TC MOD 20140506 START ------------------------------------------------------
   p_check_new_user(fzkeprc.g_uni,l_org_code);
-- TC MOD 20140506 END   ------------------------------------------------------   
--
--------------------------------------------------------------------------------
exception
   when no_data_found then
--    Return invalid user response
      l_err := 'User '|| tc_get_user_name(fzkeprc.g_uni) || ' is not setup properly in GOBEACC, FOBPROF or SPRIDEN';
      fzkeprc.p_send_error_email(l_err);
      p_rsp := p_invalid_user_response;
   when others then
      if SQLCODE=-20000 then
         p_msg := substr(sqlerrm, 12);
      else
         p_msg := 'p_user_detail_request: '||sqlerrm;
      end if;
end p_user_detail_request;
--
--==============================================================================
--
END FZKEPRC_UD;
/
