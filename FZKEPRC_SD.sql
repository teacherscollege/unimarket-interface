CREATE OR REPLACE PACKAGE        FZKEPRC_SD IS
/*******************************************************************************

   NAME:       FZKEPRC_SD

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_VALIDATE called from Banner form SDE Window,
               validates EP User Default FOAPAL and Buyer Group

   NOTES:      Procedure P_VALIDATE called from Banner form SDE Window
               by SQL Process Rule EP_USER_DEF

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/26/2011  S.Vorotnikov  Initial

*******************************************************************************/

function p_validate(sde_form      varchar2,
                    sde_table     varchar2,
                    sde_attribute varchar2,
                    sde_value     varchar2,
                    sde_pk        varchar2) return varchar2;

---=============================================================================
END FZKEPRC_SD;
/


CREATE OR REPLACE PACKAGE BODY        FZKEPRC_SD IS
/*==============================================================================

   NAME:       FZKEPRC_SD

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_VALIDATE called from Banner form SDE Window,
               validates EP User Default FOAPAL and Buyer Group

   NOTES:      Procedure P_VALIDATE called from Banner form SDE Window
               by SQL Process Rule EP_USER_DEF

   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20121212    J Stark       Added added bansecr qualification to select
   
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/26/2011  S.Vorotnikov  Initial

==============================================================================*/
--
function p_validate(sde_form      varchar2,
                    sde_table     varchar2,
                    sde_attribute varchar2,
                    sde_value     varchar2,
                    sde_pk        varchar2) return varchar2 is
   s varchar2(100);
begin
--
-- Check User Authorization
   begin
      select 'Y' into s
-- TC MOD 20130411 START ------------------------------------------------------ 
        from ftvsdat, bansecr.gurucls                             -- 20121212 added bansecr.
-- TC MOD 20130411 END   ------------------------------------------------------         
       where ftvsdat_sdat_code_entity = 'FZKEPRC'
         and FTVSDAT_SDAT_CODE_ATTR = 'SDE_EP_USER_DATA_SEC_CLASS'
         and gurucls_userid = user
         and ftvsdat_title = gurucls_class_code
         and ftvsdat_status_ind = 'A'
         and ftvsdat_eff_date <= sysdate
         and nvl(ftvsdat_term_date, sysdate+1) > sysdate
         and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
   exception when no_data_found then
      s := '*ERROR* User '||user||' not authorized to change EP data.';
      return s;
   end;
--
-- Check the Form
   begin
      select 'Y' into s
        from ftvsdat
       where ftvsdat_sdat_code_entity = 'FZKEPRC'
         and FTVSDAT_SDAT_CODE_ATTR = 'SDE_EP_USER_DATA_FORM'
         and ftvsdat_title = sde_form
         and ftvsdat_status_ind = 'A'
         and ftvsdat_eff_date <= sysdate
         and nvl(ftvsdat_term_date, sysdate+1) > sysdate
         and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
   exception when no_data_found then
      s := '*ERROR* EP Data not updatable in '||sde_form||'.';
      return s;
   end;
--
   if sde_attribute like 'EP_DEF____' then
-- Validate EP User default FOAPAL part
      begin
         select 'Y' into s from FZVEPRC
          where fzveprc_fmt_prt = substr(sde_attribute,-4)
            and fzveprc_picklist_prt = sde_value
            and rownum=1;
      exception when no_data_found then
         s := '*ERROR* '||substr(sde_attribute,-4)||' Code "'||sde_value||'" not found.';
         return s;
      end;
   elsif sde_attribute = 'EP_BUYER_GROUP' and sde_value is not null then
-- Validate EP User default Buyer Group
      begin
         select 'Y' into s
           from ftvsdat
          where ftvsdat_sdat_code_entity = 'FZKEPRC'
            and FTVSDAT_SDAT_CODE_ATTR   = 'UM BUYER GROUP'
            and ftvsdat_sdat_code_opt_1  = sde_value
            and ftvsdat_status_ind = 'A'
            and ftvsdat_eff_date <= sysdate
            and nvl(ftvsdat_term_date, sysdate+1) > sysdate
            and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
      exception when no_data_found then
         s := '*ERROR* Buyer Group Code "'||sde_value||'" not found.';
         return s;
      end;
-- Check if user already has this Buyer Group
      begin
         select '*ERROR* User already has Buyer Group Code "'||sde_value||'".' into s
           from gorsdav v
          where gorsdav_table_name = 'SPRIDEN'
            and gorsdav_attr_name  = 'EP_BUYER_GROUP'
            and gorsdav_pk_parenttab = sde_pk
            and v.gorsdav_value.accessVarchar2() = sde_value
            and rownum=1;
         return s;
      exception when no_data_found then null;
      end;
   else
-- Just in case...
      s := '*ERROR* Unknown SDE Attribute '||sde_attribute||'.';
   end if;
--
   return s;
--
end;
--
--==============================================================================
--
END FZKEPRC_SD;
/
