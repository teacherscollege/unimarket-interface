CREATE OR REPLACE PACKAGE        FZKEPRC_BD IS
/*******************************************************************************

   NAME:       FZKEPRC_BD

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_BID_REQUEST gets
                               <bid-request-request>
                         and saves XML document in FZRBIDS table

   NOTES:      Procedure P_BID_REQUEST called from FZKEPRC

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  04/13/2012  S.Vorotnikov  Initial.

*******************************************************************************/

procedure p_bid_request(p_rsp in out CLOB, p_msg in out varchar2);

---=============================================================================
END FZKEPRC_BD;
/


CREATE OR REPLACE PACKAGE BODY        FZKEPRC_BD IS
/*******************************************************************************

   NAME:       FZKEPRC_BD

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_BID_REQUEST gets
                               <bid-request-request>
                         and saves XML document in FZRBIDS table

   NOTES:      Procedure P_BID_REQUEST called from FZKEPRC

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  04/13/2012  S.Vorotnikov  Initial. (empty)

*******************************************************************************/
--==============================================================================
--
procedure p_bid_request(p_rsp in out CLOB, p_msg in out varchar2) is
begin
   null;
end p_bid_request;
--
--==============================================================================
--
END FZKEPRC_BD;
/
