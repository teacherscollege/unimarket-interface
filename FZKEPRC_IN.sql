CREATE OR REPLACE PACKAGE               "FZKEPRC_IN" IS
/*******************************************************************************

   NAME:       FZKEPRC_IN

   PURPOSE:    This package is Unimarket e-Invoice Interface.

               Procedure p_InvoiceDetailRequest gets
                               <InvoiceDetailRequest>
                         and creates Invoice calling Banner Invoice API
                               fb_invoice_header.p_create_header
                               fb_invoice_item.p_create_item
                               fb_invoice_acctg.p_create_accounting

   NOTES:      Procedure p_InvoiceDetailRequest called from FZKEPRC

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  12/06/2010  Bob Goudreau  Initial.
   1.1  07/21/2011  S.Vorotnikov  Moved from FZKEPRC package.
   1.2  08/05/2011  Bob Goudreau  Added deploymentMode
   1.3  11/08/2011  Bob Goudreau  Added g_orderID for PO number retrieval

*******************************************************************************/

-- InvoiceDetailRequest members ------------------------------------------------
   g_currency              constant varchar2(100):='currency';
   g_deploymentMode        constant varchar2(100):='deploymentMode';
   g_invoiceDate           constant varchar2(100):='invoiceDate';
   g_InvoiceDetailDiscount constant varchar2(100):='InvoiceDetailDiscount';
   g_InvoiceDetailItem     constant varchar2(100):='InvoiceDetailItem';
   g_InvoiceDetailShipping constant varchar2(100):='InvoiceDetailShipping';
   g_InvoiceDetailTax      constant varchar2(100):='InvoiceDetailTax';
   g_invoiceID             constant varchar2(100):='invoiceID';
   g_invoicePurpose        constant varchar2(100):='invoicePurpose';
   g_Item_lineNumber       constant varchar2(100):='Item_lineNumber';
   g_Item_lineNumber_ref   constant varchar2(100):='Item_lineNumber_ref';
   g_Item_quantity         constant varchar2(100):='Item_quantity';
   g_Item_SubtotalAmount   constant varchar2(100):='Item_SubtotalAmount';
   g_Item_UnitOfMeasure    constant varchar2(100):='Item_UnitOfMeasure';
   g_Item_UnitPrice        constant varchar2(100):='Item_UnitPrice';
   g_orderID               constant varchar2(100):='orderID';
   g_ShippingAmount        constant varchar2(100):='ShippingAmount';
   g_SubtotalAmount        constant varchar2(100):='SubtotalAmount';
   g_InvoiceDocRef         constant varchar2(100) := 'InvoiceDocRef';


procedure p_InvoiceDetailRequest(p_rsp in out CLOB, p_msg in out varchar2);

---=============================================================================
END FZKEPRC_IN;
/


CREATE OR REPLACE PACKAGE BODY                                                                                                                                                    FZKEPRC_IN IS
/*******************************************************************************

   NAME:       FZKEPRC_IN

   PURPOSE:    This package is Unimarket e-Invoice Interface.

               Procedure p_InvoiceDetailRequest gets
                               <InvoiceDetailRequest>
                         and creates Invoice calling Banner Invoice API
                               fb_invoice_header.p_create_header
                               fb_invoice_item.p_create_item
                               fb_invoice_acctg.p_create_accounting

   NOTES:      Procedure p_InvoiceDetailRequest called from FZKEPRC

   
   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20130910    J Stark       NEW FUNCTION FOR FOR API ERROR, DISPLAYS
                             ORDER AND INVOICE NUMBER.
   20130905    J Stark       CHANGE TO HANDLE END OF FISCAL YEAR FOR INVOICES.
                             IF PO IS IN PRIOR FISCAL YEAR, INVOICE
                             USING THE LAST DAY OF THE PRIOR FISCAL YEAR.
   20121212    J Stark       CHANGE CURRENCY CODE USD TO USA FOR BANNER
                             CHANGE EMAIL FOR TC.
   20130109    J Stark       CHANGE INVOICE EMAIL FOR MULTIPLE EMAIL 
                             ADDRESSES AND MULTIPLE LEVELS.  
   20130411    J Stark       FIX FOR DISCOUNT CODE                                  
   20130425    J Stark       ADD EMAIL PROCESS WHEN INVOICE IS IN NSF STATUS
   20140917    J Stark       WHEN INVOICE STATUS IS, "CANNOT COMPLETE                                                
                             DOCUMENT HAVING TRANSACTION IN NSF STATUS WHEN
                             APPROVALS ARE BYPASSED," - SEND INVOICE EMAIL TO
                             BUDGET.
   20160601    D Semar       Added modifications to support dual FY
   20161110    D Semar       Modified call to sokemal.p_sendemail for new parameters
   20161115    D Semar       Added "NOTICE" to the error message emailed when PO has not posted to ledgers.
   20170128    D Semar       Modifications to error handling.
   20170330    D Semar       Added extract for doc reference 
   20170414    D Semar       $0 line item fix
   20170927    D Semar       Modified p_get_fy_end_date_for_po to pull the correct end date for PO w/o enc. (ie Asset Acct)
   20180314    D Semar       Override bank code when doc reference contains 'Wire'
  
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  12/06/2010  Bob Goudreau  Initial.
   1.1  07/21/2011  S.Vorotnikov  Moved from FZKEPRC package.
   1.2  11/07/2011  Bob Goudreau  Uses new FZREPRF parameter:
                                  fzkeprf_disc_code (Discount code)
   1.3  02/21/2012  S.Vorotnikov  Response structure changed (Ticket #19427)
   1.4  03/29/2012  S.Vorotnikov  Use UTL_MAIL.SEND instead of FISHRS.MAIL_PKG.SEND

   2.0  04/27/2012  S.Vorotnikov  Added using FZREPRF_BANK_CODE as bank code in the invoice header
                                  INV_DOCNO logic changed:
                                     IF FZREPRF_INV_DOCNO = 'N' then auto-assign Banner Invoice number
                                     ELSE generate custom Banner document number:
                                           IF 'Y', then
                                             substr(FOBPROF_REQUESTER_ORGN_CODE(FOBPROF_USER_ID),1,2)||(next DOCNO)
                                          ELSE
                                             FZREPRF_INV_DOCNO||(next DOCNO)
   2.1  05/02/2012  S.Vorotnikov  Added using FZREPRF_INV_TRANS_DATE, if 'Y' then set trans date = cXML invoiceDate
   2.2  05/10/2012  S.Vorotnikov  Added if error 'PO_NOT_POSTED' then schedule ress job
   2.3  08/29/2012  S.Vorotnikov  Response structure changed (Ticket #26417)
   2.4  09/12/2012  S.Vorotnikov  Added procedure p_download_pdf
        To download Invoice PDF document we use four parameters defined in FZREPRF_INV_DIR:
           wallet    - Oracle wallet to be used in UTL_HTTP to get data over HTTPS
                         example: wallet="file:/output/data/fhs/wallet"
           directory - Oracle directory where files will be written
                         example: directory="JOBOUTPUT"
           index     - Index file name used by Xtender (BDMS)
                         example: index="bfdoc_<doc_code>.txt"
           path      - Destination path will be written into index file
                         example: path="\\yosemite\bdms_import$\FAST\<test_or_prod>\B-F-DOCS\B-F-DOCS_RF_DOCS\"
        Parameter values should be quoted by '"'
        Parameter definitions should be delimited by at least one character not equal '"'
        FZREPRF_BID_DIR can be NULL or LIKE '%wallet="%"%_%directory="%"%_%index="%"%_%path="%"%'
        Nothing will be done if FZREPRF_INV_DIR is NULL
                             or wallet is empty (wallet="")
                             or directory is empty (directory="")
        Index file will be not created if index is empty (index="")
        Text '<doc_code>' in the index file name will be replaced by INV_DOCNO
        Text '<test_or_prod>' in the path will be replaced by 'test'
               or 'prod' if current Banner instance equal to FZREPRF_PROD_DBNAME
        Example 1 (emty, nothing will be done):
                   wallet="" directory="" index="" path=""
        Example 2 (no index file):
                   wallet="file:/output/data/fhs/wallet" directory="JOBOUTPUT" index="" path=""
        Example 3 (both Xtender index and Invoice PDF files will be created in the Oracle directory JOBOUTPUT 
                   wallet="file:/output/data/fhs/wallet"
                   directory="JOBOUTPUT"
                   index="bfdoc_<doc_code>.txt"
                   path="\\yosemite\bdms_import$\FAST\<test_or_prod>\B-F-DOCS\B-F-DOCS_RF_DOCS\"

   2.5  09/18/2012  S.Vorotnikov  Allow NULL in delayed invoice DOCNO if FZREPRF_INV_DOCNO = 'N'
   2.6  09/20/2012  S.Vorotnikov  If FZREPRF_INV_DOCNO = 'N' then generate INV_DOCNO
                                     fokutil.p_gen_doc_code(3, g_inv_code);
                                  Procedure p_create_cxml_log changed to save generate INV_DOCNO
                                  before calling Banner Invoice API
                                     l_rec.fzrcxml_doc_code := g_inv_code;
         
*******************************************************************************/
--
   g_header_rec        fb_invoice_header.invoice_header_rec;
--
   g_order_ID          fzrcxml.fzrcxml_doc_id%type;
--
   eprf_disc_code      fzreprf.fzreprf_disc_code%type;      -- Default discount code
   eprf_prod_dbname    fzreprf.fzreprf_prod_dbname%type;    -- Production database name
   eprf_inv_userid     fzreprf.fzreprf_inv_userid%type;     -- Processor User ID or NULL for PO user
   eprf_inv_docno      fzreprf.fzreprf_inv_docno%type;      -- Doc Numbering method (null for automatic)
   eprf_inv_appr       fzreprf.fzreprf_inv_appr%type;       -- If 'Y' then send invoice to approvals
   eprf_inv_cmpl       fzreprf.fzreprf_inv_cmpl%type;       -- If 'Y' then complete invoice
   eprf_inv_send       fzreprf.fzreprf_inv_email_send%type; -- If 'Y' then send invoice email
   eprf_inv_email      fzreprf.fzreprf_inv_email_text%type; -- Invoice email body text template
   eprf_inv_trans_date fzreprf.fzreprf_inv_trans_date%type; -- If 'Y' then use vendor invoice date
   eprf_inv_dir        fzreprf.fzreprf_inv_dir%type;        -- If NOT NULL then download PDF document
--
   g_inv_sum_shipping  fabinvh.fabinvh_addl_chrg_amt%type;
   g_cr_memo_ind       fabinvh.fabinvh_cr_memo_ind%type;
   g_pmt_due_date      fabinvh.fabinvh_pmt_due_date%type;
--
   g_pohd_code         fpbpohd.fpbpohd_code%type;
   g_single_acctg_ind  fpbpohd.fpbpohd_single_acctg_ind%type;
   g_pohd_trans_date   fpbpohd.fpbpohd_trans_date%type;
--
   g_inv_user          fabinvh.fabinvh_user_id%type;
   g_inv_code          fabinvh.fabinvh_code%type;
   g_vend_inv_code     fabinvh.fabinvh_vend_inv_code%type;
   g_invoice_date      fabinvh.fabinvh_invoice_date%type;
   g_trans_date        fabinvh.fabinvh_trans_date%type;
--
   g_vend_pidm         fpbpohd.fpbpohd_vend_pidm%type;
   g_user_id           fobprof.fobprof_user_id%type;
   g_user_name         fobprof.fobprof_user_name%type;
   g_user_coas         fobprof.fobprof_coas_code%type;
   g_inv_prefix        varchar2(2);
   g_req_orgn          varchar2(2);
-- TC MOD 20140917 START ------------------------------------------------------    
   g_nsf_appr_byp      char(1) := 'N';
-- TC MOD 20140917 END   ------------------------------------------------------ 
--
-- TC MOD 20170330 START ------------------------------------------------------    
  g_invoice_doc_ref varchar2(100) := fzkeprc.f_get_val(g_invoiceDocRef, false);
  g_invoice_doc_ref_fy varchar2(2);
-- TC MOD 20170330 END   ------------------------------------------------------ 

-- TC MOD 20180314 START ------------------------------------------------------    
  g_bank_code varchar2(2);
-- TC MOD 20180314 END   ------------------------------------------------------ 

--==============================================================================
--
procedure p_get_fzreprf_params is
begin               -- just to make local names shorter
   eprf_disc_code      := fzkeprc.g_eprf_rec.fzreprf_disc_code;
   eprf_prod_dbname    := fzkeprc.g_eprf_rec.fzreprf_prod_dbname;
   eprf_inv_userid     := fzkeprc.g_eprf_rec.fzreprf_inv_userid;
   eprf_inv_docno      := fzkeprc.g_eprf_rec.fzreprf_inv_docno;
   eprf_inv_appr       := fzkeprc.g_eprf_rec.fzreprf_inv_appr;
   eprf_inv_cmpl       := fzkeprc.g_eprf_rec.fzreprf_inv_cmpl;
   eprf_inv_send       := fzkeprc.g_eprf_rec.fzreprf_inv_email_send;
   eprf_inv_email      := fzkeprc.g_eprf_rec.fzreprf_inv_email_text;
   eprf_inv_trans_date := fzkeprc.g_eprf_rec.fzreprf_inv_trans_date;
   eprf_inv_dir        := fzkeprc.g_eprf_rec.fzreprf_inv_dir;
end;
--
--==============================================================================
--
function f_fspd_open(p_userid varchar2, p_date date) return boolean is
   s varchar2(1);
begin
   select ftvfspd_prd_status_ind into s
     from ftvfspd, fobprof
    where ftvfspd_coas_code = fobprof_coas_code
      and fobprof_user_id = upper(p_userid)
      and p_date between trunc(ftvfspd_prd_start_date) and trunc(ftvfspd_prd_end_date);
   if s = 'O' then return true;
   else return false;
   end if;
exception when others then
   return false;
end;
----==============================================================================
-- TC MOD 20130905 START ------------------------------------------------------  
function p_get_fy_end_date_for_po(po_code fpbpohd.fpbpohd_code%TYPE) 
RETURN DATE
IS   
	   v_fy_end_date DATE := NULL;
BEGIN
--   SELECT MAX(ftvfsyr_end_date)
--     INTO v_fy_end_date 
--     FROM fgbencp,
--          ftvfsyr
--    WHERE fgbencp_num = po_code
--      AND fgbencp_fsyr_code = ftvfsyr_fsyr_code;
      
-- TC MOD 20170927 START ------------------------------------------------------  

SELECT max(ftvfsyr_end_date)
     INTO v_fy_end_date 
     FROM fprpoda,fgbencp,
          ftvfsyr
    WHERE fprpoda_pohd_code =  po_code
      and fprpoda_pohd_code = fgbencp_num (+)
      and (fprpoda_fsyr_code = ftvfsyr_fsyr_code
              or fgbencp_fsyr_code = ftvfsyr_fsyr_code);
-- TC MOD 20170927 END ------------------------------------------------------ 

   If v_fy_end_date is null then v_fy_end_date := sysdate; end if;
  
   RETURN v_fy_end_date;   
EXCEPTION when others then
   RETURN TRUNC(SYSDATE);
end p_get_fy_end_date_for_po;
-- TC MOD 20130905 END   ------------------------------------------------------
-- 
-- TC MOD 20130425 START ------------------------------------------------------  
procedure p_send_nsf_invoice_email(p_invoice_code varchar2, p_msg varchar2) is
   l_from varchar2(100);
   l_subj varchar2(500);
   l_to varchar2(32000);
   l_cc varchar2(32000);
   v_mail_successful VARCHAR2(300) := 'T' ; 
   v_mail_errmsg     VARCHAR2(300) := 'F' ;     
-- TC MOD 20161110 Start   ------------------------------------------------------        
   v_error_type varchar2(100);
   v_error_code varchar2(100);
   v_error_message varchar2(3000);
-- TC MOD 20161110 End   ------------------------------------------------------        

begin
--
   l_subj := 'Unimarket: Invoice '||p_invoice_code||' is NSF.';
--
   for e in(select upper(ftvsdat_sdat_code_opt_1) opt, ftvsdat_title email from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and upper(ftvsdat_sdat_code_attr) = 'UM INVNSF EMAIL'
               and upper(ftvsdat_sdat_code_opt_1) in('FROM','TO','CC')
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) loop
      if  e.opt='FROM' then l_from := e.email;
      elsif e.opt='TO' then l_to := l_to||trim(e.email)||', ';
      elsif e.opt='CC' then l_cc := l_cc||trim(e.email)||', '; end if;
   end loop;
   l_to := rtrim(l_to,', '); l_cc := rtrim(l_cc,', ');
     if l_from is not null and l_to is not null then
-- TC MOD 20161110 Start   ------------------------------------------------------        
   sokemal.p_sendemail (email_addr      => l_to,                   
                        email_to_name   => NULL,
                        email_from_addr => l_from,
                        email_from_name => 'Unimarket - NO REPY',
                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                        email_subject   => l_subj,
                        email_message   => p_msg,
                        email_success   => v_mail_successful,
                        email_reply     => v_mail_errmsg,
                        email_error_type => v_error_type,
                        email_error_code => v_error_code,
                        email_error_message => v_error_message);                                             
-- TC MOD 20161110 End  ------------------------------------------------------                                
   else
      dbms_output.put_line('ERROR in p_send_nsf_invoice_email: Can not send email!!!');
   end if;
--
exception when others then
   dbms_output.put_line('ERROR in p_send_nsf_invoice_email: '||SQLERRM);
end p_send_nsf_invoice_email;

procedure p_check_nsf_invoice(p_invoice_code IN varchar2) is
   l_msg varchar2(4000) := NULL;
   crlf VARCHAR2( 2 ):= CHR( 13 ) || CHR( 10 );
cursor nsf_cursor(c_invoice_code VARCHAR2) IS                          
   SELECT FARINVA_ACCI_CODE, 
          FARINVA_ACCT_CODE,
          lpad(TRIM(TO_CHAR(FARINVA_APPR_AMT,'9,999,999.00')),14) AS APPR_AMT 
   FROM 
       FARINVA
   WHERE FARINVA_INVH_CODE = p_invoice_code
     AND FARINVA_NSF_SUSP_IND = 'Y'
   ORDER BY 1; 
   nsf_info nsf_cursor%ROWTYPE;   
begin
   open nsf_cursor(p_invoice_code);
   loop
      fetch nsf_cursor into nsf_info;
      EXIT WHEN nsf_cursor%NOTFOUND;
      l_msg := l_msg||nsf_info.FARINVA_ACCI_CODE
      ||'-'
      ||nsf_info.FARINVA_ACCT_CODE
      ||nsf_info.APPR_AMT
      ||crlf; 
   end loop;
   close nsf_cursor;
   IF l_msg IS NOT NULL THEN
      l_msg := 'Invoice '||p_invoice_code||' has been created by Unimarket and is in NSF status.'
      ||crlf||crlf
      ||'Below is a list of the index/account combinations for this invoice:'||crlf
      ||crlf
      || l_msg
      || crlf
      || 'End of list.'||crlf;      
      p_send_nsf_invoice_email(p_invoice_code, l_msg);                
   END IF;
   RETURN;
exception when others then
   dbms_output.put_line('ERROR in p_send_error_email: '||SQLERRM);
end p_check_nsf_invoice;
-- TC MOD 20130425 END   ------------------------------------------------------ 
--
procedure p_get_inv_header(p_msg in out varchar2) is
   l_msg               varchar2(100);
   l_currency          gubinst.gubinst_base_curr_code%type;
   l_disc_code         fpbpohd.fpbpohd_disc_code%type;
   l_payment_days      number(4);
   l_vend_disc_code    ftvvend.ftvvend_disc_code%type;
   l_vend_payment_days ftvdisc.ftvdisc_net_days%type;
   l_vend_pct          ftvdisc.ftvdisc_pct%type;
   l_xml_disc_code     ftvdisc.ftvdisc_code%type;
   l_xml_payment_days  number(4);
   l_xml_pct           ftvdisc.ftvdisc_pct%type;
   
   -- TC MOD 20160601 START ----------------These can be cleaned up
   g_po_fy varchar2(2);
   g_po_fy_start_date date;
   g_po_fy_end_date date;
   -- TC MOD 20160601 END ----------------------------------------------------
begin
--
   fzkeprc.p_set_prc_name('p_get_inv_header');
   dbms_output.put_line('=== '||fzkeprc.g_prc_name);
   
--
-- Check if PO exists and get PO code, user id,
-- vendor discount code, discount days, pct and single_acctg_ind.
   g_order_ID := fzkeprc.f_get_val(g_orderID, true);

   begin
      select nvl(upper(eprf_inv_userid), fpbpohd_user_id),        --> g_inv_user
             fpbpohd_user_id,              --> g_user_id --> FABINVH_CREATE_USER
             substr(fobprof_requester_orgn_code,1,2),   --> g_req_orgn --> DOCNO
             fobprof_user_name,
             fobprof_coas_code,
             fpbpohd_code,
             nvl(fpbpohd_single_acctg_ind, 'N'),
             fpbpohd_trans_date,
             fpbpohd_vend_pidm,
             ftvvend_disc_code,
             ftvdisc_net_days,
             ftvdisc_pct
        into g_inv_user,
             g_user_id,
             g_req_orgn,
             g_user_name,
             g_user_coas,
             g_pohd_code,
             g_single_acctg_ind,
             g_pohd_trans_date,
             g_vend_pidm,
             l_vend_disc_code,
             l_vend_payment_days,
             l_vend_pct
        from fzrcxml, fpbpohd, ftvvend, ftvdisc, fobprof
       where fzrcxml_doc_type = 'PO'
         and fzrcxml_doc_id = g_order_ID
         and fpbpohd_code = fzrcxml_doc_code
         and fpbpohd_change_seq_num is null
         and fpbpohd_user_id   = fobprof_user_id(+)
         and fpbpohd_vend_pidm = ftvvend_pidm(+)
         and ftvvend_disc_code = ftvdisc_code(+)
         and nvl(ftvdisc_nchg_date(+),sysdate+1) >= trunc(sysdate)
         and rownum = 1; -- just in case
   exception when no_data_found then
      p_msg := 'Parent PO with orderID '||g_order_ID||' not found!';
      return;                                                       -- Return!!!
   end;
--
   fzkeprc.g_user := g_inv_user;
--
-- Check Deployment Mode
   l_msg := upper(fzkeprc.f_get_val(g_deploymentMode));
   if l_msg =  'PRODUCTION' and
        upper(fzkeprc.g_banner)<>upper(fzkeprc.g_eprf_rec.fzreprf_prod_dbname) or
      l_msg <> 'PRODUCTION' and
        upper(fzkeprc.g_banner) =upper(fzkeprc.g_eprf_rec.fzreprf_prod_dbname) then
      p_msg := 'Wrong Deployment Mode "'||l_msg||'" for Banner instance '||fzkeprc.g_banner;
      return;                                                       -- Return!!!
   end if;
--
-- Check invoice purpose
-- If XML has the line then the purpose must equal "standard"
   l_msg := upper(nvl(fzkeprc.f_get_val(g_invoicePurpose),'STANDARD'));
   if l_msg <>  'STANDARD' then
      p_msg := 'Invalid purpose in XML "'||l_msg||'" ';
      return;                                                       -- Return!!!
   end if;
--
-- Check currency code
   l_currency := fzkeprc.f_get_val(g_currency, true);
-- TC MOD 20121212 START ------------------------------------------------------ 
   IF l_currency = 'USD' 
   THEN
      l_currency := 'USA';
   END IF;
-- TC MOD 20121212 END   ------------------------------------------------------ 
   if l_currency = fb_common.f_get_base_currency then
      l_currency := null;
   elsif gb_gtvcurr.f_code_exists(l_currency,sysdate )<> 'Y' then
      p_msg := 'Invalid value for currency code '||l_currency;
      return;                                                       -- Return!!!
   end if;
--
-- Check if Invoice already exists
   g_vend_inv_code := substr(fzkeprc.f_get_val(g_invoiceID, true), 1, 15);
   begin
      select 'Invoice '||fabinvh_code||'/'||g_pohd_code||'/'||g_vend_inv_code||' already exists'
          -- This Vendor Invoice already exists for this vendor
        into l_msg from fabinvh
       where fabinvh_pohd_code = g_pohd_code
         and fabinvh_vend_inv_code = g_vend_inv_code
         and fabinvh_cancel_date is null;
      p_msg := l_msg;
      return;                                                       -- Return!!!
   exception when no_data_found then null;
   end;
   
-- TC MOD 20170128 START  Replicate API check for duplicate vendor invoice ---------------------
   begin
      select 'Vendor Invoice '||fabinvh_code||'/'||g_pohd_code||'/'||g_vend_inv_code||' already exists'
          -- This Vendor Invoice already exists for this vendor
        into l_msg from fabinvh
       where fabinvh_vend_pidm = g_vend_pidm
         and fabinvh_vend_inv_code = g_vend_inv_code
         and fabinvh_cancel_date is null;
      p_msg := l_msg;
      return;                                                       -- Return!!!
   exception when no_data_found then null;
   end;

-- TC MOD 20170128 END ----------------------------------------------------------------
--
-- Get DOCNO
   if fzkeprc.g_new_request = 'Y' then -- Get DOCNO for new invoice
      if eprf_inv_docno = 'N' then        -- Let Banner assign Invoice DOCNO
         g_inv_code := null;
         fokutil.p_gen_doc_code(3, g_inv_code);            -- 09/20/2012 sergeyv
      else                                -- Get custom DOCNO
         if eprf_inv_docno = 'Y' then        -- Use Buyer REQUESTER_ORGN_CODE
            if g_req_orgn is not null then
               g_inv_prefix := g_req_orgn;
            else
               p_msg := 'FOBPROF_REQUESTER_ORGN_CODE is not defined for user '||g_user_id;
               return;                                              -- Return!!!
            end if;
         else                                -- Use FZREPRF_INV_DOCNO
            g_inv_prefix := eprf_inv_docno;
         end if;
         loop                                -- Generate next DOCNO
            g_inv_code := fokutil.f_gen_fseq_number('EPROC_'||g_inv_prefix);
            exit when fokutil.f_no_dupdoc_exists(3, g_inv_code) = 'Y';
         end loop;
      end if;
   else                                -- Use old invoice DOCNO (reprocess)
      g_inv_code := fzkeprc.g_old_docno;
--    Validate old invoice DOCNO (just in case)
      if g_inv_code is null then
         if eprf_inv_docno != 'N' then                     -- 09/18/2012 sergeyv
            p_msg := 'DOCNO for the old invoice is not defined';
            return;                                                 -- Return!!!
         end if;
      elsif fokutil.f_no_dupdoc_exists(3, g_inv_code) <> 'Y' then
         p_msg := 'Cannot reuse old invoice DOCNO '||g_inv_code||': dup doc exists';
         return;                                                    -- Return!!!
      end if;
   end if;
--
dbms_output.put_line('  g_inv_code = '||g_inv_code);

--
-- Check if invoice has item with zero quantity
   select decode(count(*),0,'','Invoice has item with zero quantity')
     into l_msg
     from xmltable('//InvoiceDetailItem' passing fzkeprc.g_xml
          columns quantity number path '@quantity')
    where quantity=0;
   if l_msg is not null then p_msg := l_msg; return; end if;        -- Return!!!
--
--------------------------------------------------------------------------------
-- Determine the discount code and the number of days to add to the
-- XML invoice date for the payment date
--
-- Retrieve the maximum percentage rate with the maximum pay in number
-- of days values from XML (if exists)
   begin
      select days, pct
        into l_xml_payment_days, l_xml_pct
        from(select nvl(days,0) days, nvl(pct,0) pct,
                    row_number()over(order by nvl(pct,0) desc nulls last,
                                             nvl(days,0) desc nulls last) rn
               from xmltable('//PaymentTerm'
                    passing fzkeprc.g_xml
                    columns days number path '@payInNumberOfDays',
                             pct number path '//@percent'))
       where rn=1;
dbms_output.put_line('  XML days/pct '||l_xml_payment_days||'/'||l_xml_pct);
      begin
         select ftvdisc_code into l_xml_disc_code
           from ftvdisc
          where ftvdisc_net_days = l_xml_payment_days
            and ftvdisc_pct = l_xml_pct
            and nvl(ftvdisc_nchg_date,trunc(sysdate)) >= trunc(sysdate);
dbms_output.put_line('  Got XML FTVDISC_CODE '||l_xml_disc_code);
      exception when others then
dbms_output.put_line('  No FTVDISC_CODE found for XML days/pct');
         l_xml_disc_code := null;
/*
         when no_data_found then
            p_msg := 'No discount code in FTVDISC for XML values Percent '||
                      l_xml_pct||' and Net Days '||l_xml_payment_days;
            return;                                                 -- Return!!!
         when others then
            p_msg := 'More than one record in FTVDISC for XML values Percent '||
                      l_xml_pct||' and Net Days '||l_xml_payment_days;
            return;                                                 -- Return!!!
*/
      end;
   exception when no_data_found then -- No data found in XML
dbms_output.put_line('  No PaymentTerm in XML');

      l_xml_payment_days := null;
      l_xml_pct := null;
      l_xml_disc_code := null;
   end;
--
-- Compare the percents between ftvvend and the xml to get the best percent rate
   if l_vend_pct is null and l_xml_pct is null then
--    Check default discount code
      if eprf_disc_code is null then
         p_msg := 'FZREPRF_DISC_CODE is NULL';
         return;                                                    -- Return!!!
      end if;
      begin
-- TC MOD 20130411 START ------------------------------------------------------ 
/*         select ftvdisc_code, ftvdisc_net_days
           into l_disc_code, l_payment_days
           from ftvdisc
          where ftvdisc_code = eprf_disc_code
            and nvl(ftvdisc_nchg_date,trunc(sysdate)) >= trunc(sysdate);
*/            
         select ftvdisc_code, ftvdisc_net_days
           into l_disc_code, l_payment_days
           from ftvdisc
          where ftvdisc_code = eprf_disc_code
            and ftvdisc_nchg_date IS NULL;
-- TC MOD 20130411 END   ------------------------------------------------------                
      exception when no_data_found then
         p_msg := 'Wrong FZREPRF_DISC_CODE '||eprf_disc_code;
         return;                                                    -- Return!!!
      end;
   elsif l_vend_pct is not null and l_xml_pct is not null then
      if l_vend_pct > l_xml_pct then
         l_disc_code := l_vend_disc_code;
         l_payment_days := l_vend_payment_days;
      elsif l_vend_pct = l_xml_pct then
         if l_vend_payment_days > l_xml_payment_days then
            l_disc_code := l_vend_disc_code;
            l_payment_days := l_vend_payment_days;
         else
            l_disc_code := l_xml_disc_code;
            l_payment_days := l_xml_payment_days;
         end if;
      else -- l_vend_pct < l_xml_pct
         l_disc_code := l_xml_disc_code;
         l_payment_days := l_xml_payment_days;
      end if;
   elsif l_vend_pct is not null then
      l_disc_code := l_vend_disc_code;
      l_payment_days := l_vend_payment_days;
   else
      l_disc_code := l_xml_disc_code;
      l_payment_days := l_xml_payment_days;
   end if;
--

   g_invoice_date := to_date(fzkeprc.xml2date(fzkeprc.f_get_val(g_invoiceDate, true)));
   g_pmt_due_date := greatest((sysdate+1/(24*60)),(g_invoice_date+l_payment_days));
--
    if eprf_inv_trans_date = 'Y' then
      g_trans_date := greatest(g_invoice_date, g_pohd_trans_date);
      if not f_fspd_open(g_inv_user, g_trans_date) then
         g_trans_date := sysdate;
      end if;
   else
-- TC MOD 20130905 START ------------------------------------------------------       
--      g_trans_date := sysdate;
-- TC MOD 20160601 START ------------------------------------------------------
    --TC MOD 20170330 START
    if nvl(substr(g_invoice_doc_ref,1,2),'*') = 'FY' then
       if length(g_invoice_doc_ref) = 6 then
         g_invoice_doc_ref_fy := substr(g_invoice_doc_ref,5,2);
         fzkeprc.p_get_trans_eff_dt(g_invoice_doc_ref_fy,g_trans_date);
         tcapp.util_msg.logEntry(g_invoice_doc_ref);
       else 
         g_invoice_doc_ref_fy := substr(g_invoice_doc_ref,3,2);
         fzkeprc.p_get_trans_eff_dt(g_invoice_doc_ref_fy,g_trans_date);
         tcapp.util_msg.logEntry(g_invoice_doc_ref||' from the else');
       end if;
    else  
      g_trans_date := least(p_get_fy_end_date_for_po(g_pohd_code), sysdate);
     -- tcapp.util_msg.logEntry(g_invoice_doc_ref||' bypassed');
    end if;
    --TC MOD 20170330 End
    
-- TC MOD 20180314 START ------------------------------------------------------
    g_bank_code := case when upper(g_invoice_doc_ref) = 'WIRE' then '12'
                        else 
                        fzkeprc.g_eprf_rec.fzreprf_bank_code
                        end;
-- TC MOD 20180314 END ------------------------------------------------------

    
  /*  Undo dual fy change
    begin
     select ftvfsyr_fsyr_code  --, ftvfsyr_start_date, ftvfsyr_end_date
     into g_po_fy              --, g_po_fy_start_date, g_po_fy_end_date
     from ftvfsyr
     where trunc(g_pohd_trans_date) between trunc(ftvfsyr_start_date) and trunc(ftvfsyr_end_date)
      and ftvfsyr_coas_code = '1';
   exception 
     when others then
      p_msg := 'Unable to determine FY for '||g_pohd_trans_date; return;
   end;

   fzkeprc.p_get_trans_eff_dt(g_po_fy, g_trans_date);
   */
   g_pmt_due_date := greatest((g_trans_date),(g_invoice_date+l_payment_days));
  
-- TC MOD 20160601 END  ------------------------------------------------------       
-- TC MOD 20130905 END   ------------------------------------------------------        
   end if;
--
--------------------------------------------------------------------------------
-- If summary subtotal is negative then this is a credit memo.
   if fzkeprc.f_get_val(g_SubtotalAmount,true) < 0 then
      g_cr_memo_ind := 'Y'; -- Credit Memo
   else
      g_cr_memo_ind := 'N'; -- Debit transaction
   end if;
--
   g_inv_sum_shipping := fzkeprc.f_get_val(g_ShippingAmount);
   if g_cr_memo_ind = 'Y'    then g_inv_sum_shipping := abs(g_inv_sum_shipping); end if;
   if g_inv_sum_shipping = 0 then g_inv_sum_shipping := null; end if;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
   g_header_rec.r_code                  := g_inv_code;
   g_header_rec.r_pohd_code             := g_pohd_code;
   g_header_rec.r_vend_pidm             := null;
   g_header_rec.r_open_paid_ind         := null;
   g_header_rec.r_user_id               := g_inv_user;
   g_header_rec.r_vend_inv_code         := g_vend_inv_code;
   g_header_rec.r_invoice_date          := g_invoice_date;
   g_header_rec.r_pmt_due_date          := g_pmt_due_date;
   g_header_rec.r_trans_date            := g_trans_date;
   g_header_rec.r_cr_memo_ind           := g_cr_memo_ind;
   g_header_rec.r_adjt_code             := null;
   g_header_rec.r_adjt_date             := null;
   g_header_rec.r_1099_ind              := null; -- set by api
   g_header_rec.r_1099_id               := null;
   g_header_rec.r_ityp_seq_code         := null;
   g_header_rec.r_text_ind              := null;
   g_header_rec.r_appr_ind              := null; -- not in use
   g_header_rec.r_complete_ind          := null;
   g_header_rec.r_disc_code             := l_disc_code;
   g_header_rec.r_trat_code             := null;
   g_header_rec.r_addl_chrg_amt         := g_inv_sum_shipping;
   g_header_rec.r_hold_ind              := null;
   g_header_rec.r_susp_ind              := null;
   g_header_rec.r_susp_ind_addl         := null;
   g_header_rec.r_cancel_ind            := null;
   g_header_rec.r_cancel_date           := null;
   g_header_rec.r_post_date             := null;
   g_header_rec.r_atyp_code             := null;
   g_header_rec.r_atyp_seq_num          := null;
   g_header_rec.r_grouping_ind          := null;
   g_header_rec.r_bank_code             := g_bank_code;   -- TC MOD 20180314 fzkeprc.g_eprf_rec.fzreprf_bank_code;
   g_header_rec.r_ruiv_ind              := null;
   g_header_rec.r_edit_defer_ind        := null;
   g_header_rec.r_override_tax_amt      := null;
   g_header_rec.r_tgrp_code             := null;
   g_header_rec.r_submission_number     := null;
   g_header_rec.r_vend_check_pidm       := null;
   g_header_rec.r_invoice_type_ind      := null;
   g_header_rec.r_curr_code             := l_currency;
   g_header_rec.r_disb_agent_ind        := null;
   g_header_rec.r_atyp_code_vend        := null;
   g_header_rec.r_atyp_seq_num_vend     := null;
   g_header_rec.r_nsf_on_off_ind        := null;
   g_header_rec.r_single_acctg_ind      := null;
   g_header_rec.r_one_time_vend_name    := null;
   g_header_rec.r_one_time_house_number := null;
   g_header_rec.r_one_time_vend_addr1   := null;
   g_header_rec.r_one_time_vend_addr2   := null;
   g_header_rec.r_one_time_vend_addr3   := null;
   g_header_rec.r_one_time_vend_addr4   := null;
   g_header_rec.r_one_time_vend_city    := null;
   g_header_rec.r_one_time_vend_state   := null;
   g_header_rec.r_one_time_vend_zip     := null;
   g_header_rec.r_one_time_vend_natn    := null;
   g_header_rec.r_delivery_point        := null;
   g_header_rec.r_correction_digit      := null;
   g_header_rec.r_carrier_route         := null;
   g_header_rec.r_ruiv_installment_ind  := null;
   g_header_rec.r_multiple_inv_ind      := null;
   g_header_rec.r_phone_area            := null;
   g_header_rec.r_phone_number          := null;
   g_header_rec.r_phone_ext             := null;
   g_header_rec.r_email_addr            := null;
   g_header_rec.r_ctry_code_fax         := null;
   g_header_rec.r_fax_area              := null;
   g_header_rec.r_fax_number            := null;
   g_header_rec.r_fax_ext               := null;
   g_header_rec.r_cancel_code           := null;
   g_header_rec.r_attention_to          := null;
   g_header_rec.r_vendor_contact        := null;
   g_header_rec.r_invd_re_establish_ind := null;
   g_header_rec.r_ach_override_ind      := null;
   g_header_rec.r_acht_code             := null;
   g_header_rec.r_origin_code           := substr(fzkeprc.g_orig,1,10);
   g_header_rec.r_match_required        := null;
   g_header_rec.r_create_user           := null;
   g_header_rec.r_create_date           := null;
   g_header_rec.r_complete_user         := null;
   g_header_rec.r_complete_date         := null;
   g_header_rec.r_data_origin           := null;
   g_header_rec.r_create_source         := null;
   g_header_rec.r_cancel_user           := null;
   g_header_rec.r_cancel_activity_date  := null;
   g_header_rec.r_ctry_code_phone       := null;
   g_header_rec.r_vend_hold_ovrd_ind    := null;
   g_header_rec.r_vend_hold_ovrd_user   := null;
   g_header_rec.r_internal_record_id    := null;
--
dbms_output.put_line('g_single_acctg_ind = '||g_single_acctg_ind);
dbms_output.put_line('    l_payment_days = '||l_payment_days);
dbms_output.put_line('    r_pohd_code    = '||g_header_rec.r_pohd_code);
dbms_output.put_line('    r_user_id      = '||g_header_rec.r_user_id);
dbms_output.put_line('    r_vend_inv_code= '||g_header_rec.r_vend_inv_code);
dbms_output.put_line('    r_invoice_date = '||g_header_rec.r_invoice_date);
dbms_output.put_line('    r_pmt_due_date = '||g_header_rec.r_pmt_due_date);
dbms_output.put_line('    r_cr_memo_ind  = '||g_header_rec.r_cr_memo_ind);
dbms_output.put_line('    l_disc_code    = '||g_header_rec.r_disc_code);
dbms_output.put_line('    r_addl_chrg_amt= '||g_header_rec.r_addl_chrg_amt);
dbms_output.put_line('    r_curr_code    = '||g_header_rec.r_curr_code);
dbms_output.put_line('    r_origin_code  = '||g_header_rec.r_origin_code);

--
exception when others then
-- TC MOD 20130910 START ------------------------------------------------------  
--   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
   p_msg := fzkeprc.p_got_error_tagged(SQLCODE, SQLERRM,g_pohd_code, g_vend_inv_code);
-- TC MOD 20130910 END   ------------------------------------------------------   
end p_get_inv_header;
--
--==============================================================================
--
procedure p_create_items(p_msg in out varchar2) as
   l_item_rec  fb_invoice_item.invoice_item_rec;
   l_item_path varchar2(100):=fzkeprc.f_get_path(fzkeprc.InvoiceDetailRequest, g_InvoiceDetailItem);
   l_msg       varchar2(4000);
   use_item_shipping boolean := false;
   l_item      farinvc.farinvc_item%type;
   l_po_item   farinvc.farinvc_po_item%type;
--
   l_tot_item_appr_amt farinvc.farinvc_appr_qty%type;
   l_tot_item_addl_amt number;
   l_tot_item_disc_amt number;
   l_tot_item_tax_amt number;
   l_tot_actg_amt number;
   l_tot_actg_appr_amt number;
   l_tot_actg_addl_amt number;
   l_tot_actg_disc_amt number;
   l_tot_actg_tax_amt number;
--------------------------------------------------------------------------------
procedure get_item_shipping is
   l_num number;
begin
   select count(*) into l_num from xmltable
         ('//InvoiceDetailItem/InvoiceDetailLineShipping/Money'
--           fzkeprc.f_get_path(fzkeprc.InvoiceDetailRequest, g_InvoiceDetailShipping)
             passing fzkeprc.g_xml columns amt number path '/')
    where amt<>0 and rownum=1;
   use_item_shipping:=(l_num>0);
end get_item_shipping;
--------------------------------------------------------------------------------
procedure p_adjust_actg_amts(p_item number default null) is
begin
-- Redistribute amount changes in the invoice items to the accounting sequences.
dbms_output.put_line('    Call fp_invoice.p_distribute');
   fp_invoice.p_distribute(g_inv_code, g_inv_user, p_item);
--
-- Get item total amounts
   select sum(nvl(farinvc_appr_qty,0)*nvl(farinvc_appr_unit_price,0)),
          sum(nvl(farinvc_addl_chrg_amt,0)),
          sum(nvl(farinvc_disc_amt,0)),
          sum(nvl(farinvc_tax_amt,0))
     into l_tot_item_appr_amt,
          l_tot_item_addl_amt,
          l_tot_item_disc_amt,
          l_tot_item_tax_amt
     from farinvc
    where farinvc_invh_code = g_inv_code
      and nvl(p_item, farinvc_item) = farinvc_item;
--
--dbms_output.put_line('    l_tot_item_appr_amt = '||l_tot_item_appr_amt);
--dbms_output.put_line('    l_tot_item_addl_amt = '||l_tot_item_addl_amt);
--dbms_output.put_line('    l_tot_item_disc_amt = '||l_tot_item_disc_amt);
--dbms_output.put_line('    l_tot_item_tax_amt  = '||l_tot_item_tax_amt);
--
-- Get accounting total amounts
   select sum(nvl(farinva_appr_amt,0)),
          sum(nvl(farinva_addl_chrg_amt,0)),
          sum(nvl(farinva_disc_amt,0)),
          sum(nvl(farinva_tax_amt,0))
     into l_tot_actg_appr_amt,
          l_tot_actg_addl_amt,
          l_tot_actg_disc_amt,
          l_tot_actg_tax_amt
     from farinva
    where farinva_invh_code = g_inv_code
      and nvl(p_item, farinva_item) = farinva_item;
--
--dbms_output.put_line('    l_tot_actg_appr_amt = '||l_tot_actg_appr_amt);
--dbms_output.put_line('    l_tot_actg_addl_amt = '||l_tot_actg_addl_amt);
--dbms_output.put_line('    l_tot_actg_disc_amt = '||l_tot_actg_disc_amt);
--dbms_output.put_line('    l_tot_actg_tax_amt  = '||l_tot_actg_tax_amt);
--
-- Check if amounts not equal
   if l_tot_item_appr_amt <> l_tot_actg_appr_amt or
      l_tot_item_addl_amt <> l_tot_actg_addl_amt or
      l_tot_item_disc_amt <> l_tot_actg_disc_amt or
      l_tot_item_tax_amt  <> l_tot_actg_tax_amt then
--    Get amt adjustments for seq 1
      select nvl(farinva_appr_amt,0)     +(l_tot_item_appr_amt-l_tot_actg_appr_amt),
             nvl(farinva_addl_chrg_amt,0)+(l_tot_item_addl_amt-l_tot_actg_addl_amt),
             nvl(farinva_disc_amt,0)     +(l_tot_item_disc_amt-l_tot_actg_disc_amt),
             nvl(farinva_tax_amt,0)      +(l_tot_item_tax_amt -l_tot_actg_tax_amt)
        into l_tot_actg_appr_amt,
             l_tot_actg_addl_amt,
             l_tot_actg_disc_amt,
             l_tot_actg_tax_amt
        from farinva
       where farinva_invh_code = g_inv_code
         and nvl(p_item, farinva_item) = farinva_item
         and farinva_seq_num = 1;
--
dbms_output.put_line(' Adjust accounting amounts');
dbms_output.put_line('    new actg_appr_amt = '||l_tot_actg_appr_amt);
dbms_output.put_line('    new actg_addl_amt = '||l_tot_actg_addl_amt);
dbms_output.put_line('    new actg_disc_amt = '||l_tot_actg_disc_amt);
dbms_output.put_line('    new actg_tax_amt  = '||l_tot_actg_tax_amt);
--
--    Updtae appr_amt in one farinva record
dbms_output.put_line('   Call fb_invoice_acctg.p_update');
      fb_invoice_acctg.p_update
                   (p_invh_code => g_inv_code,
                         p_item => nvl(p_item, 0),
                      p_seq_num => 1,
                      p_user_id => g_inv_user,
                     p_appr_amt => l_tot_actg_appr_amt,
                p_addl_chrg_amt => l_tot_actg_addl_amt,
                     p_disc_amt => l_tot_actg_disc_amt,
                      p_tax_amt => l_tot_actg_tax_amt);
   end if;
--
end p_adjust_actg_amts;
--------------------------------------------------------------------------------
procedure p_do_item_data(p_msg in out varchar2) is
   l_item_qty  farinvc.farinvc_appr_qty%type;
   l_item_ship farinvc.farinvc_addl_chrg_amt%type;
   l_item_disc farinvc.farinvc_disc_amt%type;
   l_item_tax  farinvc.farinvc_tax_amt%type;
   ---
-- TC MOD START 20170414------------------------------------
   l_unitprice farinvc.farinvc_appr_unit_price%type;
   l_adjust_amt  farinvc.farinvc_addl_chrg_amt%type;
-- TC MOD END 20170414------------------------------------
   
begin
   fzkeprc.p_set_prc_name('p_do_item_data');
   dbms_output.put_line('====== '||fzkeprc.g_prc_name);
--
   
-- Get item detail shipping
   if use_item_shipping then
      l_item_ship := fzkeprc.f_get_val(g_InvoiceDetailShipping);
      if l_item_ship = 0 then l_item_ship := null; end if;
      if g_cr_memo_ind = 'Y' then
         l_item_ship := abs(l_item_ship);
      end if;
   elsif l_item = 1 then
      l_item_ship := g_inv_sum_shipping;
   else
      l_item_ship := null;
   end if;
--
-- Get item quantity and if invoice is a credit memo then use absolute values.
   l_item_qty := fzkeprc.f_get_val(g_Item_quantity,true);
   if g_cr_memo_ind = 'Y' then
      l_item_qty  := abs(l_item_qty);
   elsif l_item_qty < 0 then
      p_msg := 'Debit transaction with negative quantity';
      return;                                                       -- Return!!!
   end if;
--
dbms_output.put_line('    Item '||l_item||'; po_item '||l_po_item||'; qty '||l_item_qty||'; shipping = '||l_item_ship);
--
dbms_output.put_line('    Call fb_invoice_item.p_update');

-- TC MOD START 20170414------------------------------------  
-- DGS  Adjust the charge amount on free items.   
   l_unitprice := fzkeprc.f_get_val(g_item_unitprice,true);
  
   if l_unitprice = '0.00' or l_unitprice = '0.0001' then
       l_adjust_amt := -.0001* to_number(l_item_qty);
       l_item_ship  := to_char((to_number(nvl(l_item_ship,'0')) + l_adjust_amt),'999999990.00');
       l_unitprice := '0.0001';
   end if;
-- TC MOD END 20170414------------------------------------
   
-- Call Invoice API to update Invoice Item with shipping charges
   fb_invoice_item.p_update
               (p_invh_code => g_inv_code,
                     p_item => l_item,
                  p_user_id => g_inv_user,
                 p_invd_qty => l_item_qty,
                 p_appr_qty => l_item_qty,
          p_invd_unit_price => fzkeprc.f_get_val(g_item_unitprice,true),
          p_appr_unit_price => l_unitprice,  -- TC MOD 20170414  
            p_addl_chrg_amt => l_item_ship,
                 p_disc_amt => fzkeprc.f_get_val(g_invoicedetaildiscount),
                  p_tax_amt => fzkeprc.f_get_val(g_InvoiceDetailTax));
--

   if g_single_acctg_ind <> 'Y' then               -- commodity level accounting
      p_adjust_actg_amts(l_item);
   end if;
--
exception when others then
-- TC MOD 20130910 START ------------------------------------------------------  
--   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
   p_msg := fzkeprc.p_got_error_tagged(SQLCODE, SQLERRM,g_pohd_code, g_vend_inv_code);
-- TC MOD 20130910 END   ------------------------------------------------------      
end p_do_item_data;
--------------------------------------------------------------------------------
--
begin -- p_create_items
--
   fzkeprc.p_set_prc_name('p_create_items');
   dbms_output.put_line('=== '||fzkeprc.g_prc_name);
--
   get_item_shipping;
--
   for i in(select value(item) i_xml from table(XMLSequence
                  (extract(fzkeprc.g_xml, l_item_path))) item) loop
      fzkeprc.g_xml := i.i_xml;
      l_item    := fzkeprc.f_get_val(g_Item_lineNumber,    true);
      l_po_item := fzkeprc.f_get_val(g_Item_lineNumber_ref,true);
--
--    Copy items one by one with their accounting lines from PO
dbms_output.put_line('  Call fp_invoice.p_copy_po_item_acctg');
      fp_invoice.p_copy_po_item_acctg
                         (p_invh_code => g_inv_code,
                            p_user_id => g_inv_user,
                        p_data_origin => fzkeprc.g_orig,
                               p_item => l_po_item);
--          
      p_do_item_data(l_msg);
      if l_msg is not null then p_msg := l_msg; return; end if;     -- Return!!!
--
   end loop;
--
   if g_single_acctg_ind = 'Y' then                 -- document level accounting
dbms_output.put_line('  Adjust accounting amounts');
      p_adjust_actg_amts;
   end if;
--
exception when others then
-- TC MOD 20130910 START ------------------------------------------------------  
--   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
   p_msg := fzkeprc.p_got_error_tagged(SQLCODE, SQLERRM,g_pohd_code, g_vend_inv_code);
-- TC MOD 20130910 END   ------------------------------------------------------      
end p_create_items;
--
--==============================================================================
--
procedure p_InvoiceDetailRequest(p_rsp in out CLOB, p_msg in out varchar2) is
   l_rec fzrcxml%rowtype;
   l_msg varchar2(4000);
   l_str varchar2(4000);
   l_rsp xmltype := xmltype('<Response><Status code="204" text="No Content"/></Response>');
--------------------------------------------------------------------------------
procedure p_create_cxml_log is
begin
   l_rec.fzrcxml_eprc_id    := fzkeprc.g_rid;
   l_rec.fzrcxml_doc_user   := fzkeprc.g_user;
   l_rec.fzrcxml_doc_type   :='INVOICE';
-- l_rec.fzrcxml_doc_code   := null;                     -- > 09/20/2012 sergeyv
   l_rec.fzrcxml_doc_code   := g_inv_code;               --/
   l_rec.fzrcxml_doc_id     := g_header_rec.r_vend_inv_code;
   l_rec.fzrcxml_doc_status := 'I';
   l_rec.fzrcxml_doc        := fzkeprc.g_req;
   l_rec.fzrcxml_sccss_msg  := null;
   l_rec.fzrcxml_error_msg  := null;
   l_rec.fzrcxml_user_id    := user;
   l_rec.fzrcxml_activity_date := sysdate;
   
   insert into fzrcxml values l_rec;
   commit;
end;
--------------------------------------------------------------------------------
procedure p_update_cxml_log is
begin
   update fzrcxml
      set fzrcxml_doc_id     = g_vend_inv_code,
          fzrcxml_doc_code   = g_inv_code,
          fzrcxml_doc_status = l_rec.fzrcxml_doc_status,
          fzrcxml_sccss_msg  = l_rec.fzrcxml_sccss_msg,
          fzrcxml_error_msg  = l_rec.fzrcxml_error_msg,
          fzrcxml_user_id    = user,
          fzrcxml_activity_date = sysdate
    where fzrcxml_eprc_id = fzkeprc.g_rid;
   commit;
end;
--------------------------------------------------------------------------------
procedure p_create_header(p_msg in out varchar2) is
begin
--
   fzkeprc.p_set_prc_name('p_create_header');
   dbms_output.put_line('=== '||fzkeprc.g_prc_name);
--
   fb_invoice_header.p_create_header(g_header_rec);
--
-- Assign returned values to global variables
   g_pohd_code            := g_header_rec.r_pohd_code;
   g_inv_code             := g_header_rec.r_code;
   g_vend_inv_code        := g_header_rec.r_vend_inv_code;
   l_rec.fzrcxml_doc_id   := g_vend_inv_code;
   l_rec.fzrcxml_doc_code := g_inv_code;
--
dbms_output.put_line('  Invoice Header created');
dbms_output.put_line('    invoiceID           '||g_vend_inv_code);
dbms_output.put_line('    FABINVH_CODE        '||g_inv_code);
dbms_output.put_line('    FABINVH_USER_ID     '||g_header_rec.r_user_id);
dbms_output.put_line('    FPBPOHD_CODE        '||g_pohd_code);
dbms_output.put_line('    FABINVH_CREATE_USER '||g_header_rec.r_create_user);
dbms_output.put_line('    g_inv_sum_shipping  '||g_inv_sum_shipping);


--
exception when others then
-- TC MOD 20130910 START ------------------------------------------------------  
--   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
   p_msg := fzkeprc.p_got_error_tagged(SQLCODE, SQLERRM,g_pohd_code, g_vend_inv_code);
-- TC MOD 20130910 END   ------------------------------------------------------      
end;
--------------------------------------------------------------------------------
procedure p_complete(p_msg in out varchar2) is
   l_succ_msg gb_common_strings.err_type;
   l_err_msg  gb_common_strings.err_type;
   l_warn_msg gb_common_strings.err_type;
-----------------------------------------
procedure p_set_post(p_set varchar2) is
begin gb_common.p_set_context('FP_INVOICE.P_COMPLETE', 'BATCH_TRAN_POST', p_set); end;
-----------------------------------------
function f_cln_msg(p_msg varchar2) return varchar2 is
begin return replace(rtrim(ltrim(p_msg,':'),':'),'::::',chr(10)); end;
-----------------------------------------
procedure p_complete_invoice is
begin
dbms_output.put_line('Call fp_invoice.p_complete');
   fp_invoice.p_complete
             (p_code => g_inv_code,
           p_user_id => g_inv_user,
    p_completion_ind => eprf_inv_cmpl,
   p_success_msg_out => l_succ_msg,
     p_error_msg_out => l_err_msg,
      p_warn_msg_out => l_warn_msg);
end;
-----------------------------------------
begin -- p_complete
--
   fzkeprc.p_set_prc_name('p_complete');
   dbms_output.put_line('=== '||fzkeprc.g_prc_name);
--
   if eprf_inv_cmpl = 'Y' and eprf_inv_appr = 'Y' then
      p_set_post('TRUE');
   end if;
--
   p_complete_invoice;
--
   if l_err_msg is not null and eprf_inv_appr = 'Y' then
--    Got error trying to post. Try now just to complete.
      p_set_post('FALSE');
      p_complete_invoice;
   end if;
--
-- TC MOD 20140917 START ------------------------------------------------------	   
   g_nsf_appr_byp  := 'N';
-- TC MOD 20140917 END   ------------------------------------------------------	   
   if upper(l_err_msg) like '%CANNOT COMPLETE A DOCUMENT HAVING TRANSACTIONS IN NSF STATUS WHEN APPROVALS ARE BYPASSED%' then
      l_succ_msg := 'Cannot complete a document having transactions in NSF status when approvals are bypassed.';
      l_err_msg := null;
-- TC MOD 20140917 START ------------------------------------------------------	   
      g_nsf_appr_byp  := 'Y';
-- TC MOD 20140917 END   ------------------------------------------------------	   
   end if;
--
   l_rec.fzrcxml_sccss_msg := substr(f_cln_msg(l_succ_msg||l_warn_msg),1,200);
   p_msg := f_cln_msg(l_err_msg);
--
dbms_output.put_line('  l_succ_msg '||l_succ_msg);
dbms_output.put_line('  l_err_msg  '||l_err_msg);
dbms_output.put_line('  l_warn_msg '||l_warn_msg);
--
exception when others then
-- TC MOD 20130910 START ------------------------------------------------------  
--   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
   p_msg := fzkeprc.p_got_error_tagged(SQLCODE, SQLERRM,g_pohd_code, g_vend_inv_code);
-- TC MOD 20130910 END   ------------------------------------------------------      
end;
--------------------------------------------------------------------------------
procedure p_send_email(p_status varchar2) is
   l_from varchar2(100);
   l_subj varchar2(500);
   l_to   varchar2(32000);
   l_cc   varchar2(32000);
   l_bc   varchar2(32000);
   l_msg  varchar2(4000);
-- TC MOD 20121212 START ------------------------------------------------------ 
   v_mail_successful VARCHAR2(300) := 'T' ; 
   v_mail_errmsg     VARCHAR2(300) := 'F' ;  
-- TC MOD 20121212 END   ------------------------------------------------------  
-- TC MOD 20161110 Start   ------------------------------------------------------        
   v_error_type varchar2(100);
   v_error_code varchar2(100);
   v_error_message varchar2(3000);
-- TC MOD 20161110 End   ------------------------------------------------------        
   
begin
--
-- Create message body
   select upper(fzkeprc.g_banner)||': e-Invoice - '||spriden_last_name||' - '||g_inv_code,
          replace(replace(replace(replace(replace(replace(replace(
          replace(replace(replace(eprf_inv_email,
                 '%InvNo%',    g_inv_code),
                 '%DueDate%',  to_char(g_pmt_due_date,'DD-MON-YYYY')),
                 '%VendName%', spriden_last_name),
                 '%VendCode%', spriden_id),
                 '%VendInvNo%',g_vend_inv_code),
                 '%PONo%',     g_pohd_code),
                 '%VendPOID%', g_order_ID),
                 '%BN%',       g_user_name),
                 '%ID%',       g_user_id),
                 '%Status%',   p_status)
     into l_subj, l_msg
     from spriden
    where spriden_pidm(+) = g_vend_pidm
      and spriden_change_ind(+) is null;
--
-- TC MOD 20130109 START ------------------------------------------------------
-- Get email addresses
/*   for e in(select upper(ftvsdat_sdat_code_attr) atr,
                   ftvsdat_title email
              from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and upper(ftvsdat_sdat_code_attr)
                      in('INV_EMAIL_TO', 'INV_EMAIL_FROM',
                         'INV_EMAIL_CC', 'INV_EMAIL_BCC')
               and(upper(ftvsdat_sdat_code_attr) <> 'INV_EMAIL_TO'
                or upper(ftvsdat_coas_code||'-'||ftvsdat_sdat_code_opt_1) =
                   upper(g_user_coas      ||'-'||g_req_orgn))
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_sdat_code_attr, ftvsdat_code_level) loop
      if    e.atr='INV_EMAIL_FROM' then l_from:=e.email;
      elsif e.atr='INV_EMAIL_TO'   then l_to := l_to||trim(e.email)||', ';
      elsif e.atr='INV_EMAIL_CC'   then l_cc := l_cc||trim(e.email)||', ';
      elsif e.atr='INV_EMAIL_BCC'  then l_bc := l_bc||trim(e.email)||', '; end if;
   end loop;
--
   l_to := rtrim(l_to,', '); l_cc := rtrim(l_cc,', '); l_bc := rtrim(l_bc,', ');
--
   if upper(fzkeprc.g_eprf_rec.fzreprf_prod_dbname)<>upper(fzkeprc.g_banner)
      or l_to is null then
      l_to := l_bc;
   end if;
*/   
-- TC MOD 20121212 START ------------------------------------------------------ 
/*
   if l_from is not null and l_to is not null then
      UTL_MAIL.SEND(mime_type => 'text/html; charset=us-ascii',
                       sender => l_from,
                   recipients => l_to,
                           cc => l_cc,
                          bcc => l_bc,
                      subject => l_subj,
                      message => l_msg);
   else
      dbms_output.put_line('ERROR in p_send_error_email: Can not send email!!!');
   end if;
*/
--
/*
   if l_from is not null and l_to is not null then
      sokemal.p_sendemail (email_addr      => l_to,                   
                           email_to_name   => NULL,
                           email_from_addr => l_from,
                           email_from_name => 'Unimarket - NO REPY',
                           email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                           email_subject   => l_subj,
                           email_message   => l_msg,
                           email_success   => v_mail_successful,
                           email_reply     => v_mail_errmsg);     
   else
      dbms_output.put_line('ERROR in p_send_error_email: Can not send email!!!');
   end if;    
-- TC MOD 20121212 END   ------------------------------------------------------ 
*/
   for e in(select ftvsdat_title email_from, ftvsdat_code_level code_level
              from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and upper(ftvsdat_sdat_code_attr) = 'INV_EMAIL_FROM'
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) 
     loop
        l_from:=e.email_from;
        for t in(select ftvsdat_title email_to
                   from ftvsdat
                  where ftvsdat_sdat_code_entity = 'FZKEPRC'
-- TC MOD 20140917 START ------------------------------------------------------    
--                    and upper(ftvsdat_sdat_code_attr) = 'INV_EMAIL_TO'
                    and (
                         upper(ftvsdat_sdat_code_attr) = 'INV_EMAIL_TO'
                         OR (g_nsf_appr_byp = 'Y' 
                             AND upper(ftvsdat_sdat_code_attr) = 'INV_EMAIL_TO_INC'
                            )
                        )
-- TC MOD 20140917 END   ------------------------------------------------------                            
                    and ftvsdat_status_ind = 'A'
                    and ftvsdat_eff_date <= sysdate
                    and nvl(ftvsdat_term_date, sysdate+1) > sysdate
                    and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
                    and upper(ftvsdat_coas_code||'-'||NVL(ftvsdat_sdat_code_opt_1,'99')) =
                        upper(NVL(g_user_coas,'1')  ||'-'||NVL(g_req_orgn,'99'))
                    and ftvsdat_code_level = e.code_level
               order by ftvsdat_sdat_code_attr) 
           loop 
           if l_from is not null and t.email_to is not null then
-- TC MOD 20161110 Start   ------------------------------------------------------        
              sokemal.p_sendemail (email_addr      => t.email_to,                   
                                   email_to_name   => NULL,
                                   email_from_addr => l_from,
                                   email_from_name => 'Unimarket ' || fzkeprc.g_umkt_sysid || ' - NO REPY',
                                   email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                                   email_subject   => l_subj,
                                   email_message   => l_msg,
                                   email_success   => v_mail_successful,
                                   email_reply     => v_mail_errmsg,
                                   email_error_type => v_error_type,
                                   email_error_code => v_error_code,
                                  email_error_message => v_error_message);    
-- TC MOD 20161110 END   ------------------------------------------------------      
              end if;         
           end loop;
   end loop;
-- TC MOD 20130109 END  -------------------------------------------------------
--
exception when others then
-- TC MOD 20140917 START ------------------------------------------------------	
--   dbms_output.put_line('ERROR in p_send_email: '||SQLERRM);
   tcapp.util_msg.LogEntry('ERROR in p_send_email: '||SQLERRM);
-- TC MOD 20140917 END  -------------------------------------------------------
end p_send_email;
--------------------------------------------------------------------------------
procedure p_done is
begin
--
   if l_msg is null then
      l_rec.fzrcxml_doc_status := 'C';
      if l_rec.fzrcxml_sccss_msg is null then
         l_rec.fzrcxml_sccss_msg := 'Invoice '||g_inv_code||' created for PO '||g_pohd_code;
      end if;
-- TC MOD 20130425 START ------------------------------------------------------        
      p_check_nsf_invoice(g_inv_code);
-- TC MOD 20130425 END   ------------------------------------------------------              
   else
      l_rec.fzrcxml_doc_status := 'E';
      if l_msg like '%'||fb_invoice_header_strings.f_get_error('PO_NOT_POSTED')||'%' then
--       Schedule reprocess job
         if fzkeprc.f_schedule_job('INVDELAY') > 0 then
            l_rec.fzrcxml_doc_status := 'S';
-- TC MOD 20161115 START ------------------------------------------------------ 
            l_msg := 'NOTICE  '||chr(10)||l_msg||chr(10)||chr(10)||'Invoice scheduled to be reprocessed';
-- TC MOD 20161115 END ------------------------------------------------------ 
         end if;         
      end if;
      l_rec.fzrcxml_error_msg := l_msg;
      l_msg := null;
   end if;
--
   if g_inv_code is not null then
      l_msg := 'FABINVH_CODE: '||g_inv_code||chr(10);
   end if;
--
   l_msg := 'invoiceID:    '||g_vend_inv_code||chr(10)||l_msg||
            'FPBPOHD_CODE: '||g_pohd_code||chr(10)||chr(10)||
             l_rec.fzrcxml_sccss_msg||l_rec.fzrcxml_error_msg;
--
   if eprf_inv_send = 'Y' and
      eprf_inv_email is not null and
      l_rec.fzrcxml_doc_status <> 'S' then
---   Send Invoice notification
--      p_send_email(replace(l_rec.fzrcxml_sccss_msg||l_rec.fzrcxml_error_msg,chr(10),'<br>'));
      p_send_email(l_rec.fzrcxml_sccss_msg||l_rec.fzrcxml_error_msg);
   end if;
--
dbms_output.put_line('==================');
dbms_output.put_line(l_msg);
dbms_output.put_line('==================');
end;
--------------------------------------------------------------------------------
procedure p_download_pdf is
   l_like   varchar2(200):='%wallet="%"%_%directory="%"%_%index="%"%_%path="%"%';
   l_file   utl_file.file_type;
   l_wallet varchar2(200);
   l_dir    varchar2(200);
   l_path   varchar2(200);
   l_index  varchar2(200);
   l_url    varchar2(500);
   l_fname  varchar2(200);
   l_err    varchar2(200);
   l_num    number;
/*
Index file line structure:
   Banner Document Code| -- Banner Invoice Number
   Document type|        -- INVOICE
   Transaction Date|     -- Banner Invoice transaction date in DD-MON-YYYY format
   BDMS Activity Date|   -- current date YYYY-MM-DD 08:00:00
   Security token|       -- 00
   File Path             -- @file_path for PDF
Example:
UD000012|INVOICE|12-SEP-12|2012-09-12 08:00:00|00|@\\yosemite\bdms_import$\FAST\test\B-F-DOCS\B-F-DOCS_RF_DOCS\UMInv_UD000012_001.pdf
*/
------------------------------------------
function get_par_value(p_name varchar2) return varchar2 is
   ln number := length(p_name||'="');
   n1 number := instr(eprf_inv_dir, p_name||'="');
   n2 number;
begin
   if n1=0 then return null; end if;
   n2 := instr(eprf_inv_dir, '"', n1+ln);
   return substr(eprf_inv_dir, n1+ln, n2-n1-ln);
end;
------------------------------------------
procedure p_write_file(p_file varchar2, p_url varchar2) is
   request  utl_http.req;
   response utl_http.resp;
   value    raw(32767);
begin
   UTL_HTTP.set_wallet(l_wallet);
   begin
      request := UTL_HTTP.begin_request(p_url, 'GET', UTL_HTTP.HTTP_VERSION_1_1);
   exception when others then
      l_err := 'Cannot begin HTTPS request '||p_url||' with wallet '||l_wallet||chr(10)||sqlerrm;
      fzkeprc.p_send_error_email(l_err);
      return;                                     -- Wrong parameters. Return!!!
   end;
   UTL_HTTP.set_header(request, 'User-Agent', 'Mozilla/4.0 (compatible)');
   response := UTL_HTTP.get_response(request);
   l_file := utl_file.fopen(l_dir, p_file, 'wb', 32767);
   begin
      loop
         utl_http.read_raw(response, value, 30000);
         utl_file.put_raw(l_file, value, true);
      end loop;
   exception when utl_http.end_of_body then
      utl_http.end_response(response);
   end;
   if length(value)>0 then
      utl_file.put_raw(l_file, value, true);
   end if;
   utl_file.fclose(l_file);
end p_write_file;
------------------------------------------
begin
--
-- Check parameters
   if eprf_inv_dir is NULL then return; end if;            -- Nothing to do. Return!!!
--
   if eprf_inv_dir not like l_like then
      l_err := 'FZREPRF_INV_DIR should be NULL or LIKE '||l_like;
      fzkeprc.p_send_error_email(l_err);
      return;                                     -- Wrong parameters. Return!!!
   end if;
--
   l_wallet := get_par_value('wallet');
   if l_wallet is null then return; end if;          -- Nothing to do. Return!!!
   if l_wallet not like 'file:%' then
      l_err := 'FZREPRF_INV_DIR: wallet should be NULL (wallet="") or LIKE "file:%"';
      fzkeprc.p_send_error_email(l_err);
      return;                                   -- Wrong wallet value. Return!!!
   end if;
--
   l_dir := get_par_value('directory');
   if l_dir is null then return; end if;             -- Nothing to do. Return!!!
   select count(*) into l_num from all_directories where directory_name = l_dir;
   if l_num = 0 then
      l_err := 'FZREPRF_INV_DIR: Oracle directory '||l_dir||' not defined';
      fzkeprc.p_send_error_email(l_err);
      return;                                -- Wrong directory value. Return!!!
   end if;
--
   l_index := replace(get_par_value('index'),'<doc_code>',g_inv_code);
   l_path  := get_par_value('path');
   if fzkeprc.g_banner = fzkeprc.g_eprf_rec.fzreprf_prod_dbname then
      l_path := replace(l_path,'<test_or_prod>','prod');
   else
      l_path := replace(l_path,'<test_or_prod>','test');
   end if;
--
   l_fname := 'UMInv_'||g_inv_code||'_001.pdf';

   l_url := fzkeprc.xml_val('//InvoiceDetailRequestHeader/Extrinsic[@name="unimarket-url"]');
   l_url := replace(l_url,'/community/invoice/view/uuid/', '/pdf/invoice/?uuid=');
--
   p_write_file(l_fname, l_url);
--
   if l_index is not null then
      l_file := utl_file.fopen(l_dir, l_index, 'w', 30000);
      utl_file.put_line(l_file, g_inv_code||'|INVOICE|'||to_char(g_trans_date,'DD-MON-YYYY')||'|'||
                                to_char(sysdate,'YYYY-MM-DD')||' 08:00:00|00|@'||l_path||l_fname);
      utl_file.fclose(l_file);
   end if;
--
dbms_output.put_line('l_wallet: '||l_wallet);
dbms_output.put_line('l_dir   : '||l_dir);
dbms_output.put_line('l_index : '||l_index);
dbms_output.put_line('l_path  : '||l_path);
dbms_output.put_line('l_url   : '||l_url);
dbms_output.put_line('l_fname : '||l_fname);
--
exception when others then
   l_err := 'ERROR in p_download_pdf: '||SQLERRM;
   fzkeprc.p_send_error_email(l_err);
end p_download_pdf;
--------------------------------------------------------------------------------
--
begin -- p_InvoiceDetailRequest
--
   fzkeprc.p_set_prc_name('p_InvoiceDetailRequest');
dbms_output.put_line(fzkeprc.g_prc_name);
--
   p_get_fzreprf_params;
--
-- Create Response from Request
   select xmltype(replace(appendChildXML(deleteXML(deleteXML(fzkeprc.g_xml,
       '//Request'),'//Header'),'//cXML', l_rsp).getclobval(),'cxml-request','cxml-response'))
     into l_rsp from dual;
--
-- Update Response cXML/@payloadID
   select updateXML(l_rsp, '//cXML/@payloadID',
          'IN.'||to_char(sysdate,'yyyymmddhh24miss')||'.'||to_char(fzkeprc.g_rid))
     into l_rsp from dual;
--
   p_get_inv_header(l_msg);
   if l_msg is not null then
      p_msg := l_msg;
      p_rsp := l_rsp.getclobval();
      return;                                                       -- Return!!!
   end if;
--
   if fzkeprc.g_new_request = 'Y' then p_create_cxml_log; end if;
--
   p_create_header(l_msg);
--
   if l_msg is null then
      p_download_pdf;
      p_create_items(l_msg);
   end if;
--
   if l_msg is null then p_complete(l_msg); end if;
--
   p_done;
--
   p_update_cxml_log;
--
   if l_rec.fzrcxml_error_msg is not null then   -- Something wrong!!!
      fzkeprc.p_send_error_email(l_msg);         -- Send email to users
   end if;                                       -- specified in FTVSDAT
--
-- Update Response
   if l_rec.fzrcxml_doc_status = 'C' then
      select updateXML(l_rsp, '//Status/@code', '200',
                              '//Status/@text', l_rec.fzrcxml_sccss_msg)
        into l_rsp from dual;
   end if;
--
   p_rsp := l_rsp.getclobval();
--
exception when others then
-- TC MOD 20130910 START ------------------------------------------------------  
--   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
   p_msg := fzkeprc.p_got_error_tagged(SQLCODE, SQLERRM,g_pohd_code, g_vend_inv_code);
-- TC MOD 20130910 END   ------------------------------------------------------      
   p_rsp := l_rsp.getclobval();
end p_InvoiceDetailRequest;
--
--==============================================================================
--

END FZKEPRC_IN;
/
