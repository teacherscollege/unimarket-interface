--------------------------------------------------------
--  File created - Thursday-November-15-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View FZVPOFX
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "BANINST1"."FZVPOFX" ("FY", "PO_PREFIX", "FSYR_CODE", "FTVFSYR_START_DATE", "FTVFSYR_END_DATE") AS 
  select distinct case when substr(fpbpohd_code,1,3) = 'UI1' then
                '20'||substr(fpbpohd_code,3,2)
                when substr(fpbpohd_code,1,2) = 'UI' then
                '201'||substr(fpbpohd_code,3,1)
                else
                '20'||decode(substr(fpbpohd_code,3,2),'16','15',substr(fpbpohd_code,3,2))
                end AS FY,
                case when substr(fpbpohd_code,1,3) = 'UI1' then
                'UI'||substr(fpbpohd_code,4,1)
                when substr(fpbpohd_code,1,2) = 'UI' then
                'UI'||substr(fpbpohd_code,3,1)
                else 
                decode(substr(fpbpohd_code,3,2),'16','PO15','PO'||substr(fpbpohd_code,3,2))
                end  AS PO_PREFIX,
               case when substr(fpbpohd_code,1,3) = 'UI1' then
               '1'||substr(fpbpohd_code,4,1)
               when substr(fpbpohd_code, 1,2) = 'UI' then
               '1'||substr(fpbpohd_code,3,1)
               else
               decode(substr(fpbpohd_code,3,2),'16','15',substr(fpbpohd_code,3,2)) 
               end AS fsyr_code,
               FTVFSYR_START_DATE,
               FTVFSYR_END_DATE
from fpbpohd,
     ftvfsyr
where ((fpbpohd_code like 'PO%' AND FPBPOHD_CODE > 'PO130000') or (fpbpohd_code like 'U%'))
  AND FPBPOHD_ACTIVITY_DATE > TO_DATE('01/11/2013','MM/DD/YYYY')
  AND decode(substr(fpbpohd_code,1,3),'UI1','1'||substr(fpbpohd_code,4,1),
               decode(substr(fpbpohd_code,1,2),'UI','1'||substr(fpbpohd_code,3,1),
               decode(substr(fpbpohd_code,3,2),'16','15',
               substr(fpbpohd_code,3,2)))) = FTVFSYR_FSYR_CODE;
