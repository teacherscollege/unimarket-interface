--------------------------------------------------------
--  File created - Monday-August-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function TC_GET_EMAIL_ADDRESS
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "FISHRS"."TC_GET_EMAIL_ADDRESS" (p_pidm NUMBER) RETURN VARCHAR2
IS 
   v_email_address  general.goremal.goremal_email_address%TYPE := NULL;  
   CURSOR goremal_cursor (parm_pidm number) IS  
      SELECT goremal_email_address
        FROM general.goremal
       WHERE parm_pidm = goremal_pidm
         AND goremal_status_ind    = 'A'
         AND goremal_preferred_ind = 'Y'
       ORDER BY goremal_activity_date DESC;                            
   goremal_email_info goremal_cursor%ROWTYPE;
BEGIN
   OPEN goremal_cursor(p_pidm);
   FETCH goremal_cursor into goremal_email_info; 
   IF goremal_cursor%NOTFOUND 
   THEN
      v_email_address := NULL;
   ELSE
      v_email_address := goremal_email_info.goremal_email_address;
   END IF;               
   CLOSE goremal_cursor;
   RETURN v_email_address;   
EXCEPTION
   WHEN OTHERS THEN
       RETURN NULL;
END; 

/
