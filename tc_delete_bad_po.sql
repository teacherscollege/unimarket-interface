--------------------------------------------------------
--  File created - Monday-August-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure TC_DELETE_BAD_PO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "FISHRS"."TC_DELETE_BAD_PO" (p_option    VARCHAR2 DEFAULT 'V',
                            p_po_code   VARCHAR2)
                             
IS 
   program_id CONSTANT CHAR(17) := 'TC_DELETE_BAD_PO ';
   v_sql_sqlcode       NUMBER := 0;
   v_sql_sqlerrm       VARCHAR2 (512);   
---
   v_pidm NUMBER(8,0) := NULL;
   v_name VARCHAR2(100);
   v_fbpohd_count   NUMBER(5) := 0;
   v_fprpoda_count  NUMBER(5) := 0;
   v_fprpodt_count  NUMBER(5) := 0;
   v_fgbtrnd_count  NUMBER(5) := 0;
   v_fgrbako_count  NUMBER(5) := 0;
   v_fpbpohd  fimsmgr.fpbpohd%ROWTYPE;
---   
BEGIN
   DBMS_OUTPUT.PUT_LINE(program_id||'STARTED OPTION='||p_option||' PO='||
                             p_po_code ); 
   IF UPPER(p_option) IS NULL OR UPPER(p_option) NOT IN ('V','D')
   THEN
   	  DBMS_OUTPUT.PUT_LINE('FORMAT: tc_delete_bad_po(V/D, PO ID)');
      DBMS_OUTPUT.PUT_LINE('ENTER OPTION PARM V=VERFIY D=DELETE');
   	  DBMS_OUTPUT.PUT_LINE(program_id||'ENDED - DID NOTHING');
   	  RETURN; 
   END IF;
   BEGIN
      SELECT *
        INTO v_fpbpohd
        FROM fpbpohd
       WHERE fpbpohd_code = p_po_code;
   EXCEPTION
     WHEN NO_DATA_FOUND 
     THEN 
          DBMS_OUTPUT.PUT_LINE('PURCHASE ORDER '|| p_po_code || ' NOT FOUND!');
          v_fpbpohd.fpbpohd_code := NULL;
   END;
   IF v_fpbpohd.fpbpohd_code IS NULL
   THEN
   	  DBMS_OUTPUT.PUT_LINE(program_id||'ENDED - DID NOTHING***');
   	  RETURN;
   END IF;
   SELECT COUNT(*) 
     INTO v_fgbtrnd_count
     FROM fgbtrnd 
    WHERE fgbtrnd_doc_code = p_po_code;
   DBMS_OUTPUT.PUT_LINE( 'fgbtrnd='||v_fgbtrnd_count);    
   IF v_fgbtrnd_count > 0
   THEN     
      DBMS_OUTPUT.PUT_LINE('PO HAS BEEN POSTED, CANNOT DELETE THIS PO.');
   	  DBMS_OUTPUT.PUT_LINE(program_id||'ENDED - DID NOTHING***');
   	  RETURN;
   END IF;
   DBMS_OUTPUT.PUT_LINE( 'fgrbako='||v_fgrbako_count);   
   SELECT COUNT(*) 
     INTO v_fgrbako_count
     FROM fgrbako 
    WHERE fgrbako_doc_num = p_po_code;
   IF v_fgrbako_count > 0
   THEN     
      DBMS_OUTPUT.PUT_LINE('POSTING FGRBAKO RECORDS FOUND - CANNOT DELETE THIS PO.');
   	  DBMS_OUTPUT.PUT_LINE('***ENDED - DID NOTHING');
   	  RETURN;
   END IF; 
  
   IF UPPER(p_option) = 'V'
   THEN 
       DBMS_OUTPUT.PUT_LINE('VERIFICATION OPTION COMPLETE'); 
       DBMS_OUTPUT.PUT_LINE( 'PO ID.....' || v_fpbpohd.fpbpohd_code);
       DBMS_OUTPUT.PUT_LINE( 'DATE......' || TO_CHAR(v_fpbpohd.FPBPOHD_PO_DATE,'MM/DD/YYYY'));
       DBMS_OUTPUT.PUT_LINE( 'COMPLETE..' || v_fpbpohd.fpbpohd_complete_ind);
       DBMS_OUTPUT.PUT_LINE( 'PRINT.....' || v_fpbpohd.fpbpohd_print_ind);
       DBMS_OUTPUT.PUT_LINE( 'CLOSED....' || v_fpbpohd.fpbpohd_closed_ind);
       DBMS_OUTPUT.PUT_LINE( 'SUSPENDED.' || v_fpbpohd.FPBPOHD_SUSP_IND);
       DBMS_OUTPUT.PUT_LINE(program_id||'ENDED***');
       RETURN; 
   END IF;
 -- DO THE UPDATE   
   
   DBMS_OUTPUT.PUT_LINE('STARTING DELETE OF ' || p_po_code||'***');
   DELETE from FPRPODA
     WHERE FPRPODA_POHD_CODE = p_po_code;        
   DBMS_OUTPUT.PUT_LINE('FPRPODA DELETE COUNT = '||sql%rowcount);
   DELETE from FPRPODT
     WHERE FPRPODT_POHD_CODE = p_po_code;        
   DBMS_OUTPUT.PUT_LINE('FPRPODT DELETE COUNT = '||sql%rowcount);   
   DELETE from FPBPOHD 
     WHERE FPBPOHD_CODE = p_po_code;        
   DBMS_OUTPUT.PUT_LINE('FPBPOHD DELETE COUNT = '||sql%rowcount);       
   COMMIT;
   DBMS_OUTPUT.PUT_LINE(program_id||'ENDED PO DELETED***');   

EXCEPTION
   WHEN OTHERS THEN
         v_sql_sqlcode := SQLCODE;
         v_sql_sqlerrm := SQLERRM;
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (program_id||'UNHANDLED ERROR:'||V_SQL_SQLCODE||' - '||V_SQL_SQLERRM);
         tcapp.util_msg.LogEntry(program_id||'UNHANDLED ERROR:'||V_SQL_SQLCODE||' - '||V_SQL_SQLERRM);
END;

/
