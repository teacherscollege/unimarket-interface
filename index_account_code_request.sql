--------------------------------------------------------
--  File created - Monday-August-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure INDEX_ACCOUNT_CODE_REQUEST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "FISHRS"."INDEX_ACCOUNT_CODE_REQUEST" 
IS
  v_rsp CLOB;
  v_msg VARCHAR2(1000);
BEGIN
  fzkeprc_va.p_account_code_request(v_rsp , v_msg);
  INSERT INTO INDEX_SAVE (JS_MESSAGE, JS_RESPONSE) VALUES (v_msg, v_rsp);
COMMIT;
END;

/
