--------------------------------------------------------
--  File created - Thursday-November-15-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View FZVEPRC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "FISHRS"."FZVEPRC" ("FZVEPRC_FMT_PRT", "FZVEPRC_PICKLIST_PRT", "FZVEPRC_PICKLIST_DESC", "FZVEPRC_PICKLIST_PRT_NUMBER", "FZVEPRC_REQUIRED", "FZVEPRC_PARENT_COAS") AS 
  select
/*******************************************************************************
  UNIVERSITY OF NEW HAMPSHIRE - Project Unimarket eProcurement Interface
  NAME:    create_view_fzveprc.sql
  DESC:    Script to create view which returns valid FOAPAL elements to validate
           User Default FOAPAL for Unimarket eProcurement Interface (FZKEPRC_SD).
--
           Also used by All Funds Budgeting form FZAPJED.
           Also used by PRAM/CRAM/DIDM.
--
  Modifications:
  Version  Date        Author       Comments
  -------  ----------  -----------  --------------------------------------------
  1.0      09/02/2010  SERGEYV      Initial
  2.0      11/24/2010  SERGEYV      Added order by
  2.1      01/04/2011  SERGEYV      Added grants to dblink accounts
  2.2      01/31/2011  SERGEYV      Added FZVEPRC_ATTRIBUTE_VALUE
  2.3      02/23/2011  SERGEYV      Changed FTVSDAT_SDAT_CODE_ENTITY from FZKAFMT to FZKEPRC
  3.0      10/25/2011  SERGEYV      Added FZVEPRC_PARENT_COAS
  3.1      11/02/2011  SERGEYV      ORGN select fixed
  3.2      02/08/2012  SERGEYV      Removed FZVEPRC_ATTRIBUTE_VALUE
  3.3      04/20/2012  SERGEYV      Removed order by clause
*******************************************************************************/
       fzveprc_fmt_prt,
       fzveprc_picklist_prt,
       fzveprc_picklist_desc,
       ftvsdat_code_level  fzveprc_picklist_prt_number,
       lower(ftvsdat_data) fzveprc_required,
       fzveprc_parent_coas
  from(select 'COAS'            fzveprc_fmt_prt,
              ftvcoas_coas_code fzveprc_picklist_prt,
              ftvcoas_title     fzveprc_picklist_desc,
              NULL              fzveprc_parent_coas
         from ftvcoas where ftvcoas_status_ind = 'A'
          and nvl(ftvcoas_term_date,sysdate+1)>sysdate
          and sysdate>=ftvcoas_eff_date and sysdate<ftvcoas_nchg_date
        union all
       select 'FUND', ftvfund_fund_code, ftvfund_title, ftvfund_coas_code
         from ftvfund
        where ftvfund_status_ind = 'A'
          and ftvfund_data_entry_ind = 'Y'
          and nvl(ftvfund_term_date,sysdate+1)>sysdate
          and sysdate>=ftvfund_eff_date and sysdate<ftvfund_nchg_date
        union all
       select 'ORGN', ftvorgn_orgn_code, ftvorgn_title, ftvorgn_coas_code
         from ftvorgn
        where ftvorgn_status_ind = 'A'
          and ftvorgn_data_entry_ind = 'Y'
          and nvl(ftvorgn_term_date,sysdate+1)>sysdate
          and sysdate>=ftvorgn_eff_date and sysdate<ftvorgn_nchg_date
        union all
       select 'ACCT', ftvacct_acct_code, ftvacct_title, ftvacct_coas_code
         from ftvacct, ftvatyp, ftvsdat
        where ftvacct_status_ind = 'A'
          and ftvatyp_status_ind = 'A'
          and ftvsdat_status_ind = 'A'
          and ftvacct_coas_code = ftvatyp_coas_code
          and ftvacct_atyp_code = ftvatyp_atyp_code
          and ftvatyp_coas_code = ftvsdat_coas_code
          and ftvatyp_internal_atyp_code = ftvsdat_sdat_code_opt_1
          and ftvsdat_sdat_code_entity = 'FZKEPRC'
          and ftvsdat_sdat_code_attr   = 'ATYP_INT_ALLOW'
          and ftvacct_data_entry_ind   = 'Y'
          and ftvsdat_eff_date<=sysdate
          and nvl(ftvacct_term_date,sysdate+1)>sysdate
          and nvl(ftvatyp_term_date,sysdate+1)>sysdate
          and nvl(ftvsdat_term_date,sysdate+1)>sysdate
          and nvl(ftvsdat_nchg_date,sysdate+1)>sysdate
          and sysdate>=ftvacct_eff_date and sysdate<ftvacct_nchg_date
          and sysdate>=ftvatyp_eff_date and sysdate<ftvatyp_nchg_date
        union all
       select 'PROG', ftvprog_prog_code, ftvprog_title, ftvprog_coas_code
         from ftvprog
        where ftvprog_status_ind = 'A'
          and ftvprog_data_entry_ind = 'Y'
          and nvl(ftvprog_term_date,sysdate+1)>sysdate
          and sysdate>=ftvprog_eff_date and sysdate<ftvprog_nchg_date
        union all
       select 'ACTV', ftvactv_actv_code, ftvactv_title, ftvactv_coas_code
         from ftvactv
        where ftvactv_status_ind = 'A'
          and nvl(ftvactv_term_date,sysdate+1)>sysdate
          and sysdate>=ftvactv_eff_date and sysdate<ftvactv_nchg_date
        union all
       select 'LOCN', ftvlocn_locn_code, ftvlocn_title, ftvlocn_coas_code
         from ftvlocn
        where ftvlocn_status_ind = 'A'
          and nvl(ftvlocn_term_date,sysdate+1)>sysdate
          and sysdate>=ftvlocn_eff_date and sysdate<ftvlocn_nchg_date
      ) a, ftvsdat
 where ftvsdat_sdat_code_entity = 'FZKEPRC'
   and ftvsdat_sdat_code_attr   = 'CODE-FORMAT-PART'
   and ftvsdat_status_ind       = 'A'
   and ftvsdat_sdat_code_opt_1  = a.fzveprc_fmt_prt
   and ftvsdat_eff_date<=sysdate
   and nvl(ftvsdat_term_date,sysdate+1)>sysdate
   and nvl(ftvsdat_nchg_date,sysdate+1)>sysdate;
