CREATE OR REPLACE PACKAGE        "TC_UNIMKT_UTIL" IS
/*******************************************************************************

   NAME:       TC_UNIMKT_UTIL

   PURPOSE:    This package is Unimarket utility package for Unimarket testing

               Procedure p_test_f_request runs a test request built in the tcreqt table
                           saves the response in the TCREQT_RESPONSE column in 
                           tcreqt table.
               Procedure p_copy_request - copies a request from fzkeprc table 
                            based on FZBEPRC_ID into TCREQT_REQ column in 
                           tcreqt table.

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  01/27/2014  John Stark    Initial.
   1.1  06/27/2014  John Stark    added p_update_account_codes proc.   
 

*******************************************************************************/
                    
procedure p_test_f_request(p_id NUMBER, p_orig varchar2 := 'EPUNIMARKET');
procedure p_copy_request (p_fzbeprc_id fzbeprc.fzbeprc_id%type, 
                          p_id NUMBER);
procedure p_copy_request_po (p_fzbeprc_id fzbeprc.fzbeprc_id%type, 
                          p_id NUMBER);  
procedure p_update_account_codes(p_tcreqt_id NUMBER);                                                  
---=============================================================================
END TC_UNIMKT_UTIL;
/


CREATE OR REPLACE PACKAGE BODY        TC_UNIMKT_UTIL IS
/*******************************************************************************

   NAME:       TC_UNIMKT_UTIL

   PURPOSE:    This package is Unimarket utility package for Unimarket testing

               Procedure p_test_f_request runs a test request built in the tcreqt table
                           saves the response in the TCREQT_RESPONSE column in 
                           tcreqt table.
               Procedure p_copy_request - copies a request from fzkeprc table 
                            based on FZBEPRC_ID into TCREQT_REQ column in 
                           tcreqt table.
   
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  01/27/2014  John Stark    Initial.                          
   1.1  06/27/2014  John Stark    added p_update_account_codes proc.         
*******************************************************************************/

procedure p_copy_request (p_fzbeprc_id fzbeprc.fzbeprc_id%type, 
                          p_id NUMBER)
IS 
   v_id   tcreqt.tcreqt_id%type := 1;
   v_req  fzbeprc.fzbeprc_req%type := NULL;
BEGIN
  IF p_id IS NOT NULL 
  THEN 
     v_id := p_id;
  END IF;
  DELETE FROM  tcreqt
         WHERE tcreqt_id = v_id;

  SELECT FZBEPRC_REQ
    INTO v_req
    FROM FZBEPRC
   WHERE FZBEPRC_ID = p_fzbeprc_id;

  INSERT INTO tcreqt (TCREQT_ID, TCREQT_REQ)
              VALUES (v_id, v_req);
  commit;
end;

procedure p_copy_request_po (p_fzbeprc_id fzbeprc.fzbeprc_id%type, 
                          p_id NUMBER)
IS 
   v_id   tcreqt.tcreqt_id%type := 1;
   v_req  fzbeprc.fzbeprc_req%type := NULL;
BEGIN
  IF p_id IS NOT NULL 
  THEN 
     v_id := p_id;
  END IF;
  DELETE FROM  tcreqt
         WHERE tcreqt_id = v_id;

  SELECT FZRCXML_DOC
    INTO v_req
    FROM FZRCXML
   WHERE FZRCXML_EPRC_ID = p_fzbeprc_id;

  INSERT INTO tcreqt (TCREQT_ID, TCREQT_REQ)
              VALUES (v_id, v_req);
  commit;
end;

procedure  p_test_f_request(p_id NUMBER, p_orig varchar2 := 'EPUNIMARKET')
IS 
   v_id       tcreqt.tcreqt_id%type := 1;
   v_req      tcreqt.tcreqt_req%type := NULL;
   v_response tcreqt.tcreqt_response%type := NULL;
BEGIN
  IF p_id IS NOT NULL 
  THEN 
     v_id := p_id;
  END IF;
  
  SELECT tcreqt_req
    INTO v_req
    FROM tcreqt
   WHERE tcreqt_ID = v_id;

   v_response := fzkeprc.f_request(v_req, p_orig);
   
   UPDATE tcreqt
      SET tcreqt_response = v_response
    WHERE tcreqt_id = v_id;
   commit;
end;

procedure  p_update_account_codes(p_tcreqt_id NUMBER)
IS 
   v_id       tcreqt.tcreqt_id%type := 1;
   v_req      tcreqt.tcreqt_req%type := '<account-code-request xmlns="http://www.unimarket.com/schema/unimarket-ws-1.1"/>' ;
   v_response tcreqt.tcreqt_response%type := NULL;
BEGIN
  IF p_tcreqt_id IS NOT NULL 
  THEN 
     v_id := p_tcreqt_id;
  END IF;  
  delete
    FROM tcreqt
   WHERE tcreqt_ID = v_id;
  v_response := fzkeprc.f_request(v_req, 'EPUNIMARKET');
  INSERT INTO tcreqt (TCREQT_ID, TCREQT_REQ, tcreqt_response)
              VALUES (v_id, v_req, v_response);
  commit;  
end;
---=============================================================================
END TC_UNIMKT_UTIL;
/
