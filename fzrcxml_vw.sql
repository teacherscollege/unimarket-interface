--------------------------------------------------------
--  File created - Monday-August-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Materialized View FZRCXML_VW
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "FISHRS"."FZRCXML_VW" ("FZRCXML_EPRC_ID", "INVOICE_ID", "ORDER_ID", "VENDOR_ID", "INVOICE_DATE", "NET_AMOUNT", "VENDOR_INVOICE", "ERROR_MSG", "ACTIVITY_DATE")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "DEVELOPMENT" 
  BUILD IMMEDIATE
  USING INDEX 
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT trunc(sysdate+1)+4/24
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT
fzrcxml_eprc_id,
FZRCXML_DOC_CODE AS INVOICE_ID,
EXTRACTVALUE(xmltype(FZRCXML_DOC),'//InvoiceDetailOrderInfo/OrderReference/@orderID') AS ORDER_ID,
EXTRACTVALUE(xmltype(FZRCXML_DOC),'//Header/From/Credential/Identity') AS Vendor_ID,
to_date(SUBSTR(EXTRACTVALUE(xmltype(FZRCXML_DOC),'//InvoiceDetailRequestHeader/@invoiceDate'),1,10),'YYYY-MM-DD') AS INVOICE_DATE,
to_number(EXTRACTVALUE(xmltype(FZRCXML_DOC),'//InvoiceDetailSummary/NetAmount/Money')) AS NET_AMOUNT,
EXTRACTVALUE(xmltype(FZRCXML_DOC),'//InvoiceDetailRequestHeader/@invoiceID') AS VENDOR_INVOICE,
REPLACE(REPLACE(NVL(FZRCXML_ERROR_MSG,'NO ACTIVITY'),CHR(13),' '),CHR(10),' ') AS ERROR_MSG, 
fzrcxml_activity_date as ACTIVITY_DATE
FROM  FZRCXML;

   COMMENT ON MATERIALIZED VIEW "FISHRS"."FZRCXML_VW"  IS 'snapshot table for snapshot FISHRS.FZRCXML_VW';
  GRANT SELECT ON "FISHRS"."FZRCXML_VW" TO PUBLIC;
