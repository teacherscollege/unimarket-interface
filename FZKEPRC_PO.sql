CREATE OR REPLACE PACKAGE                             "FZKEPRC_PO" IS
/*******************************************************************************

   NAME:       FZKEPRC_PO

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure p_OrderRequest gets
                               <OrderRequest>
                         and creates PO calling Banner PO API
                               fpkpurr.p_create_po

   NOTES:      Procedure p_OrderRequest called from FZKEPRC
   
   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20121212    J Stark       Add Messah procedures to expand account index
                             FIX SYNONYM
   20160601    D Semar       Add modifications for dual FY capability
   20180604    D Semar       Implement PO Close from St. Louis U.

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/20/2011  S.Vorotnikov  Initial (moved from FZKEPRC).
   2.0  10/14/2011  S.Vorotnikov  Added g_Item_Tax
   3.0  05/08/2012  S.Vorotnikov  Release liquidation added
   3.1  06/22/2012  S.Vorotnikov  Added p_create_jv_schd
*******************************************************************************/
--
-- OrderRequest members
   g_deploymentMode        constant varchar2(100):='deploymentMode';
   g_orderID               constant varchar2(100):='orderID';
   g_agreementID           constant varchar2(100):='agreementID';
   g_requestType           constant varchar2(100):='requestType';
   g_orderType             constant varchar2(100):='orderType';
   g_orderDate             constant varchar2(100):='orderDate';
 -- TC MOD 20160601 START --------------------------------------------  
   g_fy                    constant varchar2(100) := 'FY_code';
   g_fy_start_date         constant varchar2(100) := 'FY_StartDate';
   g_fy_end_date           constant varchar2(100) := 'FY_EndDate';
 -- TC MOD 20160601 END  ---------------------------------------------
   g_effectiveDate         constant varchar2(100):='effectiveDate';
   g_Identity              constant varchar2(100):='Identity';
   g_addressID             constant varchar2(100):='addressID';
   g_Total_Money           constant varchar2(100):='Total_Money';
   g_currency              constant varchar2(100):='currency';
   g_DeliverTo             constant varchar2(100):='DeliverTo';
   g_shippingNote          constant varchar2(100):='shippingNote';
   g_PCard                 constant varchar2(100):='PCard';
   g_Shipping_Money        constant varchar2(100):='Shipping_Money';
   g_DeliveryDate          constant varchar2(100):='DeliveryDate';
   g_SupplierID            constant varchar2(100):='SupplierID';
   g_req_justification     constant varchar2(100):='req_justification';
--
   g_Item_Item             constant varchar2(100):='Item_Item';
   g_Item_lineNumber       constant varchar2(100):='Item_lineNumber';
   g_Item_quantity         constant varchar2(100):='Item_quantity';
   g_Item_DeliveryDate     constant varchar2(100):='Item_DeliveryDate';
   g_Item_UnitOfMeasure    constant varchar2(100):='Item_UnitOfMeasure';
   g_Item_Classification   constant varchar2(100):='Item_Classification';
   g_Item_Description      constant varchar2(100):='Item_Description';
   g_Item_UnitPrice        constant varchar2(100):='Item_UnitPrice';
   g_Item_ShipTo           constant varchar2(100):='Item_ShipTo';
   g_Item_Comments         constant varchar2(100):='Item_Comments';
   g_Item_Tax              constant varchar2(100):='Item_Tax';
   g_Item_requisitionID    constant varchar2(100):='Item_requisitionID';
--
   g_Accounting_Distr      constant varchar2(100):='Accounting_Distr';
   g_Accounting_Charge     constant varchar2(100):='Accounting_Charge';
   g_Accounting_Segment    constant varchar2(100):='Accounting_Segment';
   g_Accounting_Name       constant varchar2(100):='Accounting_Name';
   g_Accounting_id         constant varchar2(100):='Accounting_id';
--
-- <DGS 20180604>  Start
-- StatusUpdateRequest members and procedure
   g_unimarket_order_number          constant varchar2(100):='unimarket-order-number';
   g_unimarket_order_state           constant varchar2(100):='unimarket-order-state';
   g_unimarket_changed_by_user       constant varchar2(100):='unimarket-changed-by-username';
--
procedure p_CloseOrder(p_rsp in out CLOB, p_msg in out varchar2);
--
-- <DGS 20180604>  End
--

procedure p_OrderRequest(p_rsp in out CLOB, p_msg in out varchar2);
procedure p_create_jv_schd
           (p_user        varchar2,
            p_pohd_code   fpbpohd.fpbpohd_code%type,
            p_order_ID    fzrcxml.fzrcxml_doc_id%type,
            p_jv_type     varchar2,
            p_jv_docno    fgbjvch.fgbjvch_doc_num%type,
            p_jv_trans_dt varchar2,
            p_jv_tr_desc  fgbjvch.fgbjvch_doc_description%type,
            p_jv_tot_amt  fgbjvch.fgbjvch_doc_amt%type,
            p_jv_rucl     fgbjvcd.fgbjvcd_rucl_code%type,
            p_jv_act      fgbjvcd.fgbjvcd_encb_action_ind%type,
            p_orig        fgbjvch.fgbjvch_data_origin%type,
            p_schedule    ftvsdat.ftvsdat_sdat_code_opt_1%type := null);
--
---=============================================================================
END FZKEPRC_PO;
/


CREATE OR REPLACE PACKAGE BODY                                                                                                                                                                  FZKEPRC_PO IS
/*******************************************************************************

   NAME:       FZKEPRC_PO

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure p_OrderRequest gets
                               <OrderRequest>
                         and creates PO calling Banner PO API
                               fpkpurr.p_create_po

   NOTES:      Procedure p_OrderRequest called from FZKEPRC
   
   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20121212    J Stark       Add Messah procedures to expand account index
   20130422    J Stark       Fix for special characters in item description 
   20130425    J Stark       Email if PO is created with items in NSF Status.
                             Remove NSF override.
   20130917    J Stark       Add vendor code to header error messages.
   20131211    J Stark       Cancel Purchase Order.          
   20140306    J Stark       Back out PO if insufficient funds.  
   20140408    J Stark       Fix size character string buffer too small error
                             by converting Windows-1252 special character 
                             encodings.
  20140428     J Stark       Fix PO create issue when commodity description is 
                             blank. 
  20140812     J Stark       Allow NSF override for 'release' orders.  
                             Initialize g_jv_type for each header.     
  20141007     J Stark       Do not generate JV for blanket on release orders
                             when account is an Asset Account (does not begin 
                             with a '7'). 
  20141210     J Stark       Additional enhancement to fix 20140408 added characters � and �
  20160601     D Semar       Added modifications for dual FY capability
  20161025     D Semar       Modifications for dual FY for Encumbrance roll and null g_po_fy
  20161110     D Semar       Modified call to sokemal.p_sendemail for new parameters.
  20170128     D Semar       Modifications for error reduction
  20170414     D Semar       Modifications for $0 extended amount error
  20180511     D Semar       Implemented bug fix from John Harris at SLU to move insert to fobappd to below the error checking.
                              The avoids Banner creating an unexpected encumbrance in the old FY.  <DGS 05112018>
  20180604     D Semar       Implemented PO Close modifications from St. Louis U. Chg 34923  <DGS 20180604>
   
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/20/2011  S.Vorotnikov  Initial (moved from FZKEPRC).
   2.0  10/14/2011  S.Vorotnikov  Added po_tax_amt = f_get_val(g_Item_Tax,true)
   3.0  02/21/2012  S.Vorotnikov  Response structure changed (Ticket #19427)
   4.0  04/26/2012  S.Vorotnikov  Allow blanket/release orders
   4.1  04/26/2012  S.Vorotnikov  Shipping item po_uom changed to 'EA'
   4.2  04/27/2012  S.Vorotnikov  PO_DOCNO logic changed
                                     IF FZREPRF_PO_DOCNO = 'N' then auto-assign Banner Purchase Order number
                                     ELSE generate custom Banner document number:
                                          IF 'Y', then
                                             substr(FOBPROF_REQUESTER_ORGN_CODE,1,2)||substr(orderID,-6)
                                          ELSE
                                             FZREPRF_PO_DOCNO||substr(orderID,-6)
   4.3  05/08/2012  S.Vorotnikov  Release liquidation added
   4.4  06/22/2012  S.Vorotnikov  Added p_create_jv_schd
   4.5  08/22/2012  S.Vorotnikov  Response structure changed (Ticket #26417)
   4.6  10/23/2012  S.Vorotnikov  Bug in procedure p_create_jv_schd fixed
                                     'pcard' changed to 'PCard'
*******************************************************************************/
--
   g_order_ID         fzrcxml.fzrcxml_doc_id%type;
   g_fzrcxml          fzrcxml%rowtype;
   g_doc_lvl          fzreprf.fzreprf_doc_lvl_acctg%type;
--
   g_header_rec       fpkpurr.po_headentry_rectype;
   g_item_table       fpkpurr.po_itemsentry_tabtype;
   g_acct_table       fpkpurr.po_acctgentry_tabtype;
--
   g_vend_id          spriden.spriden_id%type;
   g_doc_ref_code     fpbpohd.fpbpohd_doc_ref_code%type;
   g_doc_req_id       fpbpohd.fpbpohd_doc_ref_code%type;
   g_jv_docno         gurfeed.gurfeed_doc_code%type;
   g_trans_desc       gurfeed.gurfeed_trans_desc%type;
   g_orgn             fobprof.fobprof_requester_orgn_code%type;
   g_total_amt        fprpoda.fprpoda_amt%type;
   g_shipping         Fpkutil.max_data_entry;
   g_pcard_number     Fpkutil.max_data_entry;
   g_error_mesg_table fpkutil.error_mesg_tabtype;
--
   g_blanket_ID       fzrcxml.fzrcxml_doc_id%type;
   g_blanket_code     fpbpohd.fpbpohd_code%type;
   g_jv_trans_dt      fpbpohd.fpbpohd_trans_date%type;
--
   g_po_acct_level    varchar2(1);
   g_po_type          varchar2(10);
   g_jv_type          varchar2(10);
   g_jv_act           fgbjvcd.fgbjvcd_encb_action_ind%type;
   g_jv_rucl          fgbjvcd.fgbjvcd_rucl_code%type;
   g_jv_pohd_code     fpbpohd.fpbpohd_code%type;
   g_jv_user_id       fgbjvch.fgbjvch_user_id%type;
   g_schedule         ftvsdat.ftvsdat_sdat_code_opt_1%type;
   -- TC MOD 20131211 START ------------------------------------------------------     
   g_request_type     varchar2(10);   
   -- TC MOD 20131211 END   ------------------------------------------------------ 
   -- TC MOD 20160601 START ------------------------------------------------------
   g_po_fy            ftvfsyr.ftvfsyr_fsyr_code%type;
   curr_fsyr          ftvfsyr.ftvfsyr_fsyr_code%type;
   g_po_fy_start_date    ftvfsyr.ftvfsyr_start_date%type;
   g_po_fy_end_date      ftvfsyr.ftvfsyr_end_date%type;
   -- TC MOD 20160601 END ------------------------------------------------------
   
   -- <DGS 20180604>  Start 
   prior_fsyr          fgbjvcd.fgbjvcd_fsyr_code%type;
   prior_po            VARCHAR2(01);
--  <DGS 20180604>  Ends

--==============================================================================
--
function f_fspd_open return boolean is
   s varchar2(1);
begin
   select ftvfspd_prd_status_ind into s
     from ftvfspd, fobprof
    where ftvfspd_coas_code = fobprof_coas_code
      and fobprof_user_id = upper(fzkeprc.g_user)
      and to_date(g_header_rec.po_trans_date, G$_DATE.GET_NLS_DATE_FORMAT)
          between trunc(ftvfspd_prd_start_date) and trunc(ftvfspd_prd_end_date);
   if s = 'O' then return true;
   else return false;
   end if;
exception when others then
   return false;
end;
--
--==============================================================================
--
-- TC MOD 20130425 START ------------------------------------------------------  
procedure p_send_nsf_po_email(p_po_code varchar2, p_msg varchar2) is
   l_from varchar2(100);
   l_subj varchar2(500);
   l_to varchar2(32000);
   l_cc varchar2(32000);
   v_mail_successful VARCHAR2(300) := 'T' ; 
   v_mail_errmsg     VARCHAR2(300) := 'F' ;    
-- TC MOD 20161110 Start   ------------------------------------------------------        
   v_error_type varchar2(100);
   v_error_code varchar2(100);
   v_error_message varchar2(3000);
-- TC MOD 20161110 End   ------------------------------------------------------     
begin
--
   l_subj := 'Unimarket: Order '||p_po_code||' is NSF.';
--
   for e in(select upper(ftvsdat_sdat_code_opt_1) opt, ftvsdat_title email from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and upper(ftvsdat_sdat_code_attr) = 'UM PONSF EMAIL'
               and upper(ftvsdat_sdat_code_opt_1) in('FROM','TO','CC')
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) loop
      if  e.opt='FROM' then l_from := e.email;
      elsif e.opt='TO' then l_to := l_to||trim(e.email)||', ';
      elsif e.opt='CC' then l_cc := l_cc||trim(e.email)||', '; end if;
   end loop;
   l_to := rtrim(l_to,', '); l_cc := rtrim(l_cc,', ');
     if l_from is not null and l_to is not null then
-- TC MOD 20161110 Start   ------------------------------------------------------             
   sokemal.p_sendemail (email_addr      => l_to,                   
                        email_to_name   => NULL,
                        email_from_addr => l_from,
                        email_from_name => 'Unimarket - NO REPY',
                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                        email_subject   => l_subj,
                        email_message   => p_msg,
                        email_success   => v_mail_successful,
                        email_reply     => v_mail_errmsg,
                        email_error_type => v_error_type,
                        email_error_code => v_error_code,
                        email_error_message => v_error_message);    
-- TC MOD 20161110 End   ------------------------------------------------------   
   else
      dbms_output.put_line('ERROR in p_send_nsf_req_email: Can not send email!!!');
   end if;
--
exception when others then
   dbms_output.put_line('ERROR in p_send_nsf_po_email: '||SQLERRM);
end p_send_nsf_po_email;

procedure p_check_nsf_po(p_po_code IN varchar2) is
   l_msg varchar2(4000) := NULL;
   crlf VARCHAR2( 2 ):= CHR( 13 ) || CHR( 10 );
cursor po_nsf_cursor(p_po_code VARCHAR2) IS                          
   SELECT FPRPODA_ACCI_CODE, 
          FPRPODA_ACCT_CODE 
   FROM 
       FPRPODA
   WHERE fprpoda_pohd_code = p_po_code
     AND FPRPODA_NSF_SUSP_IND = 'Y'
   ORDER BY 1; 
   po_nsf_info po_nsf_cursor%ROWTYPE;   
begin
   open po_nsf_cursor(p_po_code);
   loop
      fetch po_nsf_cursor into po_nsf_info;
      EXIT WHEN po_nsf_cursor%NOTFOUND;
      l_msg := l_msg||po_nsf_info.FPRPODA_ACCI_CODE||'-'||po_nsf_info.FPRPODA_ACCT_CODE||crlf; 
   end loop;
   close po_nsf_cursor;
   IF l_msg IS NOT NULL THEN
      l_msg := 'Purchase Order: '
               ||p_po_code
               ||' has been created by Unimarket and is in NSF status.'
               ||crlf
               ||'Below is a list of the index/account combinations for this order:'||crlf
               ||crlf
               || l_msg
               ||crlf
               || 'End of list.'||crlf;
      p_send_nsf_po_email(p_po_code, l_msg);                
   END IF;
   RETURN;
exception when others then
   dbms_output.put_line('ERROR in p_send_nsf_po_email: '||SQLERRM);
end p_check_nsf_po;
-- TC MOD 20130425 END   ------------------------------------------------------ 

procedure p_get_po_data(p_msg in out varchar2) is
   l_max_itemno Fpkutil.max_data_entry := '-1';
   l_req_type   varchar2(100);
   l_depl_type  varchar2(100);
   l_msg        varchar2(4000);
   l_item_path  varchar2(100):=fzkeprc.f_get_path(fzkeprc.OrderRequest, g_Item_Item);
   l_distr_path varchar2(100):=fzkeprc.f_get_path(fzkeprc.OrderRequest, g_Accounting_Distr);
   l_accnt_path varchar2(100):=fzkeprc.f_get_path(fzkeprc.OrderRequest, g_Accounting_Segment);
--------------------------------------------------------------------------------
procedure p_validate_header is
   l_str varchar2(4000);
begin
--
-- 1. Check Deployment Mode
   l_str := upper(fzkeprc.f_get_val(g_deploymentMode));
   if l_str =  'PRODUCTION' and
      upper(fzkeprc.g_banner)<>upper(fzkeprc.g_eprf_rec.fzreprf_prod_dbname) or
      l_str <> 'PRODUCTION' and
      upper(fzkeprc.g_banner) =upper(fzkeprc.g_eprf_rec.fzreprf_prod_dbname) then
      l_msg := 'Wrong Deployment Mode "'||l_str||'" for Banner instance '||fzkeprc.g_banner;
      return;                                                       -- Return!!!
   end if;
--
-- 2. Check PO Type
-- FPBPOHD_PO_TYPE_IND: [R|S] regular|standing purchase order
-- PO API: FPBPOHD_PO_TYPE_IND := nvl(inrec.p_po_type_ind,'R');
-- cXML orderType: (release|regular|blanket) "regular"
   if g_po_type not in('regular', 'blanket', 'release') then
      l_msg := 'Order Type "'||g_po_type||'" not supported';
      return;                                                       -- Return!!!
   end if;
--
-- 3. Check Request Type
-- cXML //OrderRequestHeader/@type
   -- TC MOD 20131211 START ------------------------------------------------------     
   g_request_type := fzkeprc.f_get_val(g_requestType);
   if nvl(g_request_type,'new') NOT IN ('new','delete') then
      l_msg := 'Request Type "'||g_request_type||'" not supported';
      return;                                                       -- Return!!!
   end if;
/*
   l_str := fzkeprc.f_get_val(g_requestType);
   if nvl(l_str,'new') <> 'new' then
      l_msg := 'Request Type "'||l_str||'" not supported';
      return;                                                       -- Return!!!
   end if;
*/   
   -- TC MOD 20131211 END   ------------------------------------------------------     
--
-- 4. Check if required Buyer and Requester ORGN Codes found
   if fzkeprc.g_orig not like 'EPROC%' then
      if g_header_rec.po_buyr_code is null then
         l_msg := 'Buyer Code not found';
         return;
      elsif g_orgn is null then
         l_msg := 'User '||upper(fzkeprc.g_user)||
                 ' does not have REQUESTER_ORGN_CODE assigned in FOBPROF';
         return;
      end if;
   end if;
--
-- 5. Check if PCard PO allowed
   if fzkeprc.g_eprf_rec.fzreprf_pcard_po <> 'Y' and g_pcard_number is not null then
      l_msg := 'PCard Order is not supported';
      return;                                                       -- Return!!!
   elsif fzkeprc.g_eprf_rec.fzreprf_pcard_po = 'Y' and
         g_pcard_number is not null and
         fzkeprc.g_eprf_rec.fzreprf_pcard_jv_rucl is not null then
      g_jv_type := 'PCard';
      g_jv_act  := 'T';
      g_jv_trans_dt := sysdate;                      -- added 06/21/2012 SERGEYV
      g_jv_rucl := fzkeprc.g_eprf_rec.fzreprf_pcard_jv_rucl;
   end if;
--
-- 6. Check Accounting Level
   select decode(existsNode(fzkeprc.g_xml, l_distr_path, fzkeprc.g_namespace),1,'C',0,'D')
     into g_po_acct_level from dual;
   if g_po_acct_level = 'C' then
      if fzkeprc.g_eprf_rec.fzreprf_doc_lvl_acctg = 'D' then
         l_msg := fzkeprc.g_orig||' PO has Commodity Level Accounting.';
      elsif fzkeprc.g_eprf_rec.fzreprf_doc_lvl_acctg = 'S' then
         g_header_rec.po_acctg_type := 'Y';
      end if;
   else
      if fzkeprc.g_eprf_rec.fzreprf_doc_lvl_acctg = 'C' then
         l_msg := fzkeprc.g_orig||' PO has Document Level Accounting.';
      elsif fzkeprc.g_eprf_rec.fzreprf_doc_lvl_acctg in('D','S','A') then
         l_msg := fzkeprc.g_orig||' PO with Document Level Accounting not supported.';
      end if;
   end if;
   if l_msg is not null then
      l_msg := 'EPRF_DOC_LVL_ACCTG='||fzkeprc.g_eprf_rec.fzreprf_doc_lvl_acctg||'. '||l_msg;
      return;
   end if;
--
end p_validate_header;
--------------------------------------------------------------------------------
procedure p_get_po_header is
l_eff_dt date;
begin
--
   g_order_ID                     := fzkeprc.f_get_val(g_orderID,true);
--
   g_header_rec.po_code           := g_order_ID;
   g_header_rec.po_type           := 'R';
   g_header_rec.po_change_seq_num := null;
   g_header_rec.po_userid         := fzkeprc.g_user;
   g_header_rec.po_order_date     := fzkeprc.f_get_val(g_orderDate,true);
   g_header_rec.po_trans_date     := sysdate;
   g_header_rec.po_vend_id        := substr(fzkeprc.f_get_val(g_SupplierID,true),1,9);
   g_header_rec.po_addr_type      := null;
   g_header_rec.po_addr_seq_no    := null;
   g_header_rec.po_buyr_code      := null;
   g_header_rec.po_deliv_date     := fzkeprc.f_get_val(g_DeliveryDate);
   g_header_rec.po_coas           := null;
   g_header_rec.po_orgn           := null;
   g_header_rec.po_ship_code      := fzkeprc.f_get_val(g_addressID,true);
   g_header_rec.po_disc_code      := null;
   g_header_rec.po_print_text     := null;
   g_header_rec.po_noprint_text   := null;
   g_header_rec.po_tgrp_code      := null;
   g_header_rec.po_curr_code      := fzkeprc.f_get_val(g_currency);
-- TC MOD 20121212 START ------------------------------------------------------ 
   IF g_header_rec.po_curr_code = 'USD' THEN
      g_header_rec.po_curr_code := 'USA';
   END IF;
-- TC MOD 20121212 END   ------------------------------------------------------       
   g_header_rec.po_acctg_type     := 'N';
   g_header_rec.po_email_addr     := null;
   g_header_rec.po_fax_area       := null;
   g_header_rec.po_fax_number     := null;
   g_header_rec.po_fax_ext        := null;
   g_header_rec.po_attn_to        := fzkeprc.f_get_val(g_DeliverTo);
   g_header_rec.po_vend_contact   := null;
   g_header_rec.po_deliv_comment  := substr(fzkeprc.f_get_val(g_shippingNote),1,30);
   g_header_rec.po_vend_email     := null;
   g_header_rec.po_requestor      := null;
   g_header_rec.po_phone_area     := null;
   g_header_rec.po_phone_num      := null;
   g_header_rec.po_phone_ext      := null;
   g_header_rec.po_edi_ind        := null;
   g_header_rec.po_ftrm_code      := null;
   g_header_rec.po_origin         := fzkeprc.g_orig;
   g_header_rec.po_doc_ref        := null;
   g_header_rec.po_ctry_code_phone:= null;
   g_header_rec.po_ctry_code_fax  := null;
--
   g_blanket_ID   := substr(fzkeprc.f_get_val(g_agreementID),1,20);
   g_doc_ref_code := g_blanket_ID;
   g_doc_req_id   := substr(fzkeprc.f_get_val(g_Item_requisitionID),1,20);
   g_shipping     := fzkeprc.f_get_val(g_Shipping_Money);
   g_pcard_number := fzkeprc.f_get_val(g_PCard);
   g_vend_id      := g_header_rec.po_vend_id;
   g_po_type      := nvl(fzkeprc.f_get_val(g_orderType), 'regular');
   --TC MOD 20160601 START ----------------------------------------
   g_po_fy        := substr(fzkeprc.f_get_val(g_fy),5,2);   -- hot fix 9/6 removed true from f_get_val
    
  -- TC MOD 20160601 END ------------------------------------------
--
   l_msg := fzkeprc.f_get_val(g_Total_Money, true);
   begin
      g_total_amt := round(to_number(l_msg),2); l_msg := null;
   exception when others then
      l_msg := 'Wrong Total Money format ('||l_msg||')'; return;
   end;
   
    --TC MOD 20161025 START ----------------------------------------
    /* if the po doesn't have an associated FY, default to the min fy of the open periods
       This will cause the prior fy to be selected during the first days of a new year 
       where the encumbrances haven't rolled */
    if g_po_fy is null then   
    
     begin
       select min(ftvfspd_fsyr_code)
         into g_po_fy
         from ftvfspd
        where ftvfspd_coas_code = '1'
        and ftvfspd_prd_status_ind = 'O';
     exception 
       when others then
          l_msg := 'Unable to establish PO fy'; return;
     end;
    else
    --TC MOD 20160601 START ----------------------------------------
    begin
     select ftvfsyr_start_date, ftvfsyr_end_date, ftvfsyr_fsyr_code
     into g_po_fy_start_date, g_po_fy_end_date, g_po_fy
     from ftvfsyr
     where ftvfsyr_fsyr_code = g_po_fy  
      and ftvfsyr_coas_code = '1';
   exception 
     when others then
      l_msg := 'Unable to determine FY start and end dates for FY'||g_po_fy; return;
   end;
  --TC MOD 20160601 END ----------------------------------------
   end if;
   --TC MOD 20161025 END ----------------------------------------
--
-- TC MOD 20140812 START ------------------------------------------------------          
   g_jv_type := NULL;
-- TC MOD 20140812 END   ------------------------------------------------------             
-- Get po_trans_date for blanket PO
   if g_po_type = 'blanket' then
      begin g_header_rec.po_trans_date := fzkeprc.xml2date(fzkeprc.f_get_val(g_effectiveDate));
      exception when others then l_msg := 'Wrong Effective Date format'; return; end;
      if g_header_rec.po_trans_date is null then
         l_msg := 'effectiveDate not found in blanket PO'; return;
      elsif not f_fspd_open then
         g_header_rec.po_trans_date := sysdate;
      end if;
   elsif g_po_type = 'release' then
      g_jv_type := 'blanket';
      g_jv_act  := 'P';
      g_jv_rucl := fzkeprc.g_eprf_rec.fzreprf_release_jv_rucl;
   end if;
--
-- Convert and check dates
   begin g_header_rec.po_order_date:=fzkeprc.xml2date(g_header_rec.po_order_date);
   exception when others then l_msg := 'Wrong Order Date format'; return; end;
--
   begin g_header_rec.po_deliv_date:=fzkeprc.xml2date(g_header_rec.po_deliv_date);
   exception when others then l_msg := 'Wrong Delivery Date format'; return; end;
--
 --TC MOD 20160601 START ----------------------------------------
    fzkeprc.p_get_trans_eff_dt(g_po_fy, l_eff_dt);
    g_header_rec.po_order_date := l_eff_dt;
    g_header_rec.po_trans_date := l_eff_dt;
    
  --TC MOD 20160601 END ----------------------------------------
   if g_header_rec.po_deliv_date is null or
      to_date(g_header_rec.po_deliv_date,fzkeprc.g_date_frmt)<=to_date(g_header_rec.po_order_date,fzkeprc.g_date_frmt)
   then
      g_header_rec.po_deliv_date:=to_char(to_date(g_header_rec.po_order_date,fzkeprc.g_date_frmt)+1,fzkeprc.g_date_frmt);
   end if;
   
--
-- Get Print Text
-- TC MOD 20140408-20141210 START ------------------------------------------------------
/*   if fzkeprc.g_eprf_rec.fzreprf_just_text = 'P' then
      g_header_rec.po_print_text   := fzkeprc.f_get_val(g_req_justification);
   elsif fzkeprc.g_eprf_rec.fzreprf_just_text = 'N' then
      g_header_rec.po_noprint_text := fzkeprc.f_get_val(g_req_justification);
   end if;
*/   
   if fzkeprc.g_eprf_rec.fzreprf_just_text = 'P' then
      g_header_rec.po_print_text   := TRANSLATE(fzkeprc.f_get_val(g_req_justification),'����'||chr(14844292),'''-''''/');
   elsif fzkeprc.g_eprf_rec.fzreprf_just_text = 'N' then
      g_header_rec.po_noprint_text := TRANSLATE(fzkeprc.f_get_val(g_req_justification),'����'||chr(14844292),'''-''''/');
   end if;   
-- TC MOD 20140408-20141210 END   ------------------------------------------------------
--
-- Get Orgn and Buyer Codes
   if fzkeprc.g_eprf_rec.fzreprf_buyr_code is not null then
      g_header_rec.po_buyr_code := fzkeprc.g_eprf_rec.fzreprf_buyr_code;
      begin
         select fobprof_requester_orgn_code
           into g_orgn
           from fobprof
          where fobprof_user_id = upper(fzkeprc.g_user)
            and fobprof_requester_orgn_code is not null;
      exception when no_data_found then
         g_orgn := null;
      end;
   else
      begin
         select fobprof_requester_orgn_code orgn,
                fpkutil.f_get_autobuyr_code
                       (fobprof_coas_code,
                        fobprof_requester_orgn_code,
                        sysdate, NULL) buyr_code
           into g_orgn, g_header_rec.po_buyr_code
           from fobprof
          where fobprof_user_id = upper(fzkeprc.g_user)
            and fobprof_requester_orgn_code is not null
            and fzkeprc.g_eprf_rec.fzreprf_buyr_code is null;
      exception when no_data_found then
         g_orgn := null;
         g_header_rec.po_buyr_code := null;
      end;
   end if;
--
end p_get_po_header;
--------------------------------------------------------------------------------
procedure p_get_po_items is
   itn pls_integer;
   dtn pls_integer;
   atn pls_integer;
   num pls_integer;
   nam varchar2(100);
   val varchar2(100);
   shp number;
   type   FOAPAL_type is table of number index by varchar2(100);
   FOAPAL FOAPAL_type;
   type FOAPAL_rec is record(COAS varchar2(1),
                             ACCI varchar2(6),
                             FUND varchar2(6),
                             ORGN varchar2(6),
                             ACCT varchar2(6),
                             PROG varchar2(6),
                             ACTV varchar2(6),
                             LOCN varchar2(6),
                             amt  number);
   type FOAPAL_recs is table of FOAPAL_rec index by varchar2(100);
   acnt FOAPAL_recs;
-- TC MOD 20121212 START ------------------------------------------------------ 
   ftvacci_rec   fimsmgr.ftvacci%rowtype;
   ftvacci_count number;
-- TC MOD 20121212 END   ------------------------------------------------------ 
--------------------------------------------------------------------------------
function foapal_val(p_part varchar2) return varchar2 is
   l_str varchar2(10);
begin
   select extractvalue(extract(fzkeprc.g_xml,'//AccountingSegment[Name="'||p_part||'"]'),'//@id')
     into l_str from dual; return l_str;
end;
--------------------------------------------------------------------------------
begin
--
   for i in(select value(item) i_xml from table(XMLSequence
                  (extract(fzkeprc.g_xml,l_item_path))) item)
   loop
      atn := 0;
      fzkeprc.g_xml := i.i_xml;
      g_item_table.extend;
      itn := g_item_table.count;
      g_item_table(itn).po_code           := g_header_rec.po_code;
      g_item_table(itn).po_change_seq_num := g_header_rec.po_change_seq_num;
      g_item_table(itn).po_itemnum        := fzkeprc.f_get_val(g_Item_lineNumber,true);
      g_item_table(itn).po_comm_code      := fzkeprc.f_get_val(g_Item_Classification);
      g_item_table(itn).po_uom            := fzkeprc.f_get_val(g_Item_UnitOfMeasure);
-- TC MOD 20130422 START ------------------------------------------------------
--      g_item_table(itn).po_comm_desc      := substr(fzkeprc.f_get_val(g_Item_Description),1,50);
      g_item_table(itn).po_comm_desc      := substrb(fzkeprc.f_get_val(g_Item_Description),1,50);
-- TC MOD 20130422 END   ------------------------------------------------------
-- TC MOD 20140428 START ------------------------------------------------------
      IF g_item_table(itn).po_comm_desc IS NULL
      THEN 
         g_item_table(itn).po_comm_desc := 'Description not available'; 
      END IF; 
-- TC MOD 20140428 END   ------------------------------------------------------           
      g_item_table(itn).po_unit_price     := fzkeprc.f_get_val(g_Item_UnitPrice,true);
      g_item_table(itn).po_quantity       := fzkeprc.f_get_val(g_Item_quantity,true);
      g_item_table(itn).po_disc_amt       := null;
      g_item_table(itn).po_addl_amt       := null;
      g_item_table(itn).po_tax_amt        := fzkeprc.f_get_val(g_Item_Tax,true);
      g_item_table(itn).po_tgrp_code      := null;
      g_item_table(itn).po_ship_code      := fzkeprc.f_get_val(g_Item_ShipTo);
      g_item_table(itn).po_deliv_date     := fzkeprc.f_get_val(g_Item_DeliveryDate);
      g_item_table(itn).po_print_text     := fzkeprc.f_get_val(g_Item_Comments);
      g_item_table(itn).po_noprint_text   := null;
      
--------------------------------------------------------------------------------
--    Redefine FPRPODT_COMM_DESC and FPRPODT_UOMS_CODE
      if g_item_table(itn).po_comm_code is not null then
         begin                                    -- if Yes then use input value
            select decode(fzkeprc.g_eprf_rec.fzreprf_comm_desc, 'Y',
                          nvl(g_item_table(itn).po_comm_desc, ftvcomm_desc),
                          ftvcomm_desc)           -- else use FTVCOMM_DESC
              into g_item_table(itn).po_comm_desc
              from ftvcomm
             where ftvcomm_code = g_item_table(itn).po_comm_code
               and sysdate between ftvcomm_eff_date
                           and nvl(ftvcomm_term_date,sysdate+1);
         exception when no_data_found then
            g_item_table(itn).po_comm_code := null;      -- Set COMM_CODE = NULL
         end;
      end if;
--
--    Check if UOMS_CODE exists in FTVUOMS
      g_item_table(itn).po_uom:=substr(g_item_table(itn).po_uom,1,3);
      begin
         select ftvuoms_code
           into g_item_table(itn).po_uom                          -- Exists! OK!
           from ftvuoms
          where ftvuoms_code = g_item_table(itn).po_uom
            and sysdate between ftvuoms_eff_date
                        and nvl(ftvuoms_term_date,sysdate+1);
      exception when no_data_found then
         g_item_table(itn).po_uom := 'EA';           -- Set to EA if not exists!
      end;
--
--    Convert Item Delivery Date
      begin g_item_table(itn).po_deliv_date:=fzkeprc.xml2date(g_item_table(itn).po_deliv_date);
      exception when others then l_msg := 'Wrong Item Delivery Date format'; return; end;
--
      if g_item_table(itn).po_deliv_date is null or
         to_date(g_item_table(itn).po_deliv_date,fzkeprc.g_date_frmt)<=
         to_date(g_header_rec.po_order_date,fzkeprc.g_date_frmt)
      then
         g_item_table(itn).po_deliv_date:=to_char(to_date(g_header_rec.po_order_date,fzkeprc.g_date_frmt)+1,fzkeprc.g_date_frmt);
      end if;
--
--    Get max(po_itemnum)
      l_max_itemno := greatest(l_max_itemno, g_item_table(itn).po_itemnum);
--
--------------------------------------------------------------------------------
--    Get Accounting (FOAPAL) lines
      for d in(select value(distr) d_xml from table(XMLSequence
                     (extract(i.i_xml, l_distr_path))) distr)
      loop
         fzkeprc.g_xml := d.d_xml;
         g_acct_table.extend;
         dtn := g_acct_table.count;
         atn := atn + 1;
         g_acct_table(dtn).po_code           := g_header_rec.po_code;
         g_acct_table(dtn).po_change_seq_num := g_header_rec.po_change_seq_num;
         g_acct_table(dtn).po_seqitem        := g_item_table(itn).po_itemnum;
         g_acct_table(dtn).po_seqnum         := atn;
         g_acct_table(dtn).po_acctg_amt      := fzkeprc.f_get_val(g_Accounting_Charge);
         g_acct_table(dtn).po_disc_amt       := null;
         g_acct_table(dtn).po_tax_amt        := null;
         g_acct_table(dtn).po_addl_amt       := null;

-- TC MOD 20130425 START ------------------------------------------------------          
--       g_acct_table(dtn).po_nsf_override   := 'Y';
         g_acct_table(dtn).po_nsf_override   := 'N';                
-- TC MOD 20130425 END   ------------------------------------------------------
-- TC MOD 20140812 START ------------------------------------------------------          
         IF g_po_type = 'release'
         THEN
            g_acct_table(dtn).po_nsf_override   := 'Y';
         END IF;
-- TC MOD 20140812 END   ------------------------------------------------------              
         g_acct_table(dtn).po_percent_dist   := null;
--
--       Get Accounting (FOAPAL) values
         g_acct_table(dtn).po_coas := foapal_val('COAS');
         g_acct_table(dtn).po_fund := foapal_val('FUND');
         g_acct_table(dtn).po_orgn := foapal_val('ORGN');
         g_acct_table(dtn).po_acct := foapal_val('ACCT');
         g_acct_table(dtn).po_prog := foapal_val('PROG');
         g_acct_table(dtn).po_actv := foapal_val('ACTV');
         g_acct_table(dtn).po_locn := foapal_val('LOCN');
-- TC MOD 20121212 START ------------------------------------------------------ 
         g_acct_table(dtn).po_acci := foapal_val('INDEX');
         ftvacci_count := 0;
         select count(*)
           into ftvacci_count
           from ftvacci
          where ftvacci_coas_code = '1' and
                ftvacci_acci_code = g_acct_table(dtn).po_acci and
                ftvacci_eff_date <= sysdate and
                ftvacci_nchg_date >= sysdate;
--
         if ftvacci_count = 1 then
            select * 
              into ftvacci_rec
              from ftvacci
             where ftvacci_coas_code = '1' and
                ftvacci_acci_code = g_acct_table(dtn).po_acci and
                   ftvacci_eff_date <= sysdate and
                   ftvacci_nchg_date >= sysdate; 
--
            g_acct_table(dtn).po_coas := ftvacci_rec.ftvacci_coas_code;
            if ftvacci_rec.ftvacci_fund_code is not null then
               g_acct_table(dtn).po_fund := ftvacci_rec.ftvacci_fund_code;
            end if;
            if ftvacci_rec.ftvacci_orgn_code is not null then
                g_acct_table(dtn).po_orgn := ftvacci_rec.ftvacci_orgn_code;
            end if;
            if ftvacci_rec.ftvacci_prog_code is not null then
               g_acct_table(dtn).po_prog := ftvacci_rec.ftvacci_prog_code;
            end if;
            if ftvacci_rec.ftvacci_actv_code is not null then
               g_acct_table(dtn).po_actv := ftvacci_rec.ftvacci_actv_code;
            end if;
            if ftvacci_rec.ftvacci_locn_code is not null then
               g_acct_table(dtn).po_locn := ftvacci_rec.ftvacci_locn_code;
            end if;
--                   
         end if;                            
-- TC MOD 20121212 END   ------------------------------------------------------ 
      end loop;
   end loop;
--
--------------------------------------------------------------------------------
-- Create shipping item line
   if g_shipping is not null and
      fzkeprc.g_eprf_rec.fzreprf_po_ship_acct is not null then
      begin
         shp := to_number(g_shipping);
         if shp > 0 then
            shp := to_number(l_max_itemno)+1;
-- Copy first item line
            g_item_table.extend;
            itn := g_item_table.count;
            g_item_table(itn) := g_item_table(1);
-- Reset item line values
            g_item_table(itn).po_itemnum    := shp;
-- TC MOD 20121212 START ------------------------------------------------------ 
--            g_item_table(itn).po_uom        :='EA';            
            g_item_table(itn).po_uom        :='EA';
-- TC MOD 20121212 END   ------------------------------------------------------ 
            g_item_table(itn).po_comm_code  := null;
            g_item_table(itn).po_comm_desc  :='Shipping';
            g_item_table(itn).po_unit_price := g_shipping;
            g_item_table(itn).po_quantity   := 1;
            g_item_table(itn).po_print_text := null;
-- Copy first accounting line
            g_acct_table.extend;
            dtn := g_acct_table.count;
            g_acct_table(dtn) := g_acct_table(1);
-- Reset accounting line values
            g_acct_table(dtn).po_seqitem   := g_item_table(itn).po_itemnum;
            g_acct_table(dtn).po_acctg_amt := g_shipping;
            g_acct_table(dtn).po_acct      := fzkeprc.g_eprf_rec.fzreprf_po_ship_acct;
         end if;
      exception when others then null;
      end;
   end if;
--
--------------------------------------------------------------------------------
-- Make Document Level Accounting
   if fzkeprc.g_eprf_rec.fzreprf_doc_lvl_acctg = 'S' then
      num := 0;
      for a in 1 .. g_acct_table.count loop
         val := g_acct_table(a).po_coas||'-'||g_acct_table(a).po_acci||'-'||
                g_acct_table(a).po_fund||'-'||g_acct_table(a).po_orgn||'-'||
                g_acct_table(a).po_acct||'-'||g_acct_table(a).po_prog||'-'||
                g_acct_table(a).po_actv||'-'||g_acct_table(a).po_locn;
         if not FOAPAL.exists(val) then
            num := num+1;
            acnt(num).coas := g_acct_table(a).po_coas;
            acnt(num).acci := g_acct_table(a).po_acci;
            acnt(num).fund := g_acct_table(a).po_fund;
            acnt(num).orgn := g_acct_table(a).po_orgn;
            acnt(num).acct := g_acct_table(a).po_acct;
            acnt(num).prog := g_acct_table(a).po_prog;
            acnt(num).actv := g_acct_table(a).po_actv;
            acnt(num).locn := g_acct_table(a).po_locn;
            acnt(num).amt  := g_acct_table(a).po_acctg_amt;
            FOAPAL(val)    := num;
         else
            acnt(FOAPAL(val)).amt := acnt(FOAPAL(val)).amt + g_acct_table(a).po_acctg_amt;
         end if;
      end loop;
      g_acct_table.delete;
      for a in 1 .. acnt.count loop
         g_acct_table.extend;
         g_acct_table(a).po_code           := g_header_rec.po_code;
         g_acct_table(a).po_change_seq_num := g_header_rec.po_change_seq_num;
         g_acct_table(a).po_seqitem        := 0;
         g_acct_table(a).po_seqnum         := a;
         g_acct_table(a).po_disc_amt       := null;
         g_acct_table(a).po_tax_amt        := null;
         g_acct_table(a).po_addl_amt       := null;
-- TC MOD 20130425 START ------------------------------------------------------          
--         g_acct_table(a).po_nsf_override   := 'Y';
         g_acct_table(a).po_nsf_override   := 'N';
-- TC MOD 20130425 END   ------------------------------------------------------
-- TC MOD 20140812 START ------------------------------------------------------          
         IF g_po_type = 'release'
         THEN
            g_acct_table(a).po_nsf_override   := 'Y';
         END IF;
-- TC MOD 20140812 END   ------------------------------------------------------                            
         g_acct_table(a).po_percent_dist   := null;
         g_acct_table(a).po_acctg_amt      := acnt(a).amt;
         g_acct_table(a).po_coas := acnt(a).coas;
         g_acct_table(a).po_acci := acnt(a).acci;
         g_acct_table(a).po_fund := acnt(a).fund;
         g_acct_table(a).po_orgn := acnt(a).orgn;
         g_acct_table(a).po_acct := acnt(a).acct;
         g_acct_table(a).po_prog := acnt(a).prog;
         g_acct_table(a).po_actv := acnt(a).actv;
         g_acct_table(a).po_locn := acnt(a).locn;
      end loop;
   end if;
--
end p_get_po_items;
--------------------------------------------------------------------------------
begin -- p_get_po_data
--
   fzkeprc.p_set_prc_name('p_get_po_data');
--
   g_item_table := fpkpurr.po_itemsentry_tabtype();
   g_acct_table := fpkpurr.po_acctgentry_tabtype();
--
-- Get PO Header Data
   p_get_po_header;
   if l_msg is not null then p_msg := l_msg; return; end if;        -- Return!!!
--
-- Validate Header Data
   p_validate_header;
   if l_msg is not null then p_msg := l_msg; return; end if;        -- Return!!!
--
-- Set DOC_CODE and DOC_REF_CODE
   if fzkeprc.g_orig like 'EPROC%' then
      if g_po_type = 'release' then
         g_header_rec.po_code := substr(g_doc_ref_code,1,10)||'-'||
                                 substr(g_order_ID,1,9);
         g_doc_ref_code := g_header_rec.po_code;
      end if;
   else
--
      if g_po_type in('regular','release') and fzkeprc.g_eprf_rec.fzreprf_po_docno = 'N' or
         g_po_type in('blanket') and fzkeprc.g_eprf_rec.fzreprf_po_blanket_docno = 'N'
      then
         g_header_rec.po_doc_ref := g_order_ID;
         g_header_rec.po_code := null;
      else
         g_header_rec.po_doc_ref := g_doc_req_id;
         if g_po_type in('regular','release') and fzkeprc.g_eprf_rec.fzreprf_po_docno = 'Y' or
            g_po_type in('blanket') and fzkeprc.g_eprf_rec.fzreprf_po_blanket_docno = 'Y'
         then
            g_header_rec.po_code := substr(g_orgn,1,2)||
                                    substr(lpad(g_order_ID,10,'0'),-6);
         elsif g_po_type = 'blanket' then
            g_header_rec.po_code := fzkeprc.g_eprf_rec.fzreprf_po_blanket_docno||
                                    substr(lpad(g_order_ID,10,'0'),-6);
         else
            g_header_rec.po_code := fzkeprc.g_eprf_rec.fzreprf_po_docno||
                                    substr(lpad(g_order_ID,10,'0'),-6);
         end if;
      end if;
--
      if g_po_type = 'release' then
         g_header_rec.po_doc_ref := substr(g_doc_ref_code,1,10)||'-'||
                                    substr(g_order_ID,1,9);
      end if;
--
      g_doc_ref_code := g_header_rec.po_doc_ref;
--
   end if;
--
dbms_output.put_line('    g_po_type = '||g_po_type);
dbms_output.put_line('eprf_po_docno = '||fzkeprc.g_eprf_rec.fzreprf_po_docno);
dbms_output.put_line('      orderID = '||g_order_ID);
dbms_output.put_line('      po_code = '||g_header_rec.po_code);
dbms_output.put_line('   po_doc_ref = '||g_header_rec.po_doc_ref);
--
   p_get_po_items;
   if l_msg is not null then p_msg := l_msg; return; end if;
--
dbms_output.put_line('       Items = '||g_item_table.count);
--
exception
   when others then
      if SQLCODE=-20000 then
         p_msg := substr(sqlerrm, 12);
      else
         p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
      end if;
end p_get_po_data;
--
--==============================================================================
--
function f_jv_docno(p_pohd_code fpbpohd.fpbpohd_code%type) return gurfeed.gurfeed_doc_code%type is
   l_doc_code gurfeed.gurfeed_doc_code%type;
begin
   if fzkeprc.g_eprf_rec.fzreprf_jv_docno = 'N' then
--    Auto-generate
      fokutil.p_gen_doc_code(doc_type   => FB_JV_HEADER.M_JV_DOC_TYPE,
                             doc_code   => l_doc_code,
                             doc_length => 8,
                             doc_prefix => FB_JV_HEADER.M_JV_SEQ_TYPE);
   elsif fzkeprc.g_eprf_rec.fzreprf_jv_docno = 'Y' then
--    Use requester ORGN
      l_doc_code := substr(g_orgn,1,2)||substr(p_pohd_code,-6);
   else
--    Use FZREPRF_JV_DOCNO
      l_doc_code := fzkeprc.g_eprf_rec.fzreprf_jv_docno||substr(p_pohd_code,-6);
   end if;
   return l_doc_code;
end;
--
--==============================================================================
--
procedure p_create_pcard_jv(p_pohd_code fpbpohd.fpbpohd_code%type) is
   l_sysdate  date := sysdate;
   l_timestm  gurfeed.gurfeed_system_time_stamp%type := to_char(l_sysdate,'YYYYMMDDHH24MISS');
   l_sys_id   gurfeed.gurfeed_system_id%type := fzkeprc.g_eprf_rec.fzreprf_jv_system_id;
   l_rucl     gurfeed.gurfeed_rucl_code%type := fzkeprc.g_eprf_rec.fzreprf_pcard_jv_rucl;
begin
--
-- Create GURFEED records
   insert into gurfeed
         (gurfeed_system_id,         -- FZREPRF_JV_SYSTEM_ID
          gurfeed_system_time_stamp, -- to_char(sysdate,'YYYYMMDDHH24MISS')
          gurfeed_doc_code,          -- f_jv_docno(p_pohd_code)
          gurfeed_rec_type,          -- 1 for header, 2 for line
          gurfeed_seq_num,           -- 0 for header, 1-999 for lines
          gurfeed_activity_date,     -- trunc(sysdate)
          gurfeed_user_id,           -- 'FIMSMGR'
          gurfeed_rucl_code,         -- null / FZREPRF_PCARD_JV_RUCL
          gurfeed_doc_ref_num,       -- null / FPBPOHD_CODE
          gurfeed_trans_date,        -- sysdate
          gurfeed_trans_amt,         -- sum(fprpoda_amt) / fprpoda_amt
          gurfeed_trans_desc,        -- Liquidate encumbrance
          gurfeed_dr_cr_ind,         -- null / '+'
          gurfeed_coas_code,         -- null / fprpoda_coas_code
          gurfeed_fund_code,         -- null / fprpoda_fund_code
          gurfeed_orgn_code,         -- null / fprpoda_orgn_code
          gurfeed_acct_code,         -- null / fprpoda_acct_code
          gurfeed_prog_code,         -- null / fprpoda_prog_code
          gurfeed_actv_code,         -- null / fprpoda_actv_code
          gurfeed_locn_code,         -- null / fprpoda_locn_code
          gurfeed_encd_num,          -- fprpoda_pohd_code
          gurfeed_encd_item_num,     -- fprpoda_item
          gurfeed_encd_seq_num,      -- fprpoda_seq_num
          gurfeed_encd_action_ind,   -- 'T'
          gurfeed_encb_type)         -- 'P'
--
-- Header record
   select l_sys_id              gurfeed_system_id,
          l_timestm             gurfeed_system_time_stamp,
          g_jv_docno            gurfeed_doc_code,
          1                     gurfeed_rec_type,
          0                     gurfeed_seq_num,
          l_sysdate             gurfeed_activity_date,
          'FIMSMGR'             gurfeed_user_id,
          null                  gurfeed_rucl_code,
          null                  gurfeed_doc_ref_num,
          l_sysdate             gurfeed_trans_date,
          trans_amt             gurfeed_trans_amt,
          g_trans_desc          gurfeed_trans_desc,
          null                  gurfeed_dr_cr_ind,
          null                  gurfeed_coas_code,
          null                  gurfeed_fund_code,
          null                  gurfeed_orgn_code,
          null                  gurfeed_acct_code,
          null                  gurfeed_prog_code,
          null                  gurfeed_actv_code,
          null                  gurfeed_locn_code,
          null                  gurfeed_encd_num,
          null                  gurfeed_encd_item_num,
          null                  gurfeed_encd_seq_num,
          null                  gurfeed_encd_action_ind,
          null                  gurfeed_encb_type
     from fpbpohd,
         (select sum(fprpoda_amt) trans_amt
            from fprpoda
           where fprpoda_pohd_code = p_pohd_code
             and fprpoda_change_seq_num is null)
    where fpbpohd_code = p_pohd_code
      and fpbpohd_change_seq_num is null
--
    union all
--
--   Line records
   select l_sys_id              gurfeed_system_id,
          l_timestm             gurfeed_system_time_stamp,
          g_jv_docno            gurfeed_doc_code,
          2                     gurfeed_rec_type,
          rownum                gurfeed_seq_num,
          l_sysdate             gurfeed_activity_date,
          'FIMSMGR'             gurfeed_user_id,
          l_rucl                gurfeed_rucl_code,
          fpbpohd_code          gurfeed_doc_ref_num,
          l_sysdate             gurfeed_trans_date,
          fprpoda_amt           gurfeed_trans_amt,
          g_trans_desc          gurfeed_trans_desc,
          '+'                   gurfeed_dr_cr_ind,
          fprpoda_coas_code     gurfeed_coas_code,
          fprpoda_fund_code     gurfeed_fund_code,
          fprpoda_orgn_code     gurfeed_orgn_code,
          fprpoda_acct_code     gurfeed_acct_code,
          fprpoda_prog_code     gurfeed_prog_code,
          fprpoda_actv_code     gurfeed_actv_code,
          fprpoda_locn_code     gurfeed_locn_code,
          fprpoda_pohd_code     gurfeed_encd_num,
          fprpoda_item          gurfeed_encd_item_num,
          fprpoda_seq_num       gurfeed_encd_seq_num,
          'T'                   gurfeed_encd_action_ind,
          'P'                   gurfeed_encb_type
     from fpbpohd, fprpoda
    where fpbpohd_code = p_pohd_code
      and fprpoda_pohd_code = fpbpohd_code
      and fpbpohd_change_seq_num is null
      and fprpoda_change_seq_num is null;
--
dbms_output.put_line('Rows inserted into GURFEED = ' ||SQL%ROWCOUNT);
--
end p_create_pcard_jv;
--
--==============================================================================
--
procedure p_create_release_jv(p_pohd_code fpbpohd.fpbpohd_code%type) is
   l_sysdate  date := sysdate;
   l_timestm  gurfeed.gurfeed_system_time_stamp%type := to_char(l_sysdate,'YYYYMMDDHH24MISS');
   l_sys_id   gurfeed.gurfeed_system_id%type := fzkeprc.g_eprf_rec.fzreprf_jv_system_id;
   l_rucl     gurfeed.gurfeed_rucl_code%type := fzkeprc.g_eprf_rec.fzreprf_release_jv_rucl;
begin
--
-- Create GURFEED records
   insert into gurfeed
         (gurfeed_system_id,         -- FZREPRF_JV_SYSTEM_ID
          gurfeed_system_time_stamp, -- to_char(sysdate,'YYYYMMDDHH24MISS')
          gurfeed_doc_code,          --
          gurfeed_rec_type,          -- 1 for header, 2 for line
          gurfeed_seq_num,           -- 0 for header, 1-999 for lines
          gurfeed_activity_date,     -- trunc(sysdate)
          gurfeed_user_id,           -- 'FIMSMGR'
          gurfeed_rucl_code,         -- null / FZREPRF_PCARD_JV_RUCL
          gurfeed_doc_ref_num,       -- fpbpohd_code
          gurfeed_trans_date,        -- greatest(blanket_trans_date, sysdate)
          gurfeed_trans_amt,         -- sum(fprpoda_amt) / fprpoda_amt
          gurfeed_trans_desc,        -- Liquidate encumbrance
          gurfeed_dr_cr_ind,         -- null / '+'
          gurfeed_coas_code,         -- null / fprpoda_coas_code
          gurfeed_fund_code,         -- null / fprpoda_fund_code
          gurfeed_orgn_code,         -- null / fprpoda_orgn_code
          gurfeed_acct_code,         -- null / fprpoda_acct_code
          gurfeed_prog_code,         -- null / fprpoda_prog_code
          gurfeed_actv_code,         -- null / fprpoda_actv_code
          gurfeed_locn_code,         -- null / fprpoda_locn_code
          gurfeed_encd_num,          -- fprpoda_pohd_code
          gurfeed_encd_item_num,     -- fprpoda_item
          gurfeed_encd_seq_num,      -- fprpoda_seq_num
          gurfeed_encd_action_ind,   -- 'P' - partial
          gurfeed_encb_type)         -- 'P'
--
-- Header record
   select l_sys_id              gurfeed_system_id,
          l_timestm             gurfeed_system_time_stamp,
          g_jv_docno            gurfeed_doc_code,
          1                     gurfeed_rec_type,
          0                     gurfeed_seq_num,
          l_sysdate             gurfeed_activity_date,
          'FIMSMGR'             gurfeed_user_id,
          null                  gurfeed_rucl_code,
          null                  gurfeed_doc_ref_num,
          g_jv_trans_dt         gurfeed_trans_date,
          g_total_amt           gurfeed_trans_amt,
          g_trans_desc          gurfeed_trans_desc,
          null                  gurfeed_dr_cr_ind,
          null                  gurfeed_coas_code,
          null                  gurfeed_fund_code,
          null                  gurfeed_orgn_code,
          null                  gurfeed_acct_code,
          null                  gurfeed_prog_code,
          null                  gurfeed_actv_code,
          null                  gurfeed_locn_code,
          null                  gurfeed_encd_num,
          null                  gurfeed_encd_item_num,
          null                  gurfeed_encd_seq_num,
          null                  gurfeed_encd_action_ind,
          null                  gurfeed_encb_type
     from fpbpohd
    where fpbpohd_code = p_pohd_code
      and fpbpohd_change_seq_num is null
--
    union all
--
--   Line records
   select l_sys_id              gurfeed_system_id,
          l_timestm             gurfeed_system_time_stamp,
          g_jv_docno            gurfeed_doc_code,
          2                     gurfeed_rec_type,
          rownum                gurfeed_seq_num,
          l_sysdate             gurfeed_activity_date,
          'FIMSMGR'             gurfeed_user_id,
          l_rucl                gurfeed_rucl_code,
          fpbpohd_code          gurfeed_doc_ref_num,
          g_jv_trans_dt         gurfeed_trans_date,
--        Distribute total release PO amt across accounting based on fprpoda_appr_amt_pct
          case count(*)over() -
               row_number()over(order by fprpoda_amt, fprpoda_seq_num)
          when 0 then -- calculate and adjust amt of the first record with max(fprpoda_amt)
             round(fprpoda_appr_amt_pct*g_total_amt/100, 2) +
             g_total_amt - sum(round(fprpoda_appr_amt_pct*g_total_amt/100, 2))over()
          else        -- just calculate amt
             round(fprpoda_appr_amt_pct*g_total_amt/100, 2)
          end                   gurfeed_trans_amt,
--
          g_trans_desc          gurfeed_trans_desc,
          '+'                   gurfeed_dr_cr_ind,
          fprpoda_coas_code     gurfeed_coas_code,
          fprpoda_fund_code     gurfeed_fund_code,
          fprpoda_orgn_code     gurfeed_orgn_code,
          fprpoda_acct_code     gurfeed_acct_code,
          fprpoda_prog_code     gurfeed_prog_code,
          fprpoda_actv_code     gurfeed_actv_code,
          fprpoda_locn_code     gurfeed_locn_code,
          fprpoda_pohd_code     gurfeed_encd_num,
          fprpoda_item          gurfeed_encd_item_num,
          fprpoda_seq_num       gurfeed_encd_seq_num,
          'P'                   gurfeed_encd_action_ind,
          'P'                   gurfeed_encb_type
     from fpbpohd, fprpoda
    where fpbpohd_code = p_pohd_code
      and fprpoda_pohd_code = fpbpohd_code
      and fpbpohd_change_seq_num is null
      and fprpoda_change_seq_num is null;
--
dbms_output.put_line('Rows inserted into GURFEED = ' ||SQL%ROWCOUNT);
--
end p_create_release_jv;
--
--==============================================================================
--
procedure p_create_jv(p_pohd_code fpbpohd.fpbpohd_code%type) is
   l_doc_num  fgbjvch.fgbjvch_doc_num%type := g_jv_docno;
   l_rowid    gb_common.internal_record_id_type;
   l_jv_user  fgbjvch.fgbjvch_user_id%type;
   l_status   fgbjvcd.fgbjvcd_status_ind%type;
   l_msg      gb_common_strings.err_type;
-- l_action   fgbjvcd.fgbjvcd_encb_action_ind%type;
   l_abal_sev varchar2(1);
--
begin
   g_trans_desc := substr(g_fzrcxml.fzrcxml_doc_code||'-'||g_order_ID||' VEND '||g_vend_id, 1, 35);
   l_jv_user    := upper(nvl(fzkeprc.g_eprf_rec.fzreprf_jv_userid, fzkeprc.g_user));
--
-- Create JV header
   begin
      fb_jv_header.p_create(
      p_doc_num_in_out    => l_doc_num,      -- IN OUT fgbjvch.fgbjvch_doc_num%TYPE,
--    p_submission_number => 0,              -- fgbjvch.fgbjvch_submission_number%TYPE DEFAULT 0,
      p_user_id           => l_jv_user,      -- fgbjvch.fgbjvch_user_id%TYPE DEFAULT gb_common.f_sct_user,
      p_trans_date        => g_jv_trans_dt,  -- fgbjvch.fgbjvch_trans_date%TYPE,
      p_doc_description   => g_trans_desc,   -- fgbjvch.fgbjvch_doc_description%TYPE DEFAULT NULL,
      p_doc_amt           => g_total_amt,    -- fgbjvch.fgbjvch_doc_amt%TYPE DEFAULT NULL,
      p_edit_defer_ind    =>'N',             -- fgbjvch.fgbjvch_edit_defer_ind%TYPE,
      p_approval_ind      =>'N',             -- fgbjvch.fgbjvch_approval_ind%TYPE DEFAULT NULL,
--    p_auto_jrnl_id      => NULL,           -- fgbjvch.fgbjvch_auto_jrnl_id%TYPE DEFAULT NULL,
--    p_reversal_ind      => NULL,           -- fgbjvch.fgbjvch_reversal_ind%TYPE DEFAULT NULL,
--    p_obud_code         => NULL,           -- fgbjvch.fgbjvch_obud_code%TYPE DEFAULT NULL,
--    p_obph_code         => NULL,           -- fgbjvch.fgbjvch_obph_code%TYPE DEFAULT NULL,
--    p_budg_dur_code     => NULL,           -- fgbjvch.fgbjvch_budg_dur_code%TYPE DEFAULT NULL,
--    p_status_ind        =>'I',             -- fgbjvch.fgbjvch_status_ind%TYPE DEFAULT 'I',
--    p_distrib_amt       => NULL,           -- fgbjvch.fgbjvch_distrib_amt%TYPE DEFAULT NULL,
--    p_nsf_on_off_ind    => NULL,           -- fgbjvch.fgbjvch_nsf_on_off_ind%TYPE DEFAULT NULL,
      p_data_origin       => fzkeprc.g_orig, -- fgbjvch.fgbjvch_data_origin%TYPE DEFAULT NULL,
      p_rowid_out         => l_rowid);       -- OUT gb_common.internal_record_id_type
   exception when others then
      fzkeprc.p_send_error_email('Could not create JV header for PO (Order ID) '||p_pohd_code||' ('||g_order_ID||')'||chr(10)||SQLERRM);
      return;
   end;
--
   if l_doc_num <> g_jv_docno then
      fzkeprc.p_send_error_email('JV API has changed FGBJVCH_DOC_NUM from '||g_jv_docno||' to '||l_doc_num);
   end if;
--
-- Create JV detail
   for r in(select rownum  rnum,
                   fprpoda_coas_code coas, fprpoda_acci_code acci,
                   fprpoda_fund_code fund, fprpoda_orgn_code orgn,
                   fprpoda_acct_code acct, fprpoda_prog_code prog,
                   fprpoda_actv_code actv, fprpoda_locn_code locn,
                   fprpoda_item      item, fprpoda_seq_num   seq,
                   decode(g_jv_type, 'PCard', fprpoda_amt,
--                 Distribute total release PO amt across blanket accounting based on appr_amt_pct
                          case count(*)over() -
                               row_number()over(order by fprpoda_amt, fprpoda_seq_num)
                          when 0 then -- calculate and adjust amt of the first record with max(fprpoda_amt)
                             round(fprpoda_appr_amt_pct*g_total_amt/100, 2) +
                             g_total_amt - sum(round(fprpoda_appr_amt_pct*g_total_amt/100, 2))over()
                          else        -- just calculate amt
                             round(fprpoda_appr_amt_pct*g_total_amt/100, 2)
                          end)       trans_amt
              from fpbpohd, fprpoda
             where fpbpohd_code = p_pohd_code
               and fprpoda_pohd_code = fpbpohd_code
               and fpbpohd_change_seq_num is null
               and fprpoda_change_seq_num is null
             order by fprpoda_item, fprpoda_seq_num)
   loop
      fb_jv_detail.p_create(
         p_doc_num           => l_doc_num,    -- fgbjvcd.fgbjvcd_doc_num%TYPE,
--       p_submission_number => 0,            -- fgbjvcd.fgbjvcd_submission_number%TYPE DEFAULT 0,
         p_seq_num           => r.rnum,       -- fgbjvcd.fgbjvcd_seq_num%TYPE,
         p_user_id           => l_jv_user,    -- fgbjvcd.fgbjvcd_user_id%TYPE DEFAULT gb_common.f_sct_user,
         p_rucl_code         => g_jv_rucl,    -- fgbjvcd.fgbjvcd_rucl_code%TYPE,
         p_trans_amt         => r.trans_amt,  -- fgbjvcd.fgbjvcd_trans_amt%TYPE DEFAULT NULL,
         p_trans_desc        => g_trans_desc, -- fgbjvcd.fgbjvcd_trans_desc%TYPE DEFAULT NULL,
         p_dr_cr_ind         => '+',          -- fgbjvcd.fgbjvcd_dr_cr_ind%TYPE DEFAULT NULL,
         p_acci_code         => r.acci,       -- fgbjvcd.fgbjvcd_acci_code%TYPE DEFAULT NULL,
         p_coas_code         => r.coas,       -- fgbjvcd.fgbjvcd_coas_code%TYPE DEFAULT NULL,
         p_fund_code         => r.fund,       -- fgbjvcd.fgbjvcd_fund_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_orgn_code         => r.orgn,       -- fgbjvcd.fgbjvcd_orgn_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_acct_code         => r.acct,       -- fgbjvcd.fgbjvcd_acct_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_prog_code         => r.prog,       -- fgbjvcd.fgbjvcd_prog_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_actv_code         => r.actv,       -- fgbjvcd.fgbjvcd_actv_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_locn_code         => r.locn,       -- fgbjvcd.fgbjvcd_locn_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_doc_ref_num       => g_order_ID,   -- fgbjvcd.fgbjvcd_doc_ref_num%TYPE DEFAULT NULL,
         p_encb_num          => p_pohd_code,  -- fgbjvcd.fgbjvcd_encb_num%TYPE DEFAULT NULL,
         p_encd_item_num     => r.item,       -- fgbjvcd.fgbjvcd_encd_item_num%TYPE DEFAULT NULL,
         p_encd_seq_num      => r.seq,        -- fgbjvcd.fgbjvcd_encd_seq_num%TYPE DEFAULT NULL,
         p_encb_type         => 'P',          -- fgbjvcd.fgbjvcd_encb_type%TYPE DEFAULT NULL,
         p_encb_action_ind   => g_jv_act,     -- fgbjvcd.fgbjvcd_encb_action_ind%TYPE DEFAULT NULL,
         p_abal_override     => 'N',          -- fgbjvcd.fgbjvcd_abal_override%TYPE DEFAULT NULL,
--       p_fsyr_code         => NULL,         -- fgbjvcd.fgbjvcd_fsyr_code%TYPE DEFAULT NULL,
--       p_posting_period    => NULL,         -- fgbjvcd.fgbjvcd_posting_period%TYPE DEFAULT NULL,
--       p_bank_code         => NULL,         -- fgbjvcd.fgbjvcd_bank_code%TYPE DEFAULT dml_common.f_unspecified_string,
--       p_vendor_pidm       => NULL,         -- fgbjvcd.fgbjvcd_vendor_pidm%TYPE DEFAULT NULL,
--       p_bud_dispn         => NULL,         -- fgbjvcd.fgbjvcd_bud_dispn%TYPE DEFAULT NULL,
--       p_bud_id            => NULL,         -- fgbjvcd.fgbjvcd_bud_id%TYPE DEFAULT NULL,
--       p_cmt_type          => NULL,         -- fgbjvcd.fgbjvcd_cmt_type%TYPE DEFAULT NULL,
--       p_cmt_pct           => NULL,         -- fgbjvcd.fgbjvcd_cmt_pct%TYPE DEFAULT NULL,
--       p_dep_num           => NULL,         -- fgbjvcd.fgbjvcd_dep_num%TYPE DEFAULT NULL,
--       p_prjd_code         => NULL,         -- fgbjvcd.fgbjvcd_prjd_code%TYPE DEFAULT NULL,
--       p_acct_code_cash    => NULL,         -- fgbjvcd.fgbjvcd_acct_code_cash%TYPE DEFAULT NULL,
--       p_dist_pct          => NULL,         -- fgbjvcd.fgbjvcd_dist_pct%TYPE DEFAULT NULL,
--       p_budget_period     => NULL,         -- fgbjvcd.fgbjvcd_budget_period%TYPE DEFAULT NULL,
--       p_accrual_ind       => NULL,         -- fgbjvcd.fgbjvcd_accrual_ind%TYPE DEFAULT NULL,
--       p_appr_ind          => NULL,         -- fgbjvcd.fgbjvcd_appr_ind%TYPE DEFAULT NULL,
--       p_curr_code         => NULL,         -- fgbjvcd.fgbjvcd_curr_code%TYPE DEFAULT NULL,
--       p_converted_amt     => NULL,         -- fgbjvcd.fgbjvcd_converted_amt%TYPE DEFAULT NULL,
--       p_gift_date         => NULL,         -- fgbjvcd.fgbjvcd_gift_date%TYPE DEFAULT NULL,
--       p_emc_units         => NULL,         -- fgbjvcd.fgbjvcd_emc_units%TYPE DEFAULT NULL,
         p_data_origin     => fzkeprc.g_orig, -- fgbjvcd.fgbjvcd_data_origin%TYPE DEFAULT NULL,
         p_status_in_out     => l_status,     -- IN OUT fgbjvcd.fgbjvcd_status_ind%TYPE,
         p_warning_out       => l_msg,        -- OUT gb_common_strings.err_type,
         p_abal_severity_out => l_abal_sev,   -- OUT VARCHAR2,
         p_rowid_out         => l_rowid);     -- OUT gb_common.internal_record_id_type
--
      if l_status <> 'P' then
         fzkeprc.p_send_error_email('JV cannot be posted, FGBJVCD_STATUS_IND = '||l_status||', '||l_msg);
         exit;
      end if;
   end loop;
--
-- Send JV to the posting process
   if l_status = 'P' then
      gb_common.p_set_context('FP_JOURNAL_VOUCHER', 'PROCESS', 'INTERFACE');
      fp_journal_voucher.p_complete(p_doc_num => l_doc_num, p_msg_out => l_msg);
      if l_msg is not null then
--       fzkeprc.p_send_error_email(l_msg);
         null;
      end if;
   end if;
--
exception when others then
   fzkeprc.p_send_error_email('Could not create JV detail for PO (Order ID) '||p_pohd_code||' ('||g_order_ID||')'||chr(10)||SQLERRM);
end p_create_jv;
--
--==============================================================================
--
procedure p_create_jv_schd
  (p_user        varchar2,
   p_pohd_code   fpbpohd.fpbpohd_code%type,
   p_order_ID    fzrcxml.fzrcxml_doc_id%type,
   p_jv_type     varchar2,
   p_jv_docno    fgbjvch.fgbjvch_doc_num%type,
   p_jv_trans_dt varchar2,
   p_jv_tr_desc  fgbjvch.fgbjvch_doc_description%type,
   p_jv_tot_amt  fgbjvch.fgbjvch_doc_amt%type,
   p_jv_rucl     fgbjvcd.fgbjvcd_rucl_code%type,
   p_jv_act      fgbjvcd.fgbjvcd_encb_action_ind%type,
   p_orig        fgbjvch.fgbjvch_data_origin%type,
   p_schedule    ftvsdat.ftvsdat_sdat_code_opt_1%type := null)
as
   l_doc_num  fgbjvch.fgbjvch_doc_num%type := p_jv_docno;
   l_jv_tr_dt date := to_date(p_jv_trans_dt,'YYYYMMDDHH24MISS');
   l_rowid    gb_common.internal_record_id_type;
   l_status   fgbjvcd.fgbjvcd_status_ind%type;
   l_msg      gb_common_strings.err_type;
   l_abal_sev varchar2(1);
   l_jv_type  varchar2(10);
   l_job      ftvsdat.ftvsdat_sdat_code_opt_1%type;
   l_what     varchar2(1000);
   l_when     date;
   l_job_no   number;
--
begin
--
-- Get schedule setting
   if p_schedule is not null and p_jv_type = 'PCard' then
      begin
         select ftvsdat_sdat_code_opt_2,
                sysdate+(to_number(ftvsdat_data)/(24*60)),
               'fzkeprc_po.p_create_jv_schd'||
                    '('''||p_user       ||''','||
                     ''''||p_pohd_code  ||''','||
                     ''''||p_order_ID   ||''','||
                     ''''||p_jv_type    ||''','||
                     ''''||p_jv_docno   ||''','||
                     ''''||p_jv_trans_dt||''','||
                     ''''||p_jv_tr_desc ||''','||
                     ''''||p_jv_tot_amt ||''','||
                     ''''||p_jv_rucl    ||''','||
                     ''''||p_jv_act     ||''','||
                     ''''||p_orig       ||''');'
           into l_job, l_when, l_what
           from ftvsdat
          where ftvsdat_sdat_code_entity = 'FZKEPRC'
            and ftvsdat_sdat_code_attr   = 'SCHEDULE'
            and ftvsdat_sdat_code_opt_1  = p_schedule
            and ftvsdat_status_ind = 'A'
            and ftvsdat_sdat_code_opt_1 is NOT NULL
            and ftvsdat_sdat_code_opt_2 is NOT NULL
            and ftvsdat_eff_date <= sysdate
            and nvl(ftvsdat_term_date, sysdate+1) > sysdate
            and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
-- Schedule Oracle Job
         if l_job = 'DBMS_JOB' then
            dbms_job.submit(l_job_no, l_what, l_when);
dbms_output.put_line(p_schedule||' scheduled job '||l_job_no);
            return;
         end if;
      exception when others then
         fzkeprc.p_send_error_email('Error in P_CREATE_JV_SCHD: Something wrong with FTVSDAT '||p_schedule||' schedule setting');
         return;
      end;
   end if;
--
-- Create JV header
   begin
      fb_jv_header.p_create(
      p_doc_num_in_out    => l_doc_num,      -- IN OUT fgbjvch.fgbjvch_doc_num%TYPE,
--    p_submission_number => 0,              -- fgbjvch.fgbjvch_submission_number%TYPE DEFAULT 0,
      p_user_id           => p_user,         -- fgbjvch.fgbjvch_user_id%TYPE DEFAULT gb_common.f_sct_user,
      p_trans_date        => l_jv_tr_dt,     -- fgbjvch.fgbjvch_trans_date%TYPE,
      p_doc_description   => p_jv_tr_desc,   -- fgbjvch.fgbjvch_doc_description%TYPE DEFAULT NULL,
      p_doc_amt           => p_jv_tot_amt,   -- fgbjvch.fgbjvch_doc_amt%TYPE DEFAULT NULL,
      p_edit_defer_ind    =>'N',             -- fgbjvch.fgbjvch_edit_defer_ind%TYPE,
      p_approval_ind      =>'N',             -- fgbjvch.fgbjvch_approval_ind%TYPE DEFAULT NULL,
--    p_auto_jrnl_id      => NULL,           -- fgbjvch.fgbjvch_auto_jrnl_id%TYPE DEFAULT NULL,
--    p_reversal_ind      => NULL,           -- fgbjvch.fgbjvch_reversal_ind%TYPE DEFAULT NULL,
--    p_obud_code         => NULL,           -- fgbjvch.fgbjvch_obud_code%TYPE DEFAULT NULL,
--    p_obph_code         => NULL,           -- fgbjvch.fgbjvch_obph_code%TYPE DEFAULT NULL,
--    p_budg_dur_code     => NULL,           -- fgbjvch.fgbjvch_budg_dur_code%TYPE DEFAULT NULL,
--    p_status_ind        =>'I',             -- fgbjvch.fgbjvch_status_ind%TYPE DEFAULT 'I',
--    p_distrib_amt       => NULL,           -- fgbjvch.fgbjvch_distrib_amt%TYPE DEFAULT NULL,
--    p_nsf_on_off_ind    => NULL,           -- fgbjvch.fgbjvch_nsf_on_off_ind%TYPE DEFAULT NULL,
      p_data_origin       => p_orig,         -- fgbjvch.fgbjvch_data_origin%TYPE DEFAULT NULL,
      p_rowid_out         => l_rowid);       -- OUT gb_common.internal_record_id_type
   exception when others then
      fzkeprc.p_send_error_email('Could not create JV header for PO (Order ID) '||p_pohd_code||' ('||p_order_ID||')'||chr(10)||SQLERRM);
      return;
   end;
--
   if l_doc_num <> p_jv_docno then
      fzkeprc.p_send_error_email('JV API has changed FGBJVCH_DOC_NUM from '||p_jv_docno||' to '||l_doc_num);
   end if;
--
-- Create JV detail
   for r in(select rownum  rnum,
                   fprpoda_coas_code coas, fprpoda_acci_code acci,
                   fprpoda_fund_code fund, fprpoda_orgn_code orgn,
                   fprpoda_acct_code acct, fprpoda_prog_code prog,
                   fprpoda_actv_code actv, fprpoda_locn_code locn,
                   fprpoda_item      item, fprpoda_seq_num   seq,
                   decode(p_jv_type, 'PCard', fprpoda_amt,     -- fixed 10/23/12
--                 Distribute total release PO amt across blanket accounting based on appr_amt_pct
                          case count(*)over() -
                               row_number()over(order by fprpoda_amt, fprpoda_seq_num)
                          when 0 then -- calculate and adjust amt of the first record with max(fprpoda_amt)
                             round(fprpoda_appr_amt_pct*p_jv_tot_amt/100, 2) +
                             p_jv_tot_amt - sum(round(fprpoda_appr_amt_pct*p_jv_tot_amt/100, 2))over()
                          else        -- just calculate amt
                             round(fprpoda_appr_amt_pct*p_jv_tot_amt/100, 2)
                          end)       trans_amt
              from fpbpohd, fprpoda
             where fpbpohd_code = p_pohd_code
               and fprpoda_pohd_code = fpbpohd_code
               and fpbpohd_change_seq_num is null
               and fprpoda_change_seq_num is null
             order by fprpoda_item, fprpoda_seq_num)
   loop
      fb_jv_detail.p_create(
         p_doc_num           => l_doc_num,    -- fgbjvcd.fgbjvcd_doc_num%TYPE,
--       p_submission_number => 0,            -- fgbjvcd.fgbjvcd_submission_number%TYPE DEFAULT 0,
         p_seq_num           => r.rnum,       -- fgbjvcd.fgbjvcd_seq_num%TYPE,
         p_user_id           => p_user,       -- fgbjvcd.fgbjvcd_user_id%TYPE DEFAULT gb_common.f_sct_user,
         p_rucl_code         => p_jv_rucl,    -- fgbjvcd.fgbjvcd_rucl_code%TYPE,
         p_trans_amt         => r.trans_amt,  -- fgbjvcd.fgbjvcd_trans_amt%TYPE DEFAULT NULL,
         p_trans_desc        => p_jv_tr_desc, -- fgbjvcd.fgbjvcd_trans_desc%TYPE DEFAULT NULL,
         p_dr_cr_ind         => '+',          -- fgbjvcd.fgbjvcd_dr_cr_ind%TYPE DEFAULT NULL,
         p_acci_code         => r.acci,       -- fgbjvcd.fgbjvcd_acci_code%TYPE DEFAULT NULL,
         p_coas_code         => r.coas,       -- fgbjvcd.fgbjvcd_coas_code%TYPE DEFAULT NULL,
         p_fund_code         => r.fund,       -- fgbjvcd.fgbjvcd_fund_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_orgn_code         => r.orgn,       -- fgbjvcd.fgbjvcd_orgn_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_acct_code         => r.acct,       -- fgbjvcd.fgbjvcd_acct_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_prog_code         => r.prog,       -- fgbjvcd.fgbjvcd_prog_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_actv_code         => r.actv,       -- fgbjvcd.fgbjvcd_actv_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_locn_code         => r.locn,       -- fgbjvcd.fgbjvcd_locn_code%TYPE DEFAULT dml_common.f_unspecified_string,
         p_doc_ref_num       => p_order_ID,   -- fgbjvcd.fgbjvcd_doc_ref_num%TYPE DEFAULT NULL,
         p_encb_num          => p_pohd_code,  -- fgbjvcd.fgbjvcd_encb_num%TYPE DEFAULT NULL,
         p_encd_item_num     => r.item,       -- fgbjvcd.fgbjvcd_encd_item_num%TYPE DEFAULT NULL,
         p_encd_seq_num      => r.seq,        -- fgbjvcd.fgbjvcd_encd_seq_num%TYPE DEFAULT NULL,
         p_encb_type         => 'P',          -- fgbjvcd.fgbjvcd_encb_type%TYPE DEFAULT NULL,
         p_encb_action_ind   => p_jv_act,     -- fgbjvcd.fgbjvcd_encb_action_ind%TYPE DEFAULT NULL,
         p_abal_override     => 'N',          -- fgbjvcd.fgbjvcd_abal_override%TYPE DEFAULT NULL,
--       p_fsyr_code         => NULL,         -- fgbjvcd.fgbjvcd_fsyr_code%TYPE DEFAULT NULL,
--       p_posting_period    => NULL,         -- fgbjvcd.fgbjvcd_posting_period%TYPE DEFAULT NULL,
--       p_bank_code         => NULL,         -- fgbjvcd.fgbjvcd_bank_code%TYPE DEFAULT dml_common.f_unspecified_string,
--       p_vendor_pidm       => NULL,         -- fgbjvcd.fgbjvcd_vendor_pidm%TYPE DEFAULT NULL,
--       p_bud_dispn         => NULL,         -- fgbjvcd.fgbjvcd_bud_dispn%TYPE DEFAULT NULL,
--       p_bud_id            => NULL,         -- fgbjvcd.fgbjvcd_bud_id%TYPE DEFAULT NULL,
--       p_cmt_type          => NULL,         -- fgbjvcd.fgbjvcd_cmt_type%TYPE DEFAULT NULL,
--       p_cmt_pct           => NULL,         -- fgbjvcd.fgbjvcd_cmt_pct%TYPE DEFAULT NULL,
--       p_dep_num           => NULL,         -- fgbjvcd.fgbjvcd_dep_num%TYPE DEFAULT NULL,
--       p_prjd_code         => NULL,         -- fgbjvcd.fgbjvcd_prjd_code%TYPE DEFAULT NULL,
--       p_acct_code_cash    => NULL,         -- fgbjvcd.fgbjvcd_acct_code_cash%TYPE DEFAULT NULL,
--       p_dist_pct          => NULL,         -- fgbjvcd.fgbjvcd_dist_pct%TYPE DEFAULT NULL,
--       p_budget_period     => NULL,         -- fgbjvcd.fgbjvcd_budget_period%TYPE DEFAULT NULL,
--       p_accrual_ind       => NULL,         -- fgbjvcd.fgbjvcd_accrual_ind%TYPE DEFAULT NULL,
--       p_appr_ind          => NULL,         -- fgbjvcd.fgbjvcd_appr_ind%TYPE DEFAULT NULL,
--       p_curr_code         => NULL,         -- fgbjvcd.fgbjvcd_curr_code%TYPE DEFAULT NULL,
--       p_converted_amt     => NULL,         -- fgbjvcd.fgbjvcd_converted_amt%TYPE DEFAULT NULL,
--       p_gift_date         => NULL,         -- fgbjvcd.fgbjvcd_gift_date%TYPE DEFAULT NULL,
--       p_emc_units         => NULL,         -- fgbjvcd.fgbjvcd_emc_units%TYPE DEFAULT NULL,
         p_data_origin       => p_orig,       -- fgbjvcd.fgbjvcd_data_origin%TYPE DEFAULT NULL,
         p_status_in_out     => l_status,     -- IN OUT fgbjvcd.fgbjvcd_status_ind%TYPE,
         p_warning_out       => l_msg,        -- OUT gb_common_strings.err_type,
         p_abal_severity_out => l_abal_sev,   -- OUT VARCHAR2,
         p_rowid_out         => l_rowid);     -- OUT gb_common.internal_record_id_type
--
      if l_status <> 'P' then
         fzkeprc.p_send_error_email('JV cannot be posted, FGBJVCD_STATUS_IND = '||l_status||', '||l_msg);
         exit;
      end if;
   end loop;
--
-- Send JV to the posting process
   if l_status = 'P' then
      gb_common.p_set_context('FP_JOURNAL_VOUCHER', 'PROCESS', 'INTERFACE');
      fp_journal_voucher.p_complete(p_doc_num => l_doc_num, p_msg_out => l_msg);
      if l_msg is not null then
--       fzkeprc.p_send_error_email(l_msg);
         null;
      end if;
   end if;
--
exception when others then
   fzkeprc.p_send_error_email('Could not create JV detail for PO (Order ID) '||p_pohd_code||' ('||p_order_ID||')'||chr(10)||SQLERRM);
end p_create_jv_schd;
--
--==============================================================================
--
procedure p_appr_po(p_doc_code fzrcxml.fzrcxml_doc_code%type,
                    p_error_msg in out varchar2,
                    p_sccss_msg in out varchar2) is
   pohd_rec fpbpohd%rowtype;
begin
--
-- Check if PO requires approvals
   select p_doc_code into pohd_rec.fpbpohd_code from fobsysc
    where fobsysc_eff_date <= sysdate
      and nvl(fobsysc_nchg_date, sysdate+1) > sysdate
      and fobsysc_appr_override_ind_po <> 'Y'
      and rownum = 1;
--
-- Check if PO already approved
   select * into pohd_rec from fpbpohd
    where fpbpohd_code = p_doc_code
      and fpbpohd_appr_ind <> 'Y';
--
-- Update FPBPOHD_APPR_IND
   update fpbpohd set fpbpohd_appr_ind = 'Y'
    where fpbpohd_code = p_doc_code;
--
dbms_output.put_line('FPBPOHD record updated, FPBPOHD_APPR_IND set to ''Y'' ');
--
-- Delete FOBUAPP created by PO API
   delete fobuapp where fobuapp_doc_code = p_doc_code;
--
-- Forward document to the posting process.
   fpkutil.p_forward_document(2, 'Purchase Order',
                              pohd_rec.fpbpohd_code,
                              pohd_rec.fpbpohd_user_id,
                              pohd_rec.fpbpohd_complete_ind,
                             'Y', -- pohd_rec.fpbpohd_appr_ind,
                              p_error_msg, p_sccss_msg);
dbms_output.put_line(p_sccss_msg);
--
exception
   when no_data_found then null; -- Nothing to do
   when others        then p_error_msg := 'Error in p_appr_po: '||SQLERRM;
end;
--
--
   -- TC MOD 20131211 START ------------------------------------------------------
--==============================================================================

PROCEDURE p_OrderCancel (p_error_msg IN OUT VARCHAR2)
IS
   v_error_msg    VARCHAR2(4000) := NULL;
   header_cursor  foksels.ref_cursortype;
   fpbpohd_rec    fpbpohd%ROWTYPE := NULL;
   fprpoda_rec    fprpoda%ROWTYPE;
   v_activity     VARCHAR2 (1);
   abal_indicator VARCHAR2(1);
   abal_message   VARCHAR2(1000);  
   lv_rucl_code   fprpoda.fprpoda_rucl_code_po%TYPE; 
   abal_ind       VARCHAR2(1) := 'N';  
   error_mesg_table    fpkutil.error_mesg_tabtype;
--   
   CURSOR acctg_cursor (p_po_code VARCHAR2)
       IS 
   SELECT * 
     FROM fprpoda
    WHERE fprpoda_pohd_Code = p_po_code
      AND fprpoda_change_seq_num   IS NULL;
    
begin
--
   fzkeprc.p_set_prc_name('p_OrderCancel');

  header_cursor := fpkpurr.f_fpbpohd_ref(g_header_rec.po_code);
  fetch header_cursor into fpbpohd_rec;
  if header_cursor%NOTFOUND then
     p_error_msg :=  'Purchase Order ID: '||g_header_rec.po_code ||' does not exist. Cannot cancel.';
     elsif     fpbpohd_rec.fpbpohd_complete_ind = 'Y'
           and fpbpohd_rec.fpbpohd_appr_ind = 'Y'
           and NVL(fpbpohd_rec.fpbpohd_cancel_ind,'N') = 'Y'
     then
       --  p_error_msg := 'Purchase Order ID: '|| g_header_rec.po_code ||' is already cancelled.';
           RETURN;  -- ALREADY CANCELLED 
  end if;
  close header_cursor;
  if p_error_msg IS NOT NULL
   THEN
      RETURN;
   END IF;

-- Insert a record in FOBAPPD - Approved Document Table
--<DGS 05112018>  start: comment out insert
--   fokutil.p_insert_fobappd  (2,g_header_rec.po_code);
--<DGS 05112018> end

-- Set indicators to simulate this PO has been through posting and approvals
   fpbpohd_rec.fpbpohd_cancel_ind := 'Y';
   fpbpohd_rec.fpbpohd_complete_ind := 'Y';

-- Change the use of SYSDATE or transdate depending on trans date
   if fpbpohd_rec.fpbpohd_trans_date > SYSDATE then
      fpbpohd_rec.fpbpohd_cancel_date := fpbpohd_rec.fpbpohd_trans_date;
   else
     fpbpohd_rec.fpbpohd_cancel_date := SYSDATE;
   end if;

-- Set cancel reason code for Unimarket  
   fpbpohd_rec.fpbpohd_crsn_code := 'EPCN';
   fpbpohd_rec.fpbpohd_activity_date := SYSDATE;
   fpbpohd_rec.FPBPOHD_ORIGIN_CODE := 'EPROCUREMENT';

--          
   fpkpurr.p_delete_header(fpbpohd_rec,g_error_mesg_table);

   if g_error_mesg_table.count >= 1 then
      p_error_msg := fpkutil.table_to_string(g_error_mesg_table);
      RETURN;
   end if;
 
   OPEN acctg_cursor(g_header_rec.po_code);
 -- update available balance for each expense item - release encumberance
   LOOP
     FETCH acctg_cursor INTO fprpoda_rec;
     EXIT WHEN acctg_cursor%NOTFOUND;

     IF fprpoda_rec.fprpoda_rucl_code_po = 'PORD'
     THEN 
        lv_rucl_code := 'PCRD';
     ELSE
        lv_rucl_code := fprpoda_rec.fprpoda_rucl_code_po;
     END IF;

     fgkabal.abal_p_check_available_balance
        (fprpoda_rec.fprpoda_pohd_code,      -- doc_code             
         '2',                                -- doc_type             
         TO_CHAR(fpbpohd_rec.fpbpohd_cancel_date,'YYYYMMDDHH24MISS'),   -- trans_date           
         'Y',                                -- reversal_ind         
         fprpoda_rec.fprpoda_item,           -- item_number          
         fprpoda_rec.fprpoda_seq_num,        -- seq_number           
         0,                                  -- submission_number    
         0,                                  -- serial_number        
         fprpoda_rec.fprpoda_coas_code,      -- coas_code            
         fprpoda_rec.fprpoda_fsyr_code,      -- fsyr_code            
         fprpoda_rec.fprpoda_fund_code,      -- fund_code            
         fprpoda_rec.fprpoda_orgn_code,      -- orgn_code            
         fprpoda_rec.fprpoda_acct_code,      -- acct_code            
         fprpoda_rec.fprpoda_prog_code,      -- prog_code            
         lv_rucl_code,                       -- rucl_code            
         fprpoda_rec.fprpoda_amt,            -- trans_amount         
         '+',                                -- dr_cr_ind            
         fprpoda_rec.fprpoda_period,         -- posting_period       
         NULL,                               -- budget_period        
         fprpoda_rec.fprpoda_pohd_code,      -- encd_num             
         fprpoda_rec.fprpoda_item,           -- encd_item_num        
         fprpoda_rec.fprpoda_seq_num,        -- encd_seq_num         
         NULL,                               -- encd_action_ind      
         'U',                                -- commitment_type      
         'Y',                                -- nsf_override_ind     
         abal_ind,                           -- abal_indicator (IN OUT)
         v_error_msg );                  -- abal_message   (IN OUT)  
     IF v_error_msg IS NOT NULL
     THEN 
       tcapp.util_msg.LogEntry(v_error_msg);
       p_error_msg := p_error_msg || ' PO-'||fprpoda_rec.fprpoda_pohd_code ||
                                     ' ITEM-'||fprpoda_rec.fprpoda_item ||
                                     ' SEQ-' || fprpoda_rec.fprpoda_seq_num||' '||
                                     v_error_msg;
       EXIT;  
     END IF;
     fpkpurr.p_delete_acctg(fprpoda_rec,
                            error_mesg_table,
                            g_header_rec.po_code);
     IF error_mesg_table.count >= 1 
     THEN
        p_error_msg := ' PO-'||fprpoda_rec.fprpoda_pohd_code ||     
                       ' ITEM-'||fprpoda_rec.fprpoda_item ||        
                       ' SEQ-' || fprpoda_rec.fprpoda_seq_num||' '||
                       fpkutil.table_to_string(error_mesg_table);
        EXIT;
     END IF;
   END LOOP;
   CLOSE acctg_cursor;
--
--<DGS 05112018> start
   IF p_error_msg is null then
      fokutil.p_insert_fobappd(2,g_header_rec.po_code);
   END IF;
--<DGS 05112018> end
--
EXCEPTION when others then
   p_error_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
END p_OrderCancel;
   -- TC MOD 20131211 END   ------------------------------------------------------
--==============================================================================
--
-- <DGS 20180604> Start
--
procedure p_CloseOrder(p_rsp in out CLOB, p_msg in out varchar2) 
IS
   v_error_msg         VARCHAR2(4000) := NULL;
   fpbpohd_rec         fpbpohd%ROWTYPE := NULL;
   v_cpo_state         VARCHAR2(6);
   v_cpo_rec           fishrs.fzepcpo%ROWTYPE;
   v_cpo_code          fishrs.fzepcpo.fzepcpo_code%TYPE;
   v_cpo_status        fishrs.fzepcpo.fzepcpo_status%TYPE;
   v_cpo_prior_yr      fishrs.fzepcpo.fzepcpo_prior_yr%TYPE;
   v_cpo_message       fishrs.fzepcpo.fzepcpo_message%TYPE;
   v_cpo_posting_date  fishrs.fzepcpo.fzepcpo_posting_date%TYPE;
   v_cpo_username      fishrs.fzepcpo.fzepcpo_username%TYPE;
   v_cpo_sys_date      fishrs.fzepcpo.fzepcpo_sys_date%TYPE;
   
--
   l_msg varchar2(4000);
   l_rsp xmltype := xmltype('<Response><Status code="204" text="No Content"/></Response>');
--   
begin

   fzkeprc.p_set_prc_name('p_CloseOrder');

-- Get requestor username
   fzkeprc.g_user := nvl(fzkeprc.g_eprf_rec.fzreprf_po_userid,
                         fzkeprc.f_get_val(g_Identity));

-- Create Response from Request
   select xmltype(replace(appendChildXML(deleteXML(deleteXML(fzkeprc.g_xml,
      '//Request'),'//Header'),'//cXML', l_rsp).getclobval(),'cxml-request','cxml-response'))
    into l_rsp from dual;
--
-- Update Response cXML/@payloadID
   select updateXML(l_rsp, '//cXML/@payloadID',
          'PO.'||to_char(sysdate,'yyyymmddhh24miss')||'.'||to_char(fzkeprc.g_rid))
     into l_rsp from dual;
--
--
--  Insert row into table fzepcpo
--
--  The state field will contain the values OPEN or CLOSE
--
--  The status field will contain the following values
--
--     C - Close New      ( a new request to close an order )
--     O - Open New       ( a new request to open  an order )
--     U - Unknown        ( a new request that is not an open or close )
--     P - In Process     ( assigned by sql fgr0293 when creating parameters for the fpppobc process  )
--     B - Sent to Banner ( assigned by sql fgr0293a after the fpppobc process has been executed )
--
   prior_fsyr          := null;
   prior_po            := null;
   v_cpo_state         := null;
   v_cpo_code          := null; 
   v_cpo_status        := 'U';
   v_cpo_prior_yr      := 'N';
   v_cpo_message       := 'Unknown';
   v_cpo_username      := null;
   v_cpo_sys_date      := sysdate;
   v_cpo_posting_date  := trunc(sysdate);
--
   v_cpo_code          := substr(fzkeprc.f_get_val(g_unimarket_order_number),1,8);
   v_cpo_state         := substr(fzkeprc.f_get_val(g_unimarket_order_state),1,6);
   v_cpo_username      := substr(fzkeprc.f_get_val(g_unimarket_changed_by_user),1,30);
--
   if v_cpo_state = 'CLOSED' then
      v_cpo_status  := 'C';   
      v_cpo_message := 'Close  New';
         else if v_cpo_state = 'OPEN' then
                 v_cpo_status  := 'O';
                 v_cpo_message := 'Open  New';
             end if;
   end if;
--
--   within dual fiscal year processing check for a prior year encumbance, 
--   if found change posting date to last day of the prior fiscal year
--
/*  Needs to be changed since we don't set the inv pref variable
if fzkeprc.g_eprf_rec.fzreprf_inv_trans_date = 'F'
      then prior_fsyr := to_char (sysdate,'rr');
           prior_po   := fp_encumbrance.f_fgbencp_exists ( p_num => v_cpo_code, p_fsyr => prior_fsyr );
              if prior_po = 'Y' then
                 v_cpo_posting_date :=  to_date ('31-AUG-' || to_char (sysdate,'yyyy'));
                 v_cpo_prior_yr     := 'Y';
              end if;
   end if; 
*/  
-- TC Mod Start
    select max(ftvfsyr_fsyr_code)
      into prior_fsyr
      from fpbpohd, ftvfsyr
      where trunc(fpbpohd_trans_date) between trunc(ftvfsyr_start_date) and trunc(ftvfsyr_end_date)
        and ftvfsyr_coas_code = '1'
        and fpbpohd_code = v_cpo_code;
        
    fzkeprc.p_get_trans_eff_dt(prior_fsyr,v_cpo_posting_date);
    
    if trunc(v_cpo_posting_date) < trunc(sysdate) then
      prior_po := 'Y';
    else
     prior_po := 'N';
    end if;
-- TC Mod End
--
   insert into fishrs.fzepcpo 
    (fzepcpo_code, fzepcpo_status, fzepcpo_prior_yr, fzepcpo_message, fzepcpo_posting_date,
     fzepcpo_username, fzepcpo_sys_date) 
   values 
    (v_cpo_code, v_cpo_status, v_cpo_prior_yr, v_cpo_message, v_cpo_posting_date,
     v_cpo_username, v_cpo_sys_date);
--
   commit;
--
--  send message for a request to open an order or if the status is unknown
--
   if v_cpo_state = 'OPEN' then
      fzkeprc.p_send_error_email('Request to re-open order ' || v_cpo_code ||', '||
      'User ' || v_cpo_username || ' has re-opened order ' || v_cpo_code);
   end if;
--   
   if v_cpo_status = 'U' then
      fzkeprc.p_send_error_email('Error in PO Close Unknown Request Type ' || v_cpo_code ||', '||
      'User ' || v_cpo_username || ' Request Type ' || v_cpo_state || '  Order ' || v_cpo_code);
   end if;
--   
-- Create OrderRequest FZRCXML log record  
      g_fzrcxml.fzrcxml_eprc_id    := fzkeprc.g_rid;
      g_fzrcxml.fzrcxml_doc_id     := v_cpo_code;
      g_fzrcxml.fzrcxml_doc_code   := v_cpo_code;
      g_fzrcxml.fzrcxml_doc_user   := fzkeprc.g_user;
      g_fzrcxml.fzrcxml_doc_type   := 'PO '  || substr(v_cpo_message,1,6);
      g_fzrcxml.fzrcxml_doc_status := 'C';
      g_fzrcxml.fzrcxml_doc        := fzkeprc.g_req;
--
     if v_cpo_status = 'C' then
         g_fzrcxml.fzrcxml_sccss_msg := 'Purchase Order: '||v_cpo_code||' has been closed.';
	   else 
	    if v_cpo_status = 'O' then
	         g_fzrcxml.fzrcxml_sccss_msg := 'Purchase Order: '||v_cpo_code||' open request.';
                   else
		         g_fzrcxml.fzrcxml_sccss_msg := 'Purchase Order: '||v_cpo_code||' unknown request.';
            end if;
     end if;
--      
      g_fzrcxml.fzrcxml_error_msg  := NULL;
      g_fzrcxml.fzrcxml_user_id    := USER;
      g_fzrcxml.fzrcxml_activity_date := SYSDATE;
--
      insert into fzrcxml values g_fzrcxml;
      commit;
--
         SELECT UPDATEXML (l_rsp,
                           '//Status/@code',
                           '200',
                           '//Status/@text',
                          g_fzrcxml.fzrcxml_sccss_msg)
         INTO l_rsp
         FROM DUAL;                                    
--
      p_msg := l_msg;
      p_rsp := l_rsp.getclobval();
      RETURN;
--
EXCEPTION when others then
   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
END p_CloseOrder;
-- <DGS 20180604> End
--==============================================================================
--
procedure p_OrderRequest(p_rsp in out CLOB, p_msg in out varchar2) is
   l_msg varchar2(4000);
   l_rsp xmltype := xmltype('<Response><Status code="204" text="No Content"/></Response>');
   l_blanket_trans_dt fpbpohd.fpbpohd_trans_date%TYPE;
 -- TC MOD 20170621 START ------------------------------------------------------  
function p_get_trans_date_for_jv(po_code fpbpohd.fpbpohd_code%TYPE) 
RETURN DATE
IS   
	   v_fy_end_date DATE := NULL;
 	   v_fy_start_date DATE := NULL;
     v_fsyr        varchar2(2) :=NULL;
     curr_fsyr     varchar2(2) := NULL;
     curr_fsyr_start DATE := NULL;
     curr_fsyr_end   DATE := NULL;
BEGIN
   SELECT max(ftvfsyr_start_date), MAX(ftvfsyr_end_date), max(ftvfsyr_fsyr_code)
     INTO v_fy_start_date, v_fy_end_date , v_fsyr
     FROM fgbencp,
          ftvfsyr
    WHERE fgbencp_num = po_code
      AND fgbencp_fsyr_code = ftvfsyr_fsyr_code;  
      
    select ftvfsyr_fsyr_code, ftvfsyr_start_date, ftvfsyr_end_date
       into curr_fsyr, curr_fsyr_start, curr_fsyr_end
       from ftvfsyr
      where trunc(sysdate) between trunc(ftvfsyr_start_date) and trunc(ftvfsyr_end_date);   
      
    if v_fsyr = curr_fsyr then 
        v_fy_end_date := sysdate;
    else
        if to_number(v_fsyr) > to_number(curr_fsyr) then
           v_fy_end_date := v_fy_start_date;
        end if;
    end if;   
      
   RETURN v_fy_end_date;   
   
EXCEPTION when others then
   RETURN TRUNC(SYSDATE);
end p_get_trans_date_for_jv;
-- TC MOD 20170621 END   ------------------------------------------------------  
begin
--
   fzkeprc.p_set_prc_name('p_OrderRequest');
--
-- Get requestor username
   fzkeprc.g_user := nvl(fzkeprc.g_eprf_rec.fzreprf_po_userid,
                         fzkeprc.f_get_val(g_Identity));
--
-- Create Response from Request
   select xmltype(replace(appendChildXML(deleteXML(deleteXML(fzkeprc.g_xml,
       '//Request'),'//Header'),'//cXML', l_rsp).getclobval(),'cxml-request','cxml-response'))
     into l_rsp from dual;
--
-- Update Response cXML/@payloadID
   select updateXML(l_rsp, '//cXML/@payloadID',
          'PO.'||to_char(sysdate,'yyyymmddhh24miss')||'.'||to_char(fzkeprc.g_rid))
     into l_rsp from dual;
--
-- Get cXML PO data
   p_get_po_data(l_msg);
--
-- Check if something wrong
   if l_msg is not null then
      p_msg := l_msg;
      p_rsp := l_rsp.getclobval();
      return;                                                       -- Return!!!
   end if;
--
--
   -- TC MOD 20131211 START ------------------------------------------------------     
   -- Check for Purchase Order Delete Request branch off here
   if g_request_type = 'delete' 
   then
   -- Return with request and response success or failure
      -- Create OrderRequest FZRCXML log record  -- may not need later
      if fzkeprc.g_orig like 'EPROC%' then
         g_fzrcxml.fzrcxml_doc_code := NULL;
      else
         g_fzrcxml.fzrcxml_doc_code := g_header_rec.po_code;
      end if;
      g_fzrcxml.fzrcxml_eprc_id    := fzkeprc.g_rid;
      g_fzrcxml.fzrcxml_doc_id     := g_order_ID;
      g_fzrcxml.fzrcxml_doc_user   := fzkeprc.g_user;
      g_fzrcxml.fzrcxml_doc_type   :='PO';
      g_fzrcxml.fzrcxml_doc_status :='I';
      g_fzrcxml.fzrcxml_doc        := fzkeprc.g_req;
      g_fzrcxml.fzrcxml_sccss_msg  := NULL;
      g_fzrcxml.fzrcxml_error_msg  := NULL;
      g_fzrcxml.fzrcxml_user_id    := USER;
      g_fzrcxml.fzrcxml_activity_date := SYSDATE;
      if fzkeprc.g_new_request = 'Y' then
         insert into fzrcxml values g_fzrcxml;
         commit;
      end if;   
      p_OrderCancel(l_msg); 
      if l_msg IS NOT NULL   -- something bad happened!
      THEN
         g_fzrcxml.fzrcxml_error_msg := l_msg;
         g_fzrcxml.fzrcxml_doc_status := 'E';
         l_msg := 'UM Order ID:  '||g_fzrcxml.fzrcxml_doc_id||CHR(10);
         l_msg := l_msg||'FPBPOHD_CODE: '||g_fzrcxml.fzrcxml_doc_code||CHR(10)||CHR(10);
         fzkeprc.p_send_error_email(l_msg||g_fzrcxml.fzrcxml_error_msg);      
      ELSE
      -- everything good, order has been cancelled.   
         g_fzrcxml.fzrcxml_doc_status := 'C';
         g_fzrcxml.fzrcxml_sccss_msg := 'Purchase Order: '
                                        ||g_header_rec.po_code
                                        ||' has been cancelled.';
         SELECT UPDATEXML (l_rsp,
                           '//Status/@code',
                           '200',
                           '//Status/@text',
                          g_fzrcxml.fzrcxml_sccss_msg)
         INTO l_rsp
         FROM DUAL;                                    
      END IF;
      -- Update OrderRequest FZRCXML log record
      update fzrcxml
         SET fzrcxml_doc_code      = g_fzrcxml.fzrcxml_doc_code,
             fzrcxml_doc_user      = UPPER(fzkeprc.g_user),
             fzrcxml_doc_status    = g_fzrcxml.fzrcxml_doc_status,
             fzrcxml_sccss_msg     = g_fzrcxml.fzrcxml_sccss_msg,
             fzrcxml_error_msg     = g_fzrcxml.fzrcxml_error_msg,
             fzrcxml_activity_date = SYSDATE
      where fzrcxml_eprc_id = fzkeprc.g_rid;
      commit; 
      -- return message and response           
      p_msg := l_msg;
      p_rsp := l_rsp.getclobval();
      RETURN;
   end if;  
   -- TC MOD 20131211 END   ------------------------------------------------------     
--
-- Check if PO already exists in the system
   begin
      select fzkeprc.g_orig||' orderID '||g_order_ID||
            '('||fpbpohd_code||') already exists in the system' into l_msg
        from fzrcxml, fpbpohd
       where fzrcxml_doc_id = g_order_ID
         and fzrcxml_doc_code = fpbpohd_code
         and rownum = 1;
      p_msg := l_msg;
      p_rsp := l_rsp.getclobval();
      return;                                                       -- Return!!!
   exception when no_data_found then null; end;
--
-- Get blanket FPBPOHD_CODE and JV trans_date
   if g_po_type = 'release' then
      begin
         select fpbpohd_code,
                greatest(fpbpohd_trans_date, sysdate)
           into g_blanket_code,
                g_jv_trans_dt
           from fzrcxml, fpbpohd
          where fzrcxml_doc_id = g_blanket_ID
            and fzrcxml_doc_type = 'PO'
            and fzrcxml_doc_status in ('C', 'W')    -- DGS 20170128  Added 'W' status
            and fzrcxml_doc_code = fpbpohd_code
            and fpbpohd_change_seq_num is null;
      exception when no_data_found then
         p_msg := 'Blanket PO not found for release orderID '||g_order_ID;
         p_rsp := l_rsp.getclobval();
         return;                                                    -- Return!!!
      end;
   end if;
--
-- Create OrderRequest FZRCXML log record
   if fzkeprc.g_orig like 'EPROC%' then
      g_fzrcxml.fzrcxml_doc_code := null;
   else
      g_fzrcxml.fzrcxml_doc_code := g_header_rec.po_code;
   end if;
   g_fzrcxml.fzrcxml_eprc_id    := fzkeprc.g_rid;
   g_fzrcxml.fzrcxml_doc_id     := g_order_ID;
   g_fzrcxml.fzrcxml_doc_user   := fzkeprc.g_user;
   g_fzrcxml.fzrcxml_doc_type   :='PO';
   g_fzrcxml.fzrcxml_doc_status :='I';
   g_fzrcxml.fzrcxml_doc        := fzkeprc.g_req;
   g_fzrcxml.fzrcxml_sccss_msg  := null;
   g_fzrcxml.fzrcxml_error_msg  := null;
   g_fzrcxml.fzrcxml_user_id    := user;
   g_fzrcxml.fzrcxml_activity_date := sysdate;
--
   if fzkeprc.g_new_request = 'Y' then
      insert into fzrcxml values g_fzrcxml;
      commit;
   end if;
--
--==============================================================================
-- Call Banner PO API 
   g_error_mesg_table.delete;
-- TC MOD 20140306 START ------------------------------------------------------
   SAVEPOINT SP_CREATE_PO;
-- TC MOD 20140306 END   ------------------------------------------------------     
   fpkpurr.p_create_po(g_header_rec,
                       g_item_table,
                       g_acct_table,
                       g_error_mesg_table,
                       g_fzrcxml.fzrcxml_sccss_msg);
   if g_fzrcxml.fzrcxml_sccss_msg is not null then
     dbms_output.put_line(g_fzrcxml.fzrcxml_sccss_msg);
   end if;
--==============================================================================
--
-- Get result DOC_CODE
   if fzkeprc.g_orig like 'EPROC%' or
      g_po_type in('regular','release') and fzkeprc.g_eprf_rec.fzreprf_po_docno = 'N' or
      g_po_type in('blanket') and fzkeprc.g_eprf_rec.fzreprf_po_blanket_docno = 'N' then
--    PO API should assign new DOC_CODE
      select max(fpbpohd_code)
        into g_fzrcxml.fzrcxml_doc_code
        from fpbpohd
       where fpbpohd_doc_ref_code = g_doc_ref_code
         and fpbpohd_origin_code  = fzkeprc.g_orig;
dbms_output.put_line('FPBPOHD_CODE = '||g_fzrcxml.fzrcxml_doc_code);
   end if;
--
   if g_error_mesg_table.count > 0 then
      for i in 1..g_error_mesg_table.count loop
         if i = 1 then
            dbms_output.put_line('Errors  '||i||': '||g_error_mesg_table(i));
         else
            dbms_output.put_line('        '||i||': '||g_error_mesg_table(i));
         end if;
         g_fzrcxml.fzrcxml_error_msg := g_fzrcxml.fzrcxml_error_msg||g_error_mesg_table(i)||chr(10);
      end loop;
      g_fzrcxml.fzrcxml_error_msg := rtrim(g_fzrcxml.fzrcxml_error_msg,chr(10));
      if instr(upper(g_fzrcxml.fzrcxml_error_msg),'ERROR') > 0 then
         g_fzrcxml.fzrcxml_doc_status := 'E';
-- TC MOD 20140306 START ------------------------------------------------------
         IF instr(upper(g_fzrcxml.fzrcxml_error_msg),'INSUFFICIENT BUDGET') > 0 
         THEN
            ROLLBACK TO SAVEPOINT SP_CREATE_PO;
            g_fzrcxml.fzrcxml_error_msg := g_fzrcxml.fzrcxml_error_msg||chr(10)||'PO deleted from Banner System.';
         END IF;
-- TC MOD 20140306 END   ------------------------------------------------------ 

-- TC MOD 20160601 START ------------------------------------------------------
         IF instr(upper(g_fzrcxml.fzrcxml_error_msg),'ENCUMBRANCES HAVE BEEN ROLLED') > 0 
         THEN
            ROLLBACK TO SAVEPOINT SP_CREATE_PO;
            g_fzrcxml.fzrcxml_error_msg := g_fzrcxml.fzrcxml_error_msg||chr(10)||'PO deleted from Banner System.';
         END IF;
-- TC MOD 20160601 END   ------------------------------------------------------ 

      else
         g_fzrcxml.fzrcxml_doc_status := 'W';
      end if;
   else
--    Check if PO completed
      if(fzkeprc.g_orig like 'EPROC%' or
         g_po_type in('regular','release') and fzkeprc.g_eprf_rec.fzreprf_po_docno = 'N' or
         g_po_type in('blanket') and fzkeprc.g_eprf_rec.fzreprf_po_blanket_docno = 'N') and
         g_fzrcxml.fzrcxml_doc_code not like 'P%' then
--       PO API did not complete PO and did not generate error message!
         g_fzrcxml.fzrcxml_error_msg := 'Unknown PO API Error';
dbms_output.put_line(g_fzrcxml.fzrcxml_error_msg);
         g_fzrcxml.fzrcxml_doc_status := 'E';
      else
         if fzkeprc.g_orig not like 'EPROC%' then
--          Approve and send PO to the posting process
            p_appr_po(g_fzrcxml.fzrcxml_doc_code,
                      g_fzrcxml.fzrcxml_error_msg,
                      g_fzrcxml.fzrcxml_sccss_msg);
dbms_output.put_line (g_fzrcxml.fzrcxml_error_msg);
         end if;
         if g_fzrcxml.fzrcxml_error_msg is not null then
            g_fzrcxml.fzrcxml_doc_status := 'W';
         else
            g_fzrcxml.fzrcxml_doc_status := 'C';
--          Create JV
            if g_jv_type in('PCard', 'blanket') then
               g_jv_docno   := f_jv_docno(g_fzrcxml.fzrcxml_doc_code);
               g_jv_user_id := upper(nvl(fzkeprc.g_eprf_rec.fzreprf_jv_userid, fzkeprc.g_user));
               g_trans_desc := substr(g_fzrcxml.fzrcxml_doc_code||'-'||g_order_ID||' VEND '||g_vend_id, 1, 35);
               if g_jv_type = 'PCard' then
                  g_jv_pohd_code := g_fzrcxml.fzrcxml_doc_code;
                  g_schedule := 'INVDELAY';
--                p_create_jv(g_fzrcxml.fzrcxml_doc_code);
               elsif g_jv_type = 'blanket' then
                  g_jv_pohd_code := g_blanket_code;
                  g_schedule := NULL;
--                  l_blanket_trans_dt := g_jv_trans_dt
                  g_jv_trans_dt := p_get_trans_date_for_jv(g_blanket_ID);
--                p_create_jv(g_blanket_code);
               end if;
-- TC MOD 20141007 START ------------------------------------------------------ 
               IF SUBSTR(g_acct_table(1).po_acct,1,1) = '7'
               THEN
               p_create_jv_schd
                     (p_user        => g_jv_user_id,
                      p_pohd_code   => g_jv_pohd_code,
                      p_order_ID    => g_order_ID,
                      p_jv_type     => g_jv_type,
                      p_jv_docno    => g_jv_docno,
                      p_jv_trans_dt => to_char(g_jv_trans_dt,'YYYYMMDDHH24MISS'),
                      p_jv_tr_desc  => g_trans_desc,
                      p_jv_tot_amt  => g_total_amt,
                      p_jv_rucl     => g_jv_rucl,
                      p_jv_act      => g_jv_act,
                      p_orig        => fzkeprc.g_orig,
                      p_schedule    => g_schedule);
--               ELSE
--                  tcapp.util_msg.LogEntry('Asset Account bypass JV'||g_acct_table(1).po_acct );
               END IF;
-- TC MOD 20141007 END  ------------------------------------------------------ 
            end if;
         end if;
      end if;
   end if;
--
-- TC MOD 20170414  START Just before we go, lets set the unit price on any items that are 0
    begin
       update fprpodt  set fprpodt_unit_price = .0001
       where fprpodt_pohd_code = g_order_id
       and fprpodt_unit_price = 0;
    exception
       when no_data_found then null;
       when others then g_fzrcxml.fzrcxml_error_msg := sqlerrm;
  end;
-- TC MOD 20170414  END Just before we go, lets set the unit price on any items that are 0
  
-- Update OrderRequest FZRCXML log record
   update fzrcxml
      set fzrcxml_doc_code      = g_fzrcxml.fzrcxml_doc_code,
          fzrcxml_doc_user      = upper(fzkeprc.g_user),
          fzrcxml_doc_status    = g_fzrcxml.fzrcxml_doc_status,
          fzrcxml_sccss_msg     = g_fzrcxml.fzrcxml_sccss_msg,
          fzrcxml_error_msg     = g_fzrcxml.fzrcxml_error_msg,
          fzrcxml_activity_date = sysdate
    where fzrcxml_eprc_id = fzkeprc.g_rid;
   commit;
--
-- Send Error Message
   if g_fzrcxml.fzrcxml_error_msg is not null then
      l_msg := 'UM Order ID:  '||g_fzrcxml.fzrcxml_doc_id||chr(10);
      if g_fzrcxml.fzrcxml_doc_code is null then
         l_msg := l_msg||'PO was not created in Banner system'||chr(10)||chr(10);
      else
   -- TC MOD 20130917 START ------------------------------------------------------     
   --      l_msg := l_msg||'FPBPOHD_CODE: '||g_fzrcxml.fzrcxml_doc_code||chr(10)||chr(10);
         l_msg := l_msg||'FPBPOHD CODE: '||g_fzrcxml.fzrcxml_doc_code||chr(10);
         l_msg := l_msg||'VENDOR CODE:  '||g_vend_id||chr(10)||chr(10);
   -- TC MOD 20130917 END   ------------------------------------------------------              
      end if;
      fzkeprc.p_send_error_email(l_msg||g_fzrcxml.fzrcxml_error_msg);
   -- TC MOD 20130425 START ------------------------------------------------------     
      p_check_nsf_po(g_fzrcxml.fzrcxml_doc_code);
   else
      p_check_nsf_po(g_fzrcxml.fzrcxml_doc_code);
   -- TC MOD 20130425 END   ------------------------------------------------------     
   end if;
--
-- Update Response
   if g_fzrcxml.fzrcxml_doc_status = 'C' then
      select updateXML(l_rsp, '//Status/@code', '200',
                              '//Status/@text', g_fzrcxml.fzrcxml_sccss_msg)
        into l_rsp from dual;
   end if;
--
   p_rsp := l_rsp.getclobval();
--
exception when others then
   p_msg := fzkeprc.p_got_error(SQLCODE, SQLERRM);
   p_rsp := l_rsp.getclobval();
end p_OrderRequest;
--
--==============================================================================
--
END FZKEPRC_PO;
/
