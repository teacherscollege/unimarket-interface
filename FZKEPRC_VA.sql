CREATE OR REPLACE PACKAGE                                                  "FZKEPRC_VA" IS
/*******************************************************************************

   NAME:       FZKEPRC_VA

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_VAL_ACCOUNT_CODE_REQUEST gets
                               <validate-account-code-request> 
                         and generates
                               <validate-account-code-response>

               Procedure P_ACCOUNT_CODE_REQUEST gets
                               <account-code-request>
                         and generates
                               <update-account-code-format-request>

   NOTES:      Both procedures called from FZKEPRC
   
   
   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20121212    J Stark       Add procedures to validate account index/account

   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/20/2011  S.Vorotnikov  Initial (moved from FZKEPRC).

*******************************************************************************/
--
-- Constants
   g_abal_doc_code constant varchar2(  6) := 'UNICON';
--
-- validate-account-code-request members
   g_account_user        constant varchar2(100):='user_ref_identity';
   g_account_line        constant varchar2(100):='account_line';
   g_account_line_number constant varchar2(100):='account_line_number';
   g_account_line_total  constant varchar2(100):='account_line_total';
   g_account_code        constant varchar2(100):='account_code';
   g_account_COAS        constant varchar2(100):='account_COAS';
   g_account_FUND        constant varchar2(100):='account_FUND';
   g_account_ORGN        constant varchar2(100):='account_ORGN';
   g_account_ACCT        constant varchar2(100):='account_ACCT';
   g_account_PROG        constant varchar2(100):='account_PROG';
-- TC MOD 20121212 START ------------------------------------------------------ 
   g_account_ACCI        constant varchar2(100):='account_INDEX';
-- TC MOD 20121212 END   ------------------------------------------------------ 
   g_account_code_value  constant varchar2(100):='account_code_value';
   g_account_code_format constant varchar2(100):='account_code_format';
   g_account_code_part   constant varchar2(100):='account_code_part';
   g_account_code_pct    constant varchar2(100):='account_code_pct';
   -- TC MOD 20160615 START --------------------------------------------  
   g_fy                    constant varchar2(100) := 'FY_code';
   g_first_fy              constant varchar2(100) := 'First_FY_code';
 -- TC MOD 20160615 END  ---------------------------------------------   
--
procedure p_val_account_request (p_rsp in out CLOB, p_msg in out varchar2);
procedure p_account_code_request(p_rsp in out CLOB, p_msg in out varchar2);
--
---=============================================================================
END FZKEPRC_VA;
/


CREATE OR REPLACE PACKAGE BODY                                                                                                                                                                                                                                               FZKEPRC_VA IS
/*******************************************************************************

   NAME:       FZKEPRC_VA

   PURPOSE:    This package is a part of the Unimarket eProcurement Interface.

               Procedure P_VAL_ACCOUNT_CODE_REQUEST gets
                               <validate-account-code-request>
                         and generates
                               <validate-account-code-response>

               Procedure P_ACCOUNT_CODE_REQUEST generates
                               <update-account-code-format-request>

   NOTES:      Both procedures called from FZKEPRC

   
   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20121212    J Stark       Add procedures to validate account index/account
   20121217    J Stark       Modify p_account_code_request for acci and account
   20130321    J Stark       Modify valid accounts to include the 6870-6879 range
   20130412    J Stark       Do not allow the following Index/account combination
                             Index 500000-599999 Account - 7324
                             Msg: 'Index xxx Account xxx cannot be used with GRANT FUNDS'
   20130425    J Stark       Do not load indexes that have terminated funds.
   20130501    J Stark       Modified Fund Org Security Check for Buyer: removed
                             Posting check option, changed message.
   20140227    J Stark       To ensure that the budget check includes multiple line 
                             items from the same index - do not delete budget entry 
                             and back out record FGRBAKO until the entire budget 
                             check has been completed. 
   20140306    J Stark       Modify valid accounts: add Account 2319, "Other Accrued 
                             Expenses" - a liability for vendor American International 
                             Distribution Corporation. As per Sheila Blalock.
   20140709    J Stark       Modify valid accounts to include 6851, 6831, 6852                            
   20140730    J Stark       Unimarket-342: Modify valid accounts to include 1411, 1412
                             (Assets accounts)  
   20140804    J Stark       Unimarket-352: Modify valid accounts to include 2285 for  
                             requisition.      
   20140910    J Stark       Unimarket-426: Modify valid accounts to include 1512.
   20140911    J Stark       Unimarket-449: Modify valid accounts to include 1514.
   20140918    J Stark       Allow upload of all 68XX accounts. 
   20141028    J Stark       Unimarket-595: Modify valid accounts to exclude 7222.
   20141031    J Stark       Unimarket-553: Modify valid index-acct upload to include 
                             index 811101 account 1891 Used by Student Accounts to 
                             process payments.
   20141117    J Stark       Unimarket-663: Modify valid index-acct upload to include 
                             account 2313.
   20150123    J Stark       Modify valid accounts to include 1513.
   20150202    J Stark       Unimarket-737: Modify valid accounts to exclude 5121 (REVENUE ACCOUNT).
   20150304    J Stark       Unimarket-771: Modify valid accounts to include 1291 (Asset Account).
   20150306    J Stark       Unimarket-737: Reopened, Remove: 5121 (REVENUE ACCOUNT).                                
   20150317    J Stark       Unimarket-787: Add account 1281 to valid accounts.                                
   20150513    J Stark       Remove change 20130412, restriction: Do not allow the following 
                             Index/account combination Index 500000-599999 Account - 7324, change                              
   20150522    J Stark       Modified index select to use Banner package, added
                             fund code expiration check.
   20150602    J Stark       Unimarket-843: Modify valid accounts to include 1516 (Asset).
   20151106    D Semar       Unimarket-1061:  Add account 122410
   20160108    D Semar       Unimarket-891:  Add account 2311
   20160101    D Semar       Modifications for Dual FY
   20160801    D Semar       Added accounts 2251 and 2252
   20160909    D Semar       Added accounts 2265 and 2284
   20160926    D Semar       Added account 2314
   20161005    D Semar       Added account 2295
   20170406    D Semar       Added account 2246
   20170804    D Semar       Added account 2289
   20170831    D Semar       Added accounts 722101,722102, 722103
                             Removed accounts 
                             Excluded account 7191
   20171002    D Semar       Added account 1532
   20171016    D Semar       Added account 7229
   20171207    D Semar       Excluded accounts '7311', '7315','7316','7317'
   20180611    D Semar       Added account 6856
   20180726    D Semar       Modified p_account_code_request to utilize new table fzbumva
                             Earlier modifications to include/exclude acct codes were deleted.
   
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   1.0  07/20/2011  S.Vorotnikov  Initial (moved from FZKEPRC).
   1.1  10/04/2011  S.Vorotnikov  <validate-account-code-response> changed:
                                    <part> elements added to <account-code>
                                  p_check_available_balance changed:
                                    account seq_num added
   1.2  10/06/2011  S.Vorotnikov  Use /account-code/part/@part to get FOAPAL

   2.0  10/13/2011  S.Vorotnikov  Use of fzreprf_bba_nsf added
   2.1  11/09/2011  S.Vorotnikov  Check Fund Org security added
   2.2  01/09/2012  S.Vorotnikov  Get values for attributes code and name
                                  of the xml element account-code-format
                                  from FTVSDAT (FZKEPRC/CODE-FORMAT)
   2.3  01/18/2012  S.Vorotnikov  Use of fzreprf_foapal_attt added

   3.0  01/31/2012  S.Vorotnikov  to support sinlge accounting (split OFF) changed
                                  l_acct_pct := fzkeprc.f_get_val(g_account_code_pct)
                                  nvl(l_acct_pct, 100)

   3.1  02/24/2012  S.Vorotnikov  Get APPR_RUCL from FTVSDAT RUCL_CODE/PORD
                                  or fpkeprf.f_get_epo_appr_rucl
                                  depending on Origin Code

   3.2  02/27/2012  S.Vorotnikov  Do not generate <attribute-definition> elements
                                  if FZREPRF_FOAPAL_ATTT is NULL

   3.3  03/19/2012  S.Vorotnikov  Added attribute depends-on-part-ref="COAS"
   
   4.0  04/27/2012  S.Vorotnikov  Bypass BBA/NSF checking if FZREPRF_BBA_NSF set to N
   5.0  05/13/2014  S.Vorotnikov  p_rsp := replace(xml_rsp.getclobval(), 'xmlns=""'); 

*******************************************************************************/
--
procedure p_val_account_request(p_rsp in out CLOB, p_msg in out varchar2) is
--
   xml_rsp      XMLType;      -- validate-account-code-response
   xml_acv      XMLType;      -- account-code-validation
   l_nsp        varchar2(200) := fzkeprc.g_namespace; -- xml name space (/@xmlns)
   trns_date    varchar2( 16) := to_char(sysdate,'YYYYMMDDHH24MISS');
   COAS_prev    varchar2(  1) := '?';
   abal_ind     varchar2( 10);
   abal_msg     varchar2(500);
--
   fobsysc_fund_org_sec fobsysc.fobsysc_fund_org_security_ind%type;
   l_appr_rucl          fpbeprf.fpbeprf_epo_appr_rucl%type;
--
   l_item_no    number;
   l_trans_amt  number;
   l_acct_pct   number;
   l_tot_pct    number;
   l_fsyr       varchar2(  4);
   l_fspd       varchar2(  2);
   l_acct_value varchar2(100);
   l_format_ref varchar2(100);
   l_coas       varchar2(  1);
   l_fund       varchar2(  6);
   l_orgn       varchar2(  6);
   l_acct       varchar2(  6);
   l_prog       varchar2(  6);
-- TC MOD 20121212 START ------------------------------------------------------ 
   l_acci       varchar2(6);
-- TC MOD 20121212 END   ------------------------------------------------------ 
--
   p_account_line varchar2(100) := fzkeprc.f_get_path(fzkeprc.g_req_type, g_account_line);
   p_account_code varchar2(100) := fzkeprc.f_get_path(fzkeprc.g_req_type, g_account_code);
   p_code_part    varchar2(100) := fzkeprc.f_get_path(fzkeprc.g_req_type, g_account_code_part);
-- TC MOD 20121212 START ------------------------------------------------------ 
   ftvacci_rec   fimsmgr.ftvacci%rowtype;
   ftvacci_count number;
-- TC MOD 20121212 END   ------------------------------------------------------ 
-- TC MOD 20150522 START ----------------------------------------------------- 
   ftvfund_rec   ftvfund%ROWTYPE;
   v_error_mesg  VARCHAR2(500);
-- TC MOD 20150522 END   ----------------------------------------------------- 
-- TC MOD 20160601 START  ----------------------------------------------------- 
   l_eff_date date := sysdate;
   p_account_fy               varchar2(100) := fzkeprc.f_get_path(fzkeprc.g_req_type, g_fy); 
   g_account_fy               varchar2(100) := fzkeprc.f_get_path(fzkeprc.g_req_type, g_first_fy);
   l_account_fy               varchar2(6);
-- TC MOD 20160601 START  -----------------------------------------------------   
-- TC MOD 20140227 START ------------------------------------------------------
TYPE fgrbako_delete_record IS RECORD ( item_num NUMBER(4), seq_num NUMBER(4) );
TYPE fgrbako_delete_vat_type IS VARRAY(5000) OF fgrbako_delete_record;
   fgrbako_delete_vat fgrbako_delete_vat_type := fgrbako_delete_vat_type();
   l_count_fgrbako_delete NUMBER(4) := 0; 
procedure set_delete_fgrbako_info (p_item_no NUMBER, 
                                   p_seq_num NUMBER) is
begin
  fgrbako_delete_vat.EXTEND;
  fgrbako_delete_vat(fgrbako_delete_vat.LAST).item_num := p_item_no;
  fgrbako_delete_vat(fgrbako_delete_vat.LAST).seq_num := p_seq_num;
  l_count_fgrbako_delete := l_count_fgrbako_delete + 1;
end set_delete_fgrbako_info;   
procedure do_delete_fgrbako is
begin
  IF l_count_fgrbako_delete > 0
  THEN
     FOR i IN 1..fgrbako_delete_vat.COUNT  LOOP
--          Delete FGRBAKO
            FGKABAL.delete_fgrbako(g_abal_doc_code, -- document_code,
                                  '2',              -- document_type,
                                   0,               -- submission_number,
                                   fgrbako_delete_vat(i).item_num, -- item_number,
                                   fgrbako_delete_vat(i).seq_num,  -- sequence_number,
                                  'N',              -- reversal_indicator,
                                   0,               -- ser_number,
                                   false);          -- from_posting
--      
     END LOOP;
  END IF;
end do_delete_fgrbako;
-- TC MOD 20140227 END   ------------------------------------------------------                                  
--------------------------------------------------------------------------------
procedure p_check_available_balance
         (item_no   number,
          seq_num   number,
          trans_amt number,
          coas_code varchar2,
          fund_code varchar2,
          orgn_code varchar2,
          acct_code varchar2,
          prog_code varchar2,
          abal_ind  in out varchar2,
          abal_msg  in out varchar2) is
begin
-- Call Banner base line procedure
   fgkabal.abal_p_check_available_balance(   -- parameter    -- Source
           g_abal_doc_code,                  -- doc_code     -- 'UNICON',
           '2',                              -- doc_type     -- '2' PO to_char(doc_type),
           trns_date,                        -- trans_date   -- sysdate YYYYMMDDHH24MISS
           'N',                              -- reversal_ind
           item_no,                          -- item_number  -- /validate-account-code-line/@line-number
           seq_num,                          -- seq_number   -- /account-code line number
           0,                                -- submission_number
           0,                                -- serial_number
           coas_code,                        -- coas_code    -- /account-code/part/[@part="COAS"]
           l_fsyr,                           -- fsyr_code    -- fokutil.p_validate_transdate
           fund_code,                        -- fund_code    -- /account-code/part/[@part="FUND"]
           orgn_code,                        -- orgn_code    -- /account-code/part/[@part="ORGN"]
           acct_code,                        -- acct_code    -- /account-code/part/[@part="ACCT"]
           prog_code,                        -- prog_code    -- /account-code/part/[@part="PROG"]
           l_appr_rucl,                      -- rucl_code    -- FTVSDAT RUCL_CODE/PORD or fpkeprf.f_get_epo_appr_rucl
           trans_amt,                        -- trans_amount -- /validate-account-code-line/total*(account-code/@distribution-percentage)
           '+',                              -- dr_cr_ind
           l_fspd,                           -- posting_period -- fokutil.p_validate_transdate
           '',                               -- budget_period
           g_abal_doc_code,                  -- encd_num      = po_code
           item_no,                          -- encd_item_num = item_number
           seq_num,                          -- encd_seq_num
           '',                               -- encd_action_ind
           'U',                              -- commitment_type
           'N',                              -- nsf_override_ind
           abal_ind,                         -- abal_indicator -- out
           abal_msg);                        -- abal_message   -- out
end p_check_available_balance;
--------------------------------------------------------------------------------
--
begin

--
-- Get requestor username (just for log)
   fzkeprc.g_user := fzkeprc.f_get_val(g_account_user,true);
--
-- Get FOBSYSC FUND ORGN security ind
   begin
      select fobsysc_fund_org_security_ind
        into fobsysc_fund_org_sec
        from fobsysc
       where fobsysc_status_ind = 'A' and fobsysc_eff_date <= sysdate
         and nvl(fobsysc_nchg_date, sysdate+1) > sysdate;
   exception when others then fobsysc_fund_org_sec := 'N';
   end;
--
-- Get APPR_RUCL
   if fzkeprc.g_orig like 'EPROC%' then
      l_appr_rucl := fpkeprf.f_get_epo_appr_rucl;
   else
      select ftvsdat_data into l_appr_rucl
        from ftvsdat
       where ftvsdat_sdat_code_attr  = 'RUCL_CODE'
         and ftvsdat_sdat_code_opt_1 = 'PORD'
         and ftvsdat_status_ind      = 'A'
         and ftvsdat_eff_date <= sysdate
         and nvl(ftvsdat_term_date, sysdate+1) > sysdate
         and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
   end if;
--

 --TC MOD 20160601 START ----------------------------------------

   /* Get the first instance of financial-year node */
   
   -- select extractvalue(value(tb),'/financial-year/@code',l_nsp) 
   -- into l_account_fy
   -- from table ( XMLSequence( extract(fzkeprc.g_xml, 
   --      '(/validate-account-code-request/validate-account-code-line/financial-year)[1]',
   --      l_nsp))) tb;
 
  begin  -- hot fix 9/6 
    select extractvalue(value(tb),p_account_fy,l_nsp) 
    into l_account_fy
    from table ( XMLSequence( extract(fzkeprc.g_xml,g_account_fy,l_nsp))) tb;
    
    l_account_fy := substr(l_account_fy,5,2);
  exception
    when others then
    select ftvfsyr_fsyr_code
      into l_account_fy
      from ftvfsyr
      where ftvfsyr_coas_code = '1'
      and trunc(sysdate) between trunc(ftvfsyr_start_date) and trunc(ftvfsyr_end_date);
--     l_account_fy := 'FY2017';
  end;
  
--    fzkeprc.p_get_trans_eff_dt(substr(l_account_fy,5,2), l_eff_date);
 
      fzkeprc.p_get_trans_eff_dt(l_account_fy, l_eff_date);
 
    trns_date := to_char(nvl(l_eff_date,sysdate),'YYYYMMDDHH24MISS');  
   
   
   
--TC MOD 20160601 END  ----------------------------------------
  
-- Create root xml element validate-account-code-response
   xml_rsp := XMLType('<?xml version="1.0" encoding="UTF-8"?>'||
                      '<validate-account-code-response '||l_nsp||'>'||
                      '   <response code="OK"/>'||
                      '</validate-account-code-response>');
-- For each item line
   for i in(select value(item) xml from table
                  (XMLSequence(extract(fzkeprc.g_xml, p_account_line, l_nsp))) item)
   loop fzkeprc.g_xml := i.xml;
--
--    Get item values from xml
      l_item_no   := fzkeprc.f_get_val(g_account_line_number, true);
      l_trans_amt := fzkeprc.f_get_val(g_account_line_total,  true);
      l_tot_pct   := 0;
--
--    For each account line
      for a in(select rownum seq_num, value(acct) xml from table
                     (XMLSequence(extract(i.xml, p_account_code, l_nsp))) acct)
      loop fzkeprc.g_xml := a.xml;
--
--       Get accounting values from xml
         l_acct_value := fzkeprc.f_get_val(g_account_code_value);
         l_format_ref := fzkeprc.f_get_val(g_account_code_format);
         l_acct_pct   := fzkeprc.f_get_val(g_account_code_pct);
-- TC MOD 20121212 START ------------------------------------------------------          
--         l_coas       := fzkeprc.f_get_val(g_account_COAS, true);
--         l_fund       := fzkeprc.f_get_val(g_account_FUND, true);
--         l_orgn       := fzkeprc.f_get_val(g_account_ORGN, true);
--         l_acct       := fzkeprc.f_get_val(g_account_ACCT);
-- TC MOD 20140306  l_coas       := fzkeprc.f_get_val(g_account_COAS);
-- TC MOD 20140306  l_fund       := fzkeprc.f_get_val(g_account_FUND);
-- TC MOD 20140306  l_orgn       := fzkeprc.f_get_val(g_account_ORGN);  
         l_acct       := fzkeprc.f_get_val(g_account_ACCT,true);  
         l_acci       := fzkeprc.f_get_val(g_account_ACCI, true);
  
-- TC MOD 20121212 END   ------------------------------------------------------ 

-- TC MOD 20140306  l_prog       := fzkeprc.f_get_val(g_account_PROG);
-- TC MOD 20150522 START ------------------------------------------------------ 
/*
 -- TC MOD 20121212 START ------------------------------------------------------ 
          ftvacci_count := 0;
          select count(*)
            into ftvacci_count
            from ftvacci
           where ftvacci_coas_code = '1' and
                 ftvacci_acci_code = l_acci and
                 ftvacci_eff_date <= sysdate and
                 ftvacci_nchg_date >= sysdate;
 --
          if ftvacci_count = 1 then
             select * 
               into ftvacci_rec
               from ftvacci
              where ftvacci_coas_code = '1' and
                 ftvacci_acci_code = l_acci and
                    ftvacci_eff_date <= sysdate and
                    ftvacci_nchg_date >= sysdate; 
 --
             l_coas := ftvacci_rec.ftvacci_coas_code;
             if ftvacci_rec.ftvacci_fund_code is not null then
                l_fund := ftvacci_rec.ftvacci_fund_code;
             end if;
             if ftvacci_rec.ftvacci_orgn_code is not null then
                 l_orgn := ftvacci_rec.ftvacci_orgn_code;
             end if;
             if ftvacci_rec.ftvacci_acct_code is not null then
                l_acct := ftvacci_rec.ftvacci_acct_code;
             end if;            
             if ftvacci_rec.ftvacci_prog_code is not null then
                l_prog := ftvacci_rec.ftvacci_prog_code;
             end if;
 --                   
          end if;                                    
 -- TC MOD 20121212 END   ------------------------------------------------------ 
*/
--       init indicators for errors
         abal_ind := null; abal_msg := null;
-- TC MOD 20160601 START ------------------------------------------------------ 
         /* Make sure the FY encumbrances haven't rolled */
         begin
         select 'INVALID', 'Cannot use FY'||l_account_fy||'. Encumbrances have been rolled'
         into abal_ind, abal_msg
         from fgbyrlm
         where fgbyrlm_coas_code = '1'
         and fgbyrlm_fsyr_code = l_account_fy
         and fgbyrlm_encb_per_date is not null;
         exception
          when no_data_found then null;
          when others then abal_ind := 'INVALID'; abal_msg := sqlerrm;
        end;
-- TC MOD 20160601 END  ------------------------------------------------------ 
--       get index infomation 

         foksels.p_get_ftvacci_rec('1', 
                                   l_acci,
                                   l_eff_date,    --TC MOD 20160601 dgs change to l_eff_date from sysdate
                                   ftvacci_rec,
                                   v_error_mesg
                                   );
-- error getting index?
         IF v_error_mesg IS NOT NULL
         THEN
            abal_ind := 'INVALID';
            abal_msg := v_error_mesg;
         ELSE
            l_coas := ftvacci_rec.ftvacci_coas_code;
            if ftvacci_rec.ftvacci_fund_code is not null then
               l_fund := ftvacci_rec.ftvacci_fund_code;
            end if;
            if ftvacci_rec.ftvacci_orgn_code is not null then
                l_orgn := ftvacci_rec.ftvacci_orgn_code;
            end if;
            if ftvacci_rec.ftvacci_acct_code is not null then
               l_acct := ftvacci_rec.ftvacci_acct_code;
            end if;            
            if ftvacci_rec.ftvacci_prog_code is not null then
               l_prog := ftvacci_rec.ftvacci_prog_code;
            end if;         	           	  
         END IF;
-- CHECK THAT FUND CODE IS NOT EXPIRED
         IF abal_msg IS NULL 
         THEN
          foksels.p_get_ftvfund_rec('1', 
                                   l_fund,
                                   l_eff_date,   --TC MOD 20160601 dgs change to l_eff_date from sysdate
                                   ftvfund_rec,
                                   v_error_mesg,
                                   'Y'
                                   );
         END IF;                                   
-- error getting fund or fund terminated?
         IF v_error_mesg IS NOT NULL
         THEN
            abal_ind := 'INVALID';
            abal_msg := v_error_mesg;        	
         END IF;
-- TC MOD 20150522 END   ------------------------------------------------------ 

         l_tot_pct    := l_tot_pct + to_number(l_acct_pct);
--
--       Generate new account-code-validation xml element
         select xmlelement("account-code-validation",
                   xmlattributes(l_item_no as "line-number"),
         decode(l_acct_pct, null,
                   xmlelement("account-code",
                      xmlattributes(l_format_ref as "format-ref",
                                    l_acct_value as "value")),
                   xmlelement("account-code",
                      xmlattributes(l_format_ref as "format-ref",
                                    l_acct_pct   as "distribution-percentage",
                                    l_acct_value as "value")) ))
           into xml_acv from dual;
--
--       Append FOAPAL parts to account-code
         for f in(select ExtractValue(value(part),'//part',  l_nsp) val,
                         ExtractValue(value(part),'//@part', l_nsp) atr
                    from table(XMLSequence(extract(a.xml, p_code_part, l_nsp))) part)
         loop
            select AppendChildXML(xml_acv,p_account_code,
                   xmlelement("part",xmlattributes(f.atr as "part"),f.val))
              into xml_acv from dual;
         end loop;
--
--       If new COAS code then get new FSYR and FSPD codes
-- TC MOD 20150522 START ------------------------------------------------------ 
--         abal_ind := null; abal_msg := null;
-- TC MOD 20150522 END   ------------------------------------------------------          
         if l_coas <> COAS_prev then
            begin
               select ftvfsyr_coas_code, ftvfsyr_fsyr_code, ftvfspd_fspd_code
                 into COAS_prev, l_fsyr, l_fspd
                 from ftvfspd, ftvfsyr
                where ftvfsyr_coas_code = l_coas
                  and ftvfspd_fsyr_code = ftvfsyr_fsyr_code
                  and ftvfspd_coas_code = ftvfsyr_coas_code
                  and trunc(sysdate) between
                      trunc(ftvfspd_prd_start_date) and
                      trunc(ftvfspd_prd_end_date);
            exception when no_data_found then
               abal_ind := 'INVALID';
               abal_msg := 'Accounting year or period is not setup in chart '||l_coas;
            end;
         end if;
-- TC MOD 20150513 START ------------------------------------------------------ 
-- TC MOD 20130412 START ------------------------------------------------------ 
/*
         if abal_msg is null
         then
            if l_acci between '500000' and '599999' and l_acct = '7324'
            then
               abal_ind := 'INVALID';
               abal_msg := 'Index '||l_acci|| ' Account '|| l_acct ||' cannot be used with GRANT FUNDS';
               dbms_output.put_line(abal_msg);
            end if;               
         end if;
*/         
-- TC MOD 20130412 END   ------------------------------------------------------          
-- TC MOD 20150513 END   ------------------------------------------------------          
--
--       Check FUND ORGN security
         if abal_msg is null and fobsysc_fund_org_sec = 'Y' then
--          Check FZREPRF_PO_USERID FUND ORGN security
            if fzkeprc.g_eprf_rec.fzreprf_po_userid is not null and
               fokutil.f_fund_org_security_fnc(l_COAS, l_FUND, l_ORGN,
                       sysdate, 'P', upper(fzkeprc.g_eprf_rec.fzreprf_po_userid)) = 'N' then
               abal_msg := 'FZREPRF_PO_USERID '||fzkeprc.g_eprf_rec.fzreprf_po_userid||
                          ' does not have authority to post to chart '||l_COAS||
                         ', Fund '||l_FUND||' and Organization '||l_ORGN;
dbms_output.put_line(abal_msg);
               fzkeprc.p_send_error_email(abal_msg);
               abal_msg := null;
            end if;
--          Check buyer FUND ORGN security
            if fzkeprc.g_eprf_rec.fzreprf_fund_org_security_ind = 'Y' and
-- TC MOD 20130501 START ------------------------------------------------------   
               fokutil.f_fund_org_security_fnc(l_COAS, l_FUND, l_ORGN,
                       sysdate, 'Q', upper(fzkeprc.g_user)) = 'N' then
               abal_ind := 'INVALID';
               abal_msg := 'User '||upper(fzkeprc.g_user)||
                          ' does not have access to this Index '||l_acci||
                         ', Fund '||l_FUND||' and Organization '||l_ORGN;            
/*               fokutil.f_fund_org_security_fnc(l_COAS, l_FUND, l_ORGN,
                       sysdate, 'P', upper(fzkeprc.g_user)) = 'N' then
               abal_ind := 'INVALID';
               abal_msg := 'User '||upper(fzkeprc.g_user)||
                          ' does not have authority to post to chart '||l_COAS||
                         ', Fund '||l_FUND||' and Organization '||l_ORGN;
*/                         
-- TC MOD 20130501 END  ------------------------------------------------------                          
dbms_output.put_line(abal_msg);
            end if;
         end if;
--
--==============================================================================
--       Check Available Balance
         if abal_msg is null then
            if fzkeprc.g_eprf_rec.fzreprf_bba_nsf = 'N' then
               abal_ind := 'Y';
            else
               p_check_available_balance(l_item_no, a.seq_num,
                                         l_trans_amt*(nvl(l_acct_pct,100)/100),
                                         l_COAS, l_FUND, l_ORGN, l_ACCT, l_PROG,
                                         abal_ind, abal_msg);
            end if;
--          Set Result
            if abal_ind  = 'Y' then
               abal_ind := 'VALID';
               abal_msg := l_acct_value||' is valid';
            elsif fzkeprc.g_eprf_rec.fzreprf_bba_nsf = 'W' then
               abal_ind := 'WARNING';
            elsif instr(upper(abal_msg),'WARNING')>0 then
               abal_ind := 'WARNING';
            else
               abal_ind := 'INVALID';
            end if;
--
dbms_output.put_line(l_item_no||': '||a.seq_num||' '||l_acct_value||': '||abal_ind||': '||abal_msg);
--
-- TC MOD 20140227 START ------------------------------------------------------
--          Delete FGRBAKO
--            FGKABAL.delete_fgrbako(g_abal_doc_code, -- document_code,
--                                  '2',              -- document_type,
--                                   0,               -- submission_number,
--                                   l_item_no,       -- item_number,
--                                   a.seq_num,       -- sequence_number,
--                                  'N',              -- reversal_indicator,
--                                   0,               -- ser_number,
--                                   false);          -- from_posting
            set_delete_fgrbako_info(l_item_no, a.seq_num);
-- TC MOD 20140227 END   ------------------------------------------------------                                   
--
         end if;
--==============================================================================
--
--       Add message to account-code-validation
         select InsertChildXML(xml_acv,'/account-code-validation', 'message',
                xmlelement("message", abal_msg))
           into xml_acv from dual;
--
--       Add result to account-code-validation
         select InsertChildXML(xml_acv,'/account-code-validation', '@result',
                abal_ind)
           into xml_acv from dual;
--
--       Append account-code-validation to validate-account-code-response
         select AppendChildXML(xml_rsp, '/validate-account-code-response',
                xml_acv, l_nsp)
           into xml_rsp from dual;
--
      end loop; -- For each account line
--
--    Check total distribution-percentage
      if l_tot_pct <> 100 then
         p_msg := 'Item '||l_item_no||': Total distribution-percentage '||l_tot_pct||' not equal 100';
-- TC MOD 20140227 START ------------------------------------------------------
         do_delete_fgrbako();
-- TC MOD 20140227 END   ------------------------------------------------------          
         return;                                                    -- Return!!!
      end if;
--
   end loop; -- For each item line
--
   p_rsp := replace(xml_rsp.getclobval(), 'xmlns=""'); -- sergeyv 05/13/2014
--
-- TC MOD 20140227 START ------------------------------------------------------
     do_delete_fgrbako();
-- TC MOD 20140227 END   ------------------------------------------------------    
exception when others then
   p_msg := 'p_val_account_code_request: '||SQLERRM;
   -- TC MOD 20140227 START ------------------------------------------------------
   do_delete_fgrbako();
   -- TC MOD 20140227 END   ------------------------------------------------------     
end p_val_account_request;
--
--==============================================================================
--
procedure p_account_code_request(p_rsp in out CLOB, p_msg in out varchar2) is
   l_clob clob;
   l_format_code ftvsdat.ftvsdat_sdat_code_opt_1%type;
   l_format_name varchar2(100);
   l_attt_code   fzreprf.fzreprf_foapal_attt%type := fzkeprc.g_eprf_rec.fzreprf_foapal_attt;
   -- TC MOD 20160601  START
   l_eff_date date := sysdate;
   l_account_fy varchar2(6);
   g_account_fy varchar2(100);
   p_account_fy varchar2(100);
  --  l_nsp        varchar2(200) := fzkeprc.g_namespace; -- xml name space (/@xmlns)
   -- TC MOD 20160601  END
begin
--

-- Get values for code and name attributes of the xml element account-code-format
   select ftvsdat_sdat_code_opt_1,
          fzkeprc.g_eprf_rec.fzreprf_instu_ref||' '||ftvsdat_sdat_code_opt_1||' Parts'
     into l_format_code, l_format_name
     from ftvsdat
    where ftvsdat_sdat_code_entity = 'FZKEPRC'
      and ftvsdat_sdat_code_attr   = 'CODE-FORMAT'
      and ftvsdat_status_ind       = 'A'
      and ftvsdat_eff_date <= sysdate
      and nvl(ftvsdat_term_date, sysdate+1) > sysdate
      and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
--
   with v as(select fmt_prt,
                    picklist_prt,
                    picklist_desc,
                    ftvsdat_code_level  picklist_prt_number,
                    lower(ftvsdat_data) prt_required,
                    attribute_value,
                    parent_coas
               from(select 'COAS'            fmt_prt,
                           ftvcoas_coas_code picklist_prt,
                           ftvcoas_title     picklist_desc,
                           NULL              attribute_value,
                           NULL              parent_coas
                      from ftvcoas where ftvcoas_status_ind = 'A'
                       and nvl(ftvcoas_term_date,sysdate+1)> l_eff_date
                       and l_eff_date>=ftvcoas_eff_date and l_eff_date<ftvcoas_nchg_date
                     union all
-- TC MOD 20121217 START ------------------------------------------------------
                    select DISTINCT 'INDEX', 
                           ftvacci_acci_code, 
                           ftvacci_title, 
                           NULL,
                           FTVACCI_COAS_CODE
                      from ftvacci, 
                           ftvsdat,
-- TC MOD 20130425 START ------------------------------------------------------
                           ftvfund
-- TC MOD 20130425 END   ------------------------------------------------------
                     where ftvacci_status_ind = 'A'
                       and ftvsdat_status_ind = 'A'
                       and ftvacci_coas_code = ftvsdat_coas_code
                       and ftvsdat_sdat_code_entity = 'FZKEPRC'
                       and ftvsdat_sdat_code_attr   = 'ATYP_INT_ALLOW'
                       and ftvsdat_eff_date<=sysdate
                       and nvl(ftvacci_term_date,sysdate+1)>l_eff_date  -- dgs change to l_eff_date
                       and nvl(ftvsdat_term_date,sysdate+1)>sysdate
                       AND NVL(FTVSDAT_NCHG_DATE,SYSDATE+1)>SYSDATE
                       AND l_eff_date>=ftvacci_eff_date and l_eff_date<ftvacci_nchg_date  -- dgs change to l_eff_date
                       AND SUBSTR(ftvacci_acci_code,1,1) BETWEEN '0' AND '9'
-- TC MOD 20141031 START ------------------------------------------------------                                               
--                       AND SUBSTR(ftvacci_acci_code,1,1) NOT IN ('3','4','8')
                       AND (ftvacci_acci_code = '811101'
                            OR
                            SUBSTR(ftvacci_acci_code,1,1) NOT IN ('3','4','8')
                            )
-- TC MOD 20141031 END   ------------------------------------------------------                                               
                       AND ftvacci_acci_code NOT LIKE '% %'
-- TC MOD 20130425 START ------------------------------------------------------                       
                       AND ftvacci_coas_code = ftvfund_coas_code
                       AND ftvacci_fund_code = ftvfund_fund_code
                       AND nvl(ftvfund_term_date,sysdate+1)> l_eff_date                 -- dgs change to l_eff_date
                       AND l_eff_date>=ftvfund_eff_date and l_eff_date<ftvfund_nchg_date  -- dgs change to l_eff_date
-- TC MOD 20130425 END   ------------------------------------------------------                       
                     UNION ALL 
-- TC MOD 20121217 END   ------------------------------------------------------
                    select 'FUND', ftvfund_fund_code, ftvfund_title, ftrfnda_attv_code,
                           ftvfund_coas_code
                      from ftvfund, ftrfnda
                     where ftvfund_status_ind = 'A'
                       and ftvfund_data_entry_ind = 'Y'
                       and nvl(ftvfund_term_date,sysdate+1)> l_eff_date                 -- dgs change to l_eff_date
                       and l_eff_date>=ftvfund_eff_date and l_eff_date<ftvfund_nchg_date  -- dgs change to l_eff_date
                       and ftvfund_coas_code = ftrfnda_coas_code(+)
                       and ftvfund_fund_code = ftrfnda_fund_code(+)
                       and ftrfnda_attt_code(+) = l_attt_code
                     union all
                    select 'ORGN', ftvorgn_orgn_code, ftvorgn_title, ftrorga_attv_code,
                           ftvorgn_coas_code
                      from ftvorgn, ftrorga
                     where ftvorgn_status_ind = 'A'
                       and ftvorgn_data_entry_ind = 'Y'
                       and nvl(ftvorgn_term_date,sysdate+1)> l_eff_date                 -- dgs change to l_eff_date
                       and l_eff_date >= ftvorgn_eff_date and l_eff_date <ftvorgn_nchg_date  -- dgs change to l_eff_date
                       and ftvorgn_coas_code = ftrorga_coas_code(+)
                       and ftvorgn_orgn_code = ftrorga_orgn_code(+)
                       and ftrorga_attt_code(+) = l_attt_code
                     union all
                    select 'ACCT', ftvacct_acct_code, ftvacct_title, ftracta_attv_code,
                           ftvacct_coas_code
                      from ftvacct, ftvatyp, ftvsdat, ftracta
                     where ftvacct_status_ind = 'A'
                       and ftvatyp_status_ind = 'A'
                       and ftvsdat_status_ind = 'A'
                       and ftvacct_coas_code = ftvatyp_coas_code
                       and ftvacct_atyp_code = ftvatyp_atyp_code
                       and ftvatyp_coas_code = ftvsdat_coas_code
                       and ftvatyp_internal_atyp_code = ftvsdat_sdat_code_opt_1
                       and ftvsdat_sdat_code_entity = 'FZKEPRC'
                       and ftvsdat_sdat_code_attr   = 'ATYP_INT_ALLOW'
                       and ftvacct_data_entry_ind   = 'Y'
                       and ftvsdat_eff_date<=sysdate
                       and nvl(ftvacct_term_date,sysdate+1)> l_eff_date    --TC MOD 20160601 dgs change to l_eff_date
                       and nvl(ftvatyp_term_date,sysdate+1)> l_eff_date    --TC MOD 20160601 dgs change to l_eff_date
                       and nvl(ftvsdat_term_date,sysdate+1)>sysdate
                       and nvl(ftvsdat_nchg_date,sysdate+1)>sysdate     
                       and l_eff_date >=ftvacct_eff_date and l_eff_date <ftvacct_nchg_date --TC MOD 20160601 dgs change to l_eff_date
                       and l_eff_date >=ftvatyp_eff_date and l_eff_date <ftvatyp_nchg_date  --TC MOD 20160601 dgs change to l_eff_date
                       and ftvacct_coas_code = ftracta_coas_code(+)
                       and ftvacct_acct_code = ftracta_acct_code(+)
                       and ftracta_attt_code(+) = l_attt_code
-- TC MOD 20121217 START ------------------------------------------------------
                       and (ftvacct_acct_code BETWEEN '7000' AND '7999'
                            or ftvacct_acct_code BETWEEN '6800' AND '6899' 
-- TC MOD 20180726 START   ------------------------------------------------------   
                            or ftvacct_acct_code in (select fzbumva_acct_code from fzbumva
                                                      where fzbumva_inc_flg = 'Y')
-- TC MOD 20180726 END   ------------------------------------------------------   
                            )                                             
                       and ftvacct_acct_code NOT  BETWEEN '7800' AND '7899'   
-- TC MOD 20180726 START   ------------------------------------------------------   
                       and ftvacct_acct_code not in (select fzbumva_acct_code from fzbumva
                                                      where fzbumva_inc_flg = 'N')
-- TC MOD 20180726 END   ------------------------------------------------------   

                     union all
                    select 'PROG', ftvprog_prog_code, ftvprog_title, ftrprga_attv_code,
                           ftvprog_coas_code
                      from ftvprog, ftrprga
                     where ftvprog_status_ind = 'A'
                       and ftvprog_data_entry_ind = 'Y'
                       and nvl(ftvprog_term_date,sysdate+1)> l_eff_date  --TC MOD 20160601 dgs change to l_eff_date
                       and l_eff_date >=ftvprog_eff_date and l_eff_date <ftvprog_nchg_date   --TC MOD 20160601 dgs change to l_eff_date
                       and ftvprog_coas_code = ftrprga_coas_code(+)
                       and ftvprog_prog_code = ftrprga_prog_code(+)
                       and ftrprga_attt_code(+) = l_attt_code
                     union all
                    select 'ACTV', ftvactv_actv_code, ftvactv_title, ftracva_attv_code,
                           ftvactv_coas_code
                      from ftvactv, ftracva
                     where ftvactv_status_ind = 'A'
                       and nvl(ftvactv_term_date,sysdate+1)>sysdate                 --TC MOD 20160601 dgs change to l_eff_date
                       and sysdate>=ftvactv_eff_date and sysdate<ftvactv_nchg_date   --TC MOD 20160601 dgs change to l_eff_date
                       and ftvactv_coas_code = ftracva_coas_code(+)
                       and ftvactv_actv_code = ftracva_actv_code(+)
                       and ftracva_attt_code(+) = l_attt_code
                     union all
                    select 'LOCN', ftvlocn_locn_code, ftvlocn_title, ftrlcna_attv_code,
                           ftvlocn_coas_code
                      from ftvlocn, ftrlcna
                     where ftvlocn_status_ind = 'A'
                       and nvl(ftvlocn_term_date,sysdate+1)>sysdate  --TC MOD 20160601 dgs change to l_eff_date
                       and sysdate>=ftvlocn_eff_date and sysdate<ftvlocn_nchg_date  --TC MOD 20160601 dgs change to l_eff_date
                       and ftvlocn_coas_code = ftrlcna_coas_code(+)
                       and ftvlocn_locn_code = ftrlcna_locn_code(+)
                       and ftrlcna_attt_code(+) = l_attt_code
                   ) a, ftvsdat
              where ftvsdat_sdat_code_entity = 'FZKEPRC'
                and ftvsdat_sdat_code_attr   = 'CODE-FORMAT-PART'
                and ftvsdat_status_ind       = 'A'
                and ftvsdat_sdat_code_opt_1  = a.fmt_prt
                and ftvsdat_eff_date<=sysdate
                and nvl(ftvsdat_term_date,sysdate+1)>sysdate  --TC MOD 20160601 dgs change to l_eff_date
                and nvl(ftvsdat_nchg_date,sysdate+1)>sysdate  --TC MOD 20160601 dgs change to l_eff_date
              order by picklist_prt_number,
                       picklist_prt,
                       parent_coas)
   select xmlelement("update-account-code-format-request", xmlattributes(fzkeprc.g_xmlns as "xmlns"),
     xmlagg(xmlelement("account-code-format",
              xmlattributes(l_format_code as "code", l_format_name as "name"),
      (select xmlelement("picklist-part", xmlattributes(fmt_prt as "code", prt_required as "required"),
           decode(l_attt_code,null,null,                 --  SERGEYV  02/27/2012
                xmlelement("attribute-definition", xmlattributes(l_attt_code as "name", 'STRING' as "type"))),
        (select xmlagg(
                xmlelement("part-option", xmlattributes(picklist_prt as "value", picklist_desc as "description"),
           decode(attribute_value,null,null,
                  xmlelement("attribute", xmlattributes(l_attt_code as "name", attribute_value as "value")))))
           from v where fmt_prt=c.fmt_prt))
         from(select distinct fmt_prt, prt_required from v where fmt_prt='COAS') c),
      (select xmlagg(
              xmlelement("picklist-part",
-- TC MOD 20121212 START ------------------------------------------------------ 
--                xmlattributes(fmt_prt as "code", prt_required as "required", 'true' as "hierarchical",
--                               'COAS' as "depends-on-part-ref"), --  SERGEYV  03/19/2012
                xmlattributes(fmt_prt as "code", prt_required as "required", 'false' as "hierarchical"),
-- RT #48072 end                                                
           decode(l_attt_code,null,null,                 --  SERGEYV  02/27/2012
                xmlelement("attribute-definition", xmlattributes(l_attt_code as "name", 'STRING' as "type"))),
        (select xmlagg(
                xmlelement("part-option",
                  xmlattributes(b.picklist_prt as "value", b.picklist_desc as "description"),
-- TC MOD 20121212 START ------------------------------------------------------ 
--                  xmlelement("parent-hierarchy-option", xmlattributes(parent_coas as "ref")),
-- TC MOD 20121212 END   ------------------------------------------------------ 
           decode(attribute_value,null,null,
                  xmlelement("attribute", xmlattributes(l_attt_code as "name", attribute_value as "value")))))
           from v b where fmt_prt=c.fmt_prt)))
         from(select distinct fmt_prt, picklist_prt_number, prt_required
                from v where fmt_prt<>'COAS' order by picklist_prt_number) c)))).getclobval()
   into l_clob from dual;
--
   p_rsp := l_clob;
--
   exception when others then p_msg := 'p_account_code_request: '||sqlerrm;
dbms_output.put_line(p_msg);
end p_account_code_request;
--
--==============================================================================
--
END FZKEPRC_VA;
/
