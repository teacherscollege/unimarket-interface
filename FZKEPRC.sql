CREATE OR REPLACE PACKAGE                             FZKEPRC IS
/*******************************************************************************

   NAME:       FZKEPRC
   PURPOSE:    This package is Unimarket eProcurement interface.
   NOTES:

   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20130910    J Stark       NEW FUNCTION FOR FOR API ERROR, DISPLAYS
                             ORDER AND INVOICE NUMBER.
   20140716    J Stark       ADDED GLOBAL g_umkt_sysid (PROD/DEMO) 
                             FOR EMAILS
   20160601    D Semar       New function for determining transaction date for dual FY modification
   20170303    D Semar       Expanded p_send_error_email to route to Purchasing or AP depending on error. p_get_email_route
   20180604    D Semar       Implemented PO Close from St. Louis U. Chg 34923
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   0.1  05/19/2010  S.Vorotnikov  Initial.
   3.0  11/24/2010  S.Vorotnikov  Added InvoiceDetailRequest
   3.1  12/06/2010  Bob Goudreau  Variables added for InvoiceDetailRequest
   3.2  06/30/2011  S.Vorotnikov  deploymentMode added
   3.3  07/14/2011  S.Vorotnikov  p_val_account_request: multi accounting added
   4.0  07/21/2011  S.Vorotnikov  Moved all request functions to separate packages
   4.1  02/14/2012  S.Vorotnikov  Changed to f_request(p_req CLOB, p_orig varchar2)
   5.0  04/13/2012  S.Vorotnikov  Added bid-request
   5.1  05/09/2012  S.Vorotnikov  Procedures added: p_request, p_schedule_job
   5.2  09/12/2012  S.Vorotnikov  Added supplier-data-request
*******************************************************************************/

   -------------------Types-----------------------------------------------------

   type strings is table of varchar2(255);

   ------------------ Constants ------------------------------------------------

-- Banner Date Format, now is DD-MON-RR
   g_date_frmt  constant varchar2(100):=G$_DATE.GET_NLS_DATE_FORMAT;

   ------------------ Response Status Type -------------------------------------

   g_unknown                     constant varchar2(100):='unknown';
   g_ok                          constant varchar2(100):='OK';
   g_bad_request                 constant varchar2(100):='BAD_REQUEST';
   g_not_implemented             constant varchar2(100):='NOT_IMPLEMENTED';
   g_unauthorized                constant varchar2(100):='UNAUTHORIZED';
   g_temporary_server_error      constant varchar2(100):='TEMPORARY_SERVER_ERROR';
   g_response   varchar2(500):='<response code="?response-state?">?response-message?</response>';

   ------------------ Unimarket Request Types ----------------------------------

   user_detail_request           constant varchar2(100):='user-detail-request';
   val_account_request           constant varchar2(100):='validate-account-code-request';
   OrderRequest                  constant varchar2(100):='OrderRequest';
   account_code_request          constant varchar2(100):='account-code-request';
   InvoiceDetailRequest          constant varchar2(100):='InvoiceDetailRequest';
   upd_account_code_frmt_request constant varchar2(100):='update-account-code-format-request';
   bid_request                   constant varchar2(100):='bid-request-request';
   supplier_data_request         constant varchar2(100):='supplier-data-request';
   --  <DGS 20180604>  Start
   StatusUpdateRequest           constant varchar2(100):='StatusUpdateRequest';
   --  <DGS 20180604>  End
--
   g_request_types    strings := strings(user_detail_request,
                                         val_account_request,
                                         OrderRequest,
                                         InvoiceDetailRequest,
                   --  <DGS 20180604>  Start
                                         StatusUpdateRequest,
                   --  <DGS 20180604>  End                         
                                         account_code_request,
                                         bid_request,
                                         supplier_data_request);

                                      

   ------------------ Global vars ----------------------------------------------

   g_req         CLOB;
   g_xml         XMLType;
   g_new_request varchar2(1);
   g_old_docno   varchar2(100);
   g_err         varchar2(4000);
   g_namespace   varchar2(100);
   g_xmlns       varchar2(100);
   g_ns_out      varchar2(100);
   g_orig        varchar2(100);
   g_fld_name    varchar2(100);
   g_prc_name    varchar2(100);
   g_user        varchar2(100);
-- TC MOD 20130325 START ------------------------------------------------------
   g_uni         varchar2(30);
-- TC MOD 20130325 END   ------------------------------------------------------     
-- TC MOD 20140716 START ------------------------------------------------------
   g_umkt_sysid  varchar2(4);
-- TC MOD 20140716 END   ------------------------------------------------------       
   g_banner      general.gubinst.gubinst_instance_name%type;
   g_rid         fzbeprc.fzbeprc_id%Type;
   g_req_type    fzbeprc.fzbeprc_req_type%Type;
   g_eprf_rec    fzreprf%rowtype;


   ------------------ Functions ------------------------------------------------
-- This is THE ONE.
   function  f_request(p_req CLOB, p_orig varchar2) return CLOB;
   -----------------------------------------------------------------------------
-- The procedure to reprocess old request
   procedure p_request(p_epr_id fzbeprc.fzbeprc_id%type,
                       p_origin fzreprf.fzreprf_origin_code%type);
   -----------------------------------------------------------------------------
-- The function to schedule request reprocess job
   function  f_schedule_job(p_schedule varchar2) return number;
   -----------------------------------------------------------------------------
-- Functions called from other FZKEPRC_xx packages
   function  xml2date   (p_date    varchar2)                         return varchar2;
   function  xml_val    (p_pth     varchar2, p_req boolean := false) return varchar2;
   function  f_get_val  (p_member  varchar2, p_req boolean := false) return varchar2;
   function  f_get_path (p_reqtype varchar2, p_member_name varchar2) return varchar2;
   function  p_got_error(p_sqlcode number,   p_sqlerrm varchar2)     return varchar2;
-- TC MOD 20130910 START ------------------------------------------------------
   function p_got_error_tagged(p_sqlcode      number,
                               p_sqlerrm      varchar2,
                               p_po_code      fpbpohd.fpbpohd_code%TYPE, 
                               p_invoice_code fabinvh.fabinvh_code%TYPE) return varchar2;
-- TC MOD 20130910 START ------------------------------------------------------
   procedure p_set_prc_name(p_name varchar2);
   procedure p_send_error_email(p_msg varchar2);
   
-- TC MOD 20160601 START ------------------------------------------------------
  procedure p_get_trans_eff_dt (p_fsyr_in  in varchar2,
                               p_trans_eff_date out date);
                              
---=============================================================================
-- TC MOD 20170303 START ------------------------------------------------------
  procedure p_get_email_route (p_msg_in  in varchar2,
                               p_route_to out varchar2);
                              
---=============================================================================
--
END FZKEPRC;
/


CREATE OR REPLACE PACKAGE BODY                                                                                                                                                                  FZKEPRC IS
/*******************************************************************************

   NAME:       FZKEPRC
   PURPOSE:    This package is Unimarket eProcurement interface.
   NOTES:

   TC REVISIONS:
   Date Stamp  Author        Description
   ----------  ------------  ---------------------------------------------
   20130910    J Stark       NEW FUNCTION FOR FOR API ERROR, DISPLAYS
                             ORDER AND INVOICE NUMBER.
   20121212    J Stark       CHANGE EMAIL AS PER TC STANDARD
   20130109    J Stark       Fix email routine for multiple "to" 
                             addresses.
   20140716    J Stark       ADDED GLOBAL g_umkt_sysid (PROD/DEMO) 
                             FOR EMAILS
   20140724    J Stark       Modified error message when path not found is
                             //ItemOut[1]//SupplierID[1][@domain="external-id"]
                             Vendor T number is missing.         
   20160601    D Semar      New function for determining transaction date for dual FY modification
   20161110    D Semar      Modified call to sokemal.p_sendemail for new parameters.
   20170303    D Semar      Expanded email to route to Purchasing or AP offices according to the error raised. p_get_email_route
   20180604    D Semar      Implement PO Close St. Louis U Chg 34923 <DGS 20180604>
   REVISIONS:
   Ver  Date        Author        Description
   ---  ----------  ------------  ------------------------------------
   0.1  05/19/2010  S.Vorotnikov  Initial. (R&R Request #4348)
   0.2  08/09/2010  S.Vorotnikov  SUBSTR added
                                     l_header_rec.po_vend_id := substr(f_get_val(g_SupplierID,true),1,9);
   0.3  08/16/2010  S.Vorotnikov  p_user_detail_request changed to remove
                                  empty optional elements from response
                                     </groups>, </phone>, </email>, </address>

   1.0  08/27/2010  S.Vorotnikov  p_send_error_email added

   2.0  01/31/2011  S.Vorotnikov  fzkafmt.fnc_clob_upd_acct_code_fmt_req removed
                                    from p_account_code_request
   2.1  01/31/2011  S.Vorotnikov  p_account_code_request changed:
                                    xml element "attribute" added
   2.2  02/23/2011  S.Vorotnikov  Changed FTVSDAT_SDAT_CODE_ENTITY
                                    from FZKAFMT to FZKEPRC
   2.3  03/08/2011  S.Vorotnikov  p_account_code_request: xmlns changed
   2.4  03/08/2011  S.Vorotnikov  p_val_account_code_request: xml structure changed
   2.5  03/10/2011  S.Vorotnikov  get g_xmlns from request added
   2.6  05/31/2011  S.Vorotnikov  New field FZBEPRC_RESPONSE added
   2.7  06/30/2011  S.Vorotnikov  Banner production instance check added
   2.8  07/14/2011  S.Vorotnikov  p_val_account_request: multi accounting added

   3.0  11/24/2010  S.Vorotnikov  p_InvoiceDetailRequest added to p_process_req
   3.1  12/08/2010  Bob Goudreau  p_get_inv_data added
   3.2  07/19/2011  S.Vorotnikov  p_user_detail_request changed to use GORSDAV data
   3.4  07/20/2011  S.Vorotnikov  Procedures moved to separate packages
                                    p_user_detail_request      to FZKEPRC_UD package
                                    p_val_account_request      to FZKEPRC_VA package
                                    p_account_code_request     to FZKEPRC_VA package
                                    p_OrderRequest             to FZKEPRC_PO package
                                    p_InvoiceDetailRequest     to FZKEPRC_IN package

   3.5  08/01/2011  S.Vorotnikov  List of the supported xml requests moved to FTVSDAT (FZKEPRC)
                                       SDAT_CODE_OPT_1   - key word UMREQEST
                                       SDAT_CODE_ATTR    - request type (xml element)
                                       SHORT_TITLE.TITLE - <package_name>.<proc_name>
                                  From now Request Processing procedures will be called dynamically
                                  execute immediate 'begin '||l_proc||'(p_rsp => :l_rsp, p_msg => :l_msg); end;'
                                  based on XML request type        (SDAT_CODE_ATTR)
                                       and packaged procedure name (SHORT_TITLE.TITLE).
                                  It will allow migration of changed or implementing new requests
                                  without any modifications of the main FZKEPRC package.
        SDAT_CODE_ATTR                 SDAT_CODE_OPT_1  TITLE                       SHORT_TITLE
        -----------------------------  ---------------  --------------------------  -----------
        user-detail-request            UMXMLREQ         p_user_detail_request       fzkeprc_ud
        validate-account-code-request  UMXMLREQ         p_val_account_request       fzkeprc_va
        account-code-request           UMXMLREQ         p_account_code_request      fzkeprc_va
        OrderRequest                   UMXMLREQ         p_OrderRequest              fzkeprc_po
        InvoiceDetailRequest           UMXMLREQ         p_InvoiceDetailRequest      fzkeprc_in

   4.0  10/11/2011  S.Vorotnikov  Changed logic to define and use XML name space
   4.1  02/14/2012  S.Vorotnikov  Changed to f_request(p_req CLOB, p_orig varchar2)
   4.2  03/21/2012  S.Vorotnikov  Added cXML Response
   4.3  03/29/2012  S.Vorotnikov  Use UTL_MAIL.SEND instead of FISHRS.MAIL_PKG.SEND
   4.4  04/18/2012  S.Vorotnikov  Added raise in xml_val function
   5.0  04/13/2012  S.Vorotnikov  Added bid-request
   5.1  05/09/2012  S.Vorotnikov  Procedures added: p_request, p_shcedule_job
   5.2  07/17/2012  S.Vorotnikov  Procedures p_update_log_record_OK changed:
                                     update FZBEPRC set fzbeprc_req = null
                                     added: if g_req_type = bid_request
   5.3  08/30/2012  S.Vorotnikov  Leave response generated by cXML request process
   5.4  09/10/2012  S.Vorotnikov  Procedures p_update_log_record_OK and p_update_log_record_ERROR changed
                                    fzbeprc_response = decode(g_req_type, account_code_request, null, l_rsp)
   5.5  09/12/2012  S.Vorotnikov  Added supplier-data-request (yet does nothing)
   5.5  09/13/2012  S.Vorotnikov  Changed fzbeprc_response = decode(g_req_type, account_code_request, null, l_rsp),
                                       to fzbeprc_response = case g_req_type when account_code_request then null
                                                                             else l_rsp end,
*******************************************************************************/
--
--==============================================================================
--
-- TC MOD 20170303 START ------------------------------------------------------
procedure p_get_email_route (p_msg_in  in varchar2,
                               p_route_to out varchar2) is

begin
      p_route_to := case when p_msg_in like '*FB_INVOICE%' then  'UM_PO_ERROR'
                         when p_msg_in like '%currency code%' then 'UM_PO_ERROR'
                         when p_msg_in like '%Parent PO%' then 'UM_PO_ERROR'
                         when p_msg_in like '%Blanket PO%' then 'UM_PO_ERROR'
                         when p_msg_in like '%Purchase Order%' then 'UM_PO_ERROR'
                         when p_msg_in like '%Fund code%terminated%' then 
                                  case when substr(p_msg_in ,instr(p_msg_in,'Fund code')+10,1) = '5' then 'UM_GRANTS_ERROR'
                                  else 'UM_FINANCE_ERROR'
                                  end
                         when p_msg_in like '%Organization code%not valid%' then
                                  case when substr(p_msg_in ,instr(p_msg_in,'Fund code')+18,1) = '5' then 'UM_GRANTS_ERROR'
                                  else 'UM_FINANCE_ERROR'
                                  end
                         when p_msg_in like '%Index%invalid%' then 
                                  case when substr(p_msg_in ,instr(p_msg_in,'Index')+6,1) = '5' then 'UM_GRANTS_ERROR'
                                  else 'UM_FINANCE_ERROR'
                                  end
                        else
                        'INV_EMAIL_TO'
                        end;
exception 
when others then p_route_to := '';
end;
                               
--==============================================================================
procedure p_get_trans_eff_dt (p_fsyr_in in  varchar2,
                               p_trans_eff_date out date) is 
l_msg varchar2(80) := null;
p_curr_fsyr ftvfsyr.ftvfsyr_fsyr_code%type;
g_fsyr_start_date date;
g_fsyr_end_date date;
g_enc_roll_ind varchar2(1);

begin
  --TC MOD 20160601 START ----------------------------------------

   begin 
     select ftvfsyr_fsyr_code into p_curr_fsyr
       from ftvfsyr
      where trunc(sysdate) between ftvfsyr_start_date and ftvfsyr_end_date
        and ftvfsyr_coas_code = '1';
    exception
     when others then
      l_msg := 'Unable to determine current fsyr'; 
   end;
   begin
     select ftvfsyr_start_date, ftvfsyr_end_date, decode(fgbyrlm_encb_per_date,null,'N','Y') 
     into g_fsyr_start_date, g_fsyr_end_date, g_enc_roll_ind
     from ftvfsyr, fgbyrlm
     where ftvfsyr_fsyr_code = p_fsyr_in
      and ftvfsyr_coas_code = '1'
      and fgbyrlm_fsyr_code (+) = ftvfsyr_fsyr_code
      and fgbyrlm_coas_code (+) = '1';
   exception 
     when others then
      l_msg := 'Unable to determine FY start and end dates for FY'||p_fsyr_in; 
   end;
    
   -- DGS code for changing the order and trans dates based on FY
   -- if the FY on the order is in the future, use the start date of the fiscal year for the order date
   -- if the FY on the order is in the past and encumbrances haven't rolled, use the end date of the FY
   -- otherwise, use the sysdate.
   case when p_fsyr_in > p_curr_fsyr then
           p_trans_eff_date := g_fsyr_start_date;
        when p_fsyr_in < p_curr_fsyr then
          if g_enc_roll_ind = 'N' then
           p_trans_eff_date := g_fsyr_end_date;
          else
           p_trans_eff_date := sysdate;
          end if;
        else
           p_trans_eff_date := sysdate;
   end case; 
exception
   when others then dbms_output.put_line(sqlerrm);
end;

--
--==============================================================================
--
procedure p_send_error_email(p_msg varchar2) is
   l_from varchar2(100);
   l_subj varchar2(500);
   l_to varchar2(32000);
   l_cc varchar2(32000);
-- TC MOD 20121212 START ------------------------------------------------------ 
   v_mail_successful VARCHAR2(300) := 'T' ; 
   v_mail_errmsg     VARCHAR2(300) := 'F' ; 
-- TC MOD DGS 20161110 START ------------------------------------------------------
   v_error_type varchar2(100);
   v_error_code varchar2(100);
   v_error_message varchar2(3000);
-- TC MOD DGS 20161110 END ------------------------------------------------------
-- TC MOD DGS 20170303 START ------------------------------------------------------
   v_to_list varchar2(100);
   p_route_to varchar2(100); 
-- TC MOD DGS 20170303 END ------------------------------------------------------   
begin
-- TC MOD 20121212 END   ------------------------------------------------------ 
--
   l_subj := 'Unimarket: '||g_banner||': Error in request (FZBEPRC_ID): '||g_req_type||' ('||g_rid||')';
--
-- TC MOD DGS 20170303 START ------------------------------------------------------
/* determine where to route the email to */

if g_req_type = InvoiceDetailRequest then
    p_get_email_route(p_msg,p_route_to);
    v_to_list := nvl(p_route_to,'UM ERROR EMAIL');
    tcapp.util_msg.LogEntry('Send Mail To: '||v_to_list);
else
    v_to_list := case g_req_type when OrderRequest then 'UM_PO_ERROR'
                                 when user_detail_request then 'UM_PO_ERROR'
                                 else 'UM ERROR EMAIL'
                  end;
end if;

-- TC MOD DGS 20170303 END ------------------------------------------------------

-- TC MOD 20130109 START ------------------------------------------------------ 
/*   for e in(select upper(ftvsdat_sdat_code_opt_1) opt, ftvsdat_title email from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and upper(ftvsdat_sdat_code_attr) = 'UM ERROR EMAIL'
               and upper(ftvsdat_sdat_code_opt_1) in('FROM','TO','CC')
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) loop
      if  e.opt='FROM' then l_from := e.email;
      elsif e.opt='TO' then l_to := l_to||trim(e.email)||', ';
      elsif e.opt='CC' then l_cc := l_cc||trim(e.email)||', '; end if;
   end loop;
--
   l_to := rtrim(l_to,', '); l_cc := rtrim(l_cc,', ');
-- TC MOD 20121212 START ------------------------------------------------------ 
     if l_from is not null and l_to is not null then
--       UTL_MAIL.SEND(sender=>l_from, recipients=>l_to, cc=>l_cc,
--                     subject=>l_subj, message=>p_msg);
   sokemal.p_sendemail (email_addr      => l_to,                   
                        email_to_name   => NULL,
                        email_from_addr => l_from,
                        email_from_name => 'Unimarket Prod - NO REPY',
                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                        email_subject   => l_subj,
                        email_message   => p_msg,
                        email_success   => v_mail_successful,
                        email_reply     => v_mail_errmsg);    
-- TC MOD 20121212 END   ------------------------------------------------------                                         
   else
      dbms_output.put_line('ERROR in p_send_error_email: Can not send email!!!');
   end if;
--
*/   
   for e in(select ftvsdat_title email, ftvsdat_code_level code_level  from ftvsdat
             where ftvsdat_sdat_code_entity = 'FZKEPRC'
               and upper(ftvsdat_sdat_code_attr) =  v_to_list    -- DGS 20170303   'UM ERROR EMAIL'
               and upper(ftvsdat_sdat_code_opt_1) = 'FROM'
               and ftvsdat_status_ind = 'A'
               and ftvsdat_eff_date <= sysdate
               and nvl(ftvsdat_term_date, sysdate+1) > sysdate
               and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
             order by ftvsdat_code_level) 
      loop
         l_from := e.email;
         for t in(select ftvsdat_title email from ftvsdat
                where ftvsdat_sdat_code_entity = 'FZKEPRC'
                  and upper(ftvsdat_sdat_code_attr) =  v_to_list -- DGS 20170303 'UM ERROR EMAIL'
                  and upper(ftvsdat_sdat_code_opt_1) = 'TO'
                  and ftvsdat_status_ind = 'A'
                  and ftvsdat_eff_date <= sysdate
                  and nvl(ftvsdat_term_date, sysdate+1) > sysdate
                  and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate
                  and ftvsdat_code_level = e.code_level)
             loop
             	  tcapp.util_msg.LogEntry('Send Mail To: '||t.email||' '||l_subj);

                If l_from is not null and t.email is not null then          	
-- -- TC MOD DGS 20161110 Start
                   sokemal.p_sendemail (email_addr      => t.email,                   
                                        email_to_name   => NULL,
                                        email_from_addr => l_from,
                                        email_from_name => 'Unimarket '||g_umkt_sysid||' - NO REPY',
                                        email_host      => 'SUNMAIL.TC.COLUMBIA.EDU',
                                        email_subject   => l_subj,
                                        email_message   => p_msg,
                                        email_success   => v_mail_successful,
                                        email_reply     => v_mail_errmsg,
                                        email_error_type => v_error_type,
                                        email_error_code => v_error_code,
                                        email_error_message => v_error_message); 
-- -- TC MOD DGS 20161110 End
                else
                   dbms_output.put_line('ERROR in p_send_error_email: Can not send email!!!'); 
                   tcapp.util_msg.LogEntry('ERROR in p_send_error_email: Can not send email!!!');                               	                                     
                end if;
             end loop;   
      end loop;   
-- TC MOD 20130109 END   ------------------------------------------------------


exception when others then
   dbms_output.put_line('ERROR in p_send_error_email: '||SQLERRM);
end p_send_error_email;
--
--==============================================================================
--
function xml2date(p_date varchar2) return varchar2 is
   s varchar2(11):=to_char(to_date(substr(p_date,1,10),'YYYY-MM-DD'),g_date_frmt);
begin return s; end xml2date;
--
--==============================================================================
--
function f_get_path(p_reqtype varchar2, p_member_name varchar2)return varchar2 is
   l_path fzbxmlp.fzbxmlp_xml_path%type;
begin
   select fzbxmlp_xml_path into l_path from fzbxmlp
    where fzbxmlp_req_type = p_reqtype and fzbxmlp_membername = p_member_name;
   return l_path;
exception when no_data_found then
   return null;
end f_get_path;
--
--==============================================================================
--
function xml_val(p_pth varchar2, p_req boolean := false)return varchar2 is
   l_str varchar2(4000);
begin
   g_err := ''; l_str := '';
   select ExtractValue(g_xml, p_pth, g_namespace) into l_str
     from dual where existsNode(g_xml, p_pth, g_namespace)=1;
   if p_req and l_str is null then
      g_err := p_pth||' is NULL';
      raise_application_error(-20000, g_err);
   end if;
   if l_str='null' then l_str:=null; end if;
   return l_str;
exception
   when no_data_found then
      if not p_req then return null;
      else
         g_err := 'Path '||p_pth||' not found';
-- TC MOD 20140724 START ------------------------------------------------------ 
--       ADD SPECIAL ERROR MESSAGE FOR MISSING T NUMBER
         IF p_pth = '//ItemOut[1]//SupplierID[1][@domain="external-id"]'
         THEN
         	  g_err := f_get_val('orderID',true);
         	  g_err := g_err || ' - Supplier ID, "T" Number, not found.  Please add external-ID to supplier in Unimarket.';
         END IF;        
-- TC MOD 20140724 END   ------------------------------------------------------         
         raise_application_error(-20000, g_err);
--         
      end if;
   when others then
      if SQLCODE=-20000 then
         raise;
      elsif SQLCODE=-19025 then
         g_err := 'Too many nodes: '||p_pth;
         raise_application_error(-20000, g_err);
      else
--       g_err := 'xml_val: '||sqlerrm;          -- commented out
         raise;                                  -- added by SERGEYV  04/18/2012
      end if;
end xml_val;
--
--==============================================================================
--
function f_get_val(p_member varchar2, p_req boolean := false)return varchar2 is
begin
   g_fld_name := p_member;
   return xml_val(f_get_path(g_req_type, p_member), p_req);
end f_get_val;
--
--==============================================================================
--
procedure p_set_prc_name(p_name varchar2) is
begin fzkeprc.g_prc_name:=p_name; fzkeprc.g_fld_name:=null; end;
--
--==============================================================================
--
function p_got_error(p_sqlcode number, p_sqlerrm varchar2) return varchar2 is
   l_msg varchar2(4000);
begin
   if p_sqlcode=-20000 then           -- f_get_val internal error
      l_msg := substr(p_sqlerrm, 12);
   elsif p_sqlcode=-20100 then        -- Invoice API error
      l_msg := 'API Error:'||chr(10)||replace(ltrim(rtrim(substr(SQLERRM,12),':'),':'),'::::',chr(10));
   else
      l_msg := 'Error in '||fzkeprc.g_prc_name||':'||chr(10)||'  Last called f_get_val('||fzkeprc.g_fld_name||')'||chr(10)||'  '||p_sqlerrm;
   end if;
dbms_output.put_line(l_msg);
tcapp.util_msg.LogEntry(l_msg);
   return l_msg;
end;
--
-- TC MOD 20130910 START ------------------------------------------------------  
function p_got_error_tagged(p_sqlcode      number,
                            p_sqlerrm      varchar2,
                            p_po_code      fpbpohd.fpbpohd_code%TYPE, 
                            p_invoice_code fabinvh.fabinvh_code%TYPE)
RETURN VARCHAR2
IS
   l_msg varchar2(4000); 
begin
   if p_sqlcode=-20000 then           -- f_get_val internal error
      l_msg := substr(p_sqlerrm, 12);
   elsif p_sqlcode=-20100 then        -- Invoice API error
      l_msg := 'API Error:'||chr(10)||replace(ltrim(rtrim(substr(SQLERRM,12),':'),':'),'::::',chr(10));
      l_msg := 'PO: ' || NVL(p_po_code,'UNKNOWN') ||chr(10) ||
               'INVOICE: ' || NVL(p_invoice_code,'UNKNOWN') ||chr(10) || l_msg;                
   else
      l_msg := 'Error in '||fzkeprc.g_prc_name||':'||chr(10)||'  Last called f_get_val('||fzkeprc.g_fld_name||')'||chr(10)||'  '||p_sqlerrm;
   end if;
   return l_msg;
end;
-- TC MOD 20130910 END   ------------------------------------------------------

--==============================================================================
--==============================================================================
--
procedure P_NEW_REQUEST(p_rsp in out CLOB)is
   l_msg  fzbeprc.fzbeprc_message%Type:='';
   l_sts  fzbeprc.fzbeprc_status%Type:='';
   l_proc varchar2(255):='';
   l_rsp  CLOB:='';
--------------------------------------------------------------------------------
procedure p_create_log_record is
begin
   begin
      g_req :=  REPLACE(g_req, '&amp;', 'and');
      g_xml := sys.xmltype.createXML(g_req);
      l_sts := 'I';
      l_msg := null;
   exception when others then
      l_sts := 'E';
      l_msg := 'Not XML';
      g_req_type := g_unknown;
   end;
   insert into fzbeprc(fzbeprc_status, fzbeprc_req, fzbeprc_message)
                values(l_sts, g_req, l_msg)
   returning fzbeprc_id into g_rid;
   commit;
exception when others then
   tcapp.util_msg.LogEntry('p_create_log_record Exception: ' ||SQLERRM);   
end;
--------------------------------------------------------------------------------
procedure p_get_request_type is
   l_num  number:=0;
begin
-- Check root element and get xml name space
   select g_xml.getRootElement() into l_proc from dual;
   if l_proc = 'cxml-request' then
      g_xmlns := null; g_namespace := null;
   else
      select g_xml.getnamespace() into g_xmlns from dual;
      g_namespace := 'xmlns="'||g_xmlns||'"';
   end if;
-- Get request type
   for i in 1..g_request_types.count loop
      select count(*) into l_num from dual
       where existsNode(g_xml,'//'||g_request_types(i), g_namespace) = 1;
      if l_num > 0 then
         g_req_type := g_request_types(i);
         exit;
      end if;
   end loop;
   if l_num = 0 then
      g_req_type := g_unknown;
      l_msg := g_not_implemented;
   end if;
end;
--------------------------------------------------------------------------------
procedure p_get_fzreprf_rec is
begin
-- Get e-Procurement parameters
   select * into g_eprf_rec from fzreprf
    where upper(ltrim(rtrim(fzreprf_origin_code))) = g_orig;
exception when no_data_found then
   l_msg := 'Origin '||g_orig||' not found in FZREPRF parameter table';
end;
--------------------------------------------------------------------------------
procedure p_process_request(p_rsp in out CLOB, p_msg in out varchar2) is
begin
   case g_req_type
   when user_detail_request   then fzkeprc_ud.p_user_detail_request (p_rsp, p_msg);
   when val_account_request   then fzkeprc_va.p_val_account_request (p_rsp, p_msg);
   when OrderRequest          then fzkeprc_po.p_OrderRequest        (p_rsp, p_msg);
   when InvoiceDetailRequest  then fzkeprc_in.p_InvoiceDetailRequest(p_rsp, p_msg);
   when account_code_request  then fzkeprc_va.p_account_code_request(p_rsp, p_msg);
   --  <DGS 20180604>  Start
   when StatusUpdateRequest   then fzkeprc_po.p_CloseOrder(p_rsp, p_msg);
   --  <DGS 20180604>  End

   when supplier_data_request then p_rsp := replace(replace(g_response,
                                                    '?response-state?', g_ok),
                                                  '?response-message?', g_ok);
-- <<<
-- Comment next line out if you don't have package FZKEPRC_BD installed
   when bid_request           then fzkeprc_bd.p_bid_request         (p_rsp, p_msg);
-- >>>
   else p_msg := g_not_implemented;
   end case;
end;
--------------------------------------------------------------------------------
procedure p_update_log_record_OK is
begin
   if g_req_type = account_code_request then
      g_user :='unicon';
      l_msg  :='update-account-code-format-request xml doc sent to Unimarket';
--  <DGS 20180604>  Start
--   elsif g_req_type in(OrderRequest, InvoiceDetailRequest) then
   elsif g_req_type in(OrderRequest, InvoiceDetailRequest, StatusUpdateRequest) then
--  <DGS 20180604>  End

      l_msg := g_req_type||' processed';
   elsif g_req_type in(bid_request) then
      l_msg := 'bid-request-response xml doc sent to Unimarket';
   else
      l_msg := replace(g_req_type,'request','response')||' xml doc sent to Unimarket';
   end if;
   update FZBEPRC
      set fzbeprc_req_type = g_req_type,
          fzbeprc_req_user = g_user,
          fzbeprc_status   = 'C',
          fzbeprc_message  = l_msg,
--        fzbeprc_response = decode(g_req_type, account_code_request, null, l_rsp),
          fzbeprc_response = case g_req_type when account_code_request then null
                                             else l_rsp end,
          fzbeprc_user_id  = user,
          fzbeprc_activity_date = sysdate
    where FZBEPRC_ID = g_rid;
 -- <DGS 20180604>  Start   
--    if g_req_type in(OrderRequest, InvoiceDetailRequest, bid_request) then
   if g_req_type in(OrderRequest, InvoiceDetailRequest, bid_request, StatusUpdateRequest) then
 -- <DGS 20180604>  End      
      update FZBEPRC
         set fzbeprc_req = null
       where FZBEPRC_ID = g_rid;
   end if;
end;
--------------------------------------------------------------------------------
procedure p_update_log_record_ERROR is
begin
   update FZBEPRC
      set fzbeprc_req_type = g_req_type,
          fzbeprc_req_user = g_user,
          fzbeprc_status   = 'E',
          fzbeprc_message  = l_msg,
--        fzbeprc_response = decode(g_req_type, account_code_request, null, l_rsp),
          fzbeprc_response = case g_req_type when account_code_request then null
                                             else l_rsp end,
          fzbeprc_user_id  = user,
          fzbeprc_activity_date = sysdate
    where FZBEPRC_ID = g_rid;
end;
--------------------------------------------------------------------------------
--
begin -- P_NEW_REQUEST
--
   if g_new_request = 'Y' then
      p_create_log_record;
      if l_msg is null then
         p_get_request_type;
      end if;
   end if;
--
   if l_msg is null then p_get_fzreprf_rec; end if;
--
   if l_msg is null then p_process_request(l_rsp, l_msg); end if;
--
-- Update log record
   if l_msg is null then -- Everything is OK
      p_update_log_record_OK;
   else                  -- Something wrong!!!
 --  <DGS 20180604>  Start
 --      if g_req_type NOT in(OrderRequest, InvoiceDetailRequest) then
      if g_req_type NOT in(OrderRequest, InvoiceDetailRequest, StatusUpdateRequest) then
 --  <DGS 20180604>  End     
--       generate uXML "Bad Request" response
         l_rsp := replace(replace
                 (g_response,'?response-state?',   g_bad_request),
                             '?response-message?', l_msg);
      end if;
      p_update_log_record_ERROR;
      p_send_error_email(l_msg);
dbms_output.put_line(l_msg);
tcapp.util_msg.LogEntry(l_msg);
   end if;
--
-- Done
   p_rsp := l_rsp;
--dbms_output.put_line('length(p_rsp)='||length(p_rsp));
--tcapp.util_msg.LogEntry('length(p_rsp)='||length(p_rsp));
   commit;
--
exception when others then
   tcapp.util_msg.LogEntry('p_new_request Exception: ' ||SQLERRM);
end P_NEW_REQUEST;
--
--==============================================================================
--
function f_schedule_job(p_schedule varchar2) return number is
   l_job    ftvsdat.ftvsdat_sdat_code_opt_1%type;
   l_what   varchar2(100);
   l_when   date;
   l_job_no number;
begin
--
-- Get schedule setting
   begin
      select ftvsdat_sdat_code_opt_2,
            'fzkeprc.p_request('||g_rid||','''||g_orig||''');',
             sysdate+(to_number(ftvsdat_data)/(24*60))
        into l_job, l_what, l_when
        from ftvsdat
       where ftvsdat_sdat_code_entity = 'FZKEPRC'
         and ftvsdat_sdat_code_attr   = 'SCHEDULE'
         and ftvsdat_sdat_code_opt_1  = p_schedule
         and ftvsdat_status_ind = 'A'
         and ftvsdat_sdat_code_opt_1 is NOT NULL
         and ftvsdat_sdat_code_opt_2 is NOT NULL
         and ftvsdat_eff_date <= sysdate
         and nvl(ftvsdat_term_date, sysdate+1) > sysdate
         and nvl(ftvsdat_nchg_date, sysdate+1) > sysdate;
   exception
      when no_data_found then return -1;
      when others then
         p_send_error_email('F_SCHEDULE_JOB: Something wrong with FTVSDAT '||p_schedule||' schedule setting');
         return -2;
   end;
--
-- Schedule Oracle Job
   if l_job = 'DBMS_JOB' then
      dbms_job.submit(l_job_no, l_what, l_when);
dbms_output.put_line(p_schedule||' scheduled job '||l_job_no);
      return l_job_no;
   end if;
--
   p_send_error_email('F_SCHEDULE_JOB: Schedule type '||l_job||' is not supported for schedule '||p_schedule);
   return -3;
--
exception when others then return -4;
end;
--
--==============================================================================
--
procedure p_request(p_epr_id fzbeprc.fzbeprc_id%type,
                    p_origin fzreprf.fzreprf_origin_code%type) is
   l_rsp clob; -- dummy
begin
--
-- Set parameter values
   g_new_request:='N'; g_orig:=p_origin; g_rid:=p_epr_id; g_user:=null;
--
-- Get request type, XML doc and Banner instance name
   select fzbeprc_req_type, fzbeprc_req,
          xmltype(nvl(fzbeprc_req,'<empty/>')),
          upper(gubinst_instance_name)
     into g_req_type, g_req, g_xml, g_banner
     from fzbeprc, gubinst where fzbeprc_id = p_epr_id;
--
-- Get old DOCNO and cXML doc from FZRCXML
   if g_req is null then
      select fzrcxml_doc_code, fzrcxml_doc, xmltype(fzrcxml_doc)
        into g_old_docno, g_req, g_xml
        from fzrcxml
       where fzrcxml_eprc_id = g_rid;
   end if;
--
   p_new_request(l_rsp);
--
exception when no_data_found then
   p_send_error_email('Error in P_REQUEST: FZBEPRC record not found: fzbeprc_id='||p_epr_id);
end p_request;
--
--==============================================================================
--
function f_request(p_req CLOB, p_orig varchar2) return CLOB is
   l_rsp CLOB;
   v_msg VARCHAR2(4000);
begin
--
--   tcapp.util_trap.MouseTrap(p_req);
-- Set global parameter values
   g_new_request:='Y'; g_orig:=upper(trim(p_orig));
   g_req:=p_req; g_req_type:=null; g_user:=null;
--   v_msg := dbms_lob.substr(g_req, 4000, 1 );
--
-- Get Banner instance name
   select upper(gubinst_instance_name) into g_banner from general.gubinst;
-- TC MOD 20140716 START ------------------------------------------------------
   IF g_banner = 'PROD'
   THEN
   	  g_umkt_sysid := 'Prod';
   ELSE
   	  g_umkt_sysid := 'Demo';
   END IF;
--   tcapp.util_msg.LogEntry(v_msg);
-- TC MOD 20140716 END   ------------------------------------------------------
--
   p_new_request(l_rsp);
   return l_rsp;
--
exception when others then
   tcapp.util_msg.LogEntry('f_request Exception: ' ||SQLERRM);
end;
--
--==============================================================================
--
  
END FZKEPRC;
/
