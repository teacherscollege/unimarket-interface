/* deleting a PO from Banner */
update fzbeprc
set FZBEPRC_STATUS = 'X'
where fzbeprc_id = '442813';

--These sql statements provide the steps for solving the error "Blanket PO not found" in Unimarket.  Before deleting the PO, check to be sure that it only exists in the PO tables and has not posted or been invoiced.

select * from fzbeprc
where fzbeprc_id = '442813'; -- This id will be in the error email 

select * from fzrcxml
where fzrcxml_eprc_id = '442813';

/* Check to make sure that this PO (the PO number will be in the error email) has not posted, encumbered or invoiced */

select * from fabinvh
where fabinvh_pohd_code = 'UI814880';  -- should not have anything here

select * from fgbtrnh
where fgbtrnh_doc_code = 'UI814880';  -- don't want to see anything in these tables either.

select * from fgbtrnd
where fgbtrnd_doc_code = 'UI814880';

select * from fgbench
where fgbench_num = 'UI814880';  -- shouldn't see anything here

select * from fgbjvcd
where fgbjvcd_doc_num = 'UI814880'; -- or here

/* If everything is clear, then the next three tables are the ones to delete from */

delete from fpbpohd
where fpbpohd_code = 'UI814880';  

delete from fprpodt
where fprpodt_pohd_code = 'UI814880';

delete from fprpoda
where fprpoda_pohd_code = 'UI814880';


/* example of fully formed po */
select * from fpbpohd
where fpbpohd_code = 'UI601667';

select * from fgbtrnd
where fgbtrnd_doc_code = 'UI601667';

select * from fgbench
where fgbench_num = 'UI601667';

select * from fgbjvcd
where fgbjvcd_doc_num = 'UI601667';

select * from fprpodt
where fprpodt_pohd_code = 'UI601667';

select * from fprpoda
where fprpoda_pohd_code = 'UI601667';








